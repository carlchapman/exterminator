package app.exterminator.sync.connect;

import android.os.AsyncTask;

/**
 * The Class RunnableTask is essentially the Android version of a Thread class,
 * but using AsyncTask - it takes a runnable object, which it then runs in the
 * background.
 * 
 * @author carlchapman
 */
public class RunnableTask extends AsyncTask<Runnable, Void, Void> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected Void doInBackground(Runnable... arg0) {
		arg0[0].run();
		return null;
	}

}
