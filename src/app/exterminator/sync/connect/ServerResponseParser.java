package app.exterminator.sync.connect;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.util.Log;

/**
 * Parser used to handle registration and login XML returned from the server.
 * 
 * Reference:
 *		http://www.androidhive.info/2011/11/android-xml-parsing-tutorial/
 * 
 * @author colegroff
 */
public class ServerResponseParser {

	/**
	 * Constructor.
	 */
	public ServerResponseParser() {
		
	}
	
	/**
	 * Gets the dom element.
	 *
	 * @param xml the xml
	 * @return the dom element
	 */
	public Document getDomElement(String xml) {
		
		Document doc = null;
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			 
			DocumentBuilder db = dbf.newDocumentBuilder();
 
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			doc = db.parse(is); 
 
		} catch (ParserConfigurationException e) {
			Log.e("Error: ", e.getMessage());
			return null;
		} catch (SAXException e) {
			Log.e("Error: ", e.getMessage());
			return null;
		} catch (IOException e) {
			Log.e("Error: ", e.getMessage());
			return null;
		}
		return doc;
	}
	
	/**
	 * Gets the value.
	 *
	 * @param item the item
	 * @param str the str
	 * @return the value
	 */
	public String getValue(Element item, String str) {
	    NodeList n = item.getElementsByTagName(str);
	    return this.getElementValue(n.item(0));
	}
	
	/**
	 * Gets the element value.
	 *
	 * @param elem the elem
	 * @return the element value
	 */
	public final String getElementValue(Node elem) {
		
		Node child;
		
		if(elem != null) {	
			if(elem.hasChildNodes()) {
				for(child = elem.getFirstChild(); child != null; child = child.getNextSibling()) {
					if(child.getNodeType() == Node.TEXT_NODE){
						return child.getNodeValue();
					}
				}
			}
		}
		return "";	
	}
}
