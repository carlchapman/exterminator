package app.exterminator.sync.connect;

import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import app.exterminator.GUI.control.AbstractControlScreen;

/**
 * A ConnectTask sends name value pairs to a url using httpPost while keeping a
 * progressDialog updated with the state of the connecting process, doing this
 * all on a background thread. 
 * 
 * Extending classes must implement the abstract method 'handleResponse(...)'
 * which is always called after a successful connection has been established and
 * the server has sent a response back.
 * 
 * @author carlchapman
 * 
 */
public abstract class AbstractConnectionTask extends
		AsyncTask<ConnectionData, Integer, Boolean> {

	/** The progress dialog. */
	protected ProgressDialog progressDialog;

	/** The parent. */
	protected AbstractControlScreen parent;

	/** The CONNECTIO n_ timeout. */
	final int CONNECTION_TIMEOUT = 50000;

	/** The CANCELLED. */
	final int CANCELLED = -1;

	/** The CONNECTING. */
	final int CONNECTING = 0;

	/** The NULLRESPONSE. */
	final int NULLRESPONSE = 1;

	/** The SERVERERROR. */
	final int SERVERERROR = 2;

	/** The SUCCESS. */
	final int SUCCESS = 3;

	/** The INVALI d_ register. */
	final int INVALID_REGISTER = 4;

	/** The attempt number. */
	int attemptNumber;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected Boolean doInBackground(ConnectionData... params) {
		// prepare to connect
		ConnectionData data = params[0];
		this.progressDialog = data.getProgressDialog();
		this.parent = (AbstractControlScreen) data.getParent();
		// progressDialog.show();
		attemptNumber = 0;
		boolean connectionSuccessful = false;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = setupPost(data);
		publishProgress(CONNECTING);
		long startTime = System.currentTimeMillis();

		while (attemptNumber < 3 && !connectionSuccessful
				&& System.currentTimeMillis() - startTime < CONNECTION_TIMEOUT) {
			System.out.println("Attempt number:" + attemptNumber);
			attemptNumber++;

			try {

				// try to connect, allowing user to cancel and publishing state
				if (isCancelled()) {
					publishProgress(CANCELLED);
					return false;
				}

				HttpResponse response = httpClient.execute(httpPost);

				if (isCancelled()) {
					publishProgress(CANCELLED);
					return false;
				} else if (response == null) {
					System.out.println("null response");
					publishProgress(NULLRESPONSE);
				} else {

					int responseNumber = response.getStatusLine()
							.getStatusCode();
					System.out.println("responseNumber: " + responseNumber
							+ " *********");

					if (responseNumber >= 300) {

						System.out
								.println("error of some kind here with status code: "
										+ responseNumber);
						publishProgress(SERVERERROR);

					} else if (responseNumber == 207) {

						publishProgress(INVALID_REGISTER);
						connectionSuccessful = true;

					} else if (response.getStatusLine().getStatusCode() < 300) {

						System.out.println("success..."
								+ response.getStatusLine().getStatusCode());
						publishProgress(SUCCESS);
						connectionSuccessful = true;

					}
				}
				if (connectionSuccessful) {
					handleResponse(response, data);
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("returning false");
		return false;
	}

	/**
	 * Setup post.
	 * 
	 * @param data
	 *            the ConnectionData
	 * @return the HttpPost object
	 */
	private HttpPost setupPost(ConnectionData data) {

		// set up the httpPost
		String urlArgument = data.getPath();
		HttpPost httpPost = new HttpPost(urlArgument);
		httpPost.addHeader("Accept", "text/xml");

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(data
					.getNameValuePairs()));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("httpPost set up");
		return httpPost;
	}

	/**
	 * Handle response.
	 * 
	 * @param response
	 *            the response
	 * @param data
	 *            the data
	 */
	protected abstract void handleResponse(HttpResponse response,
			ConnectionData data);

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	protected void onPostExecute(Boolean result) {
		System.out.println("dismissing dialog");
		progressDialog.dismiss();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
	 */
	protected void onProgressUpdate(Integer... values) {
		String message = "";
		switch (values[0]) {
		case CANCELLED:
			message = "cancelled";
			break;
		case CONNECTING:
			message = "connecting...";
			break;
		case NULLRESPONSE:
			message = "the response was null";
			break;
		case SERVERERROR:
			message = "there was a server error";
			break;
		case SUCCESS:
			message = "connection made!";
			break;
		case INVALID_REGISTER:
			message = "invalid username or email";
			break;
		default:
			message = "default message";
		}
		System.out.println("updating message.  message = " + message);
		if (progressDialog == null)
			System.out.println("null pd!");
		else {
			System.out.println("ProgressDialog pd: "
					+ progressDialog.toString());
			parent.runOnUiThread(new ChangeMessage(message, progressDialog));
		}
	}

	// ////////////////////runner that runs on the main gui thread when changing
	// messages

	/**
	 * The Class ChangeMessage.
	 */
	class ChangeMessage implements Runnable {

		/** The message. */
		private String message;

		/** The pd. */
		private ProgressDialog pd;

		/**
		 * Instantiates a new change message.
		 * 
		 * @param message
		 *            the message
		 * @param progressDialog
		 *            the progress dialog
		 */
		public ChangeMessage(String message, ProgressDialog progressDialog) {
			this.message = message;
			this.pd = progressDialog;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			pd.setMessage(message);
		}
	}
}
