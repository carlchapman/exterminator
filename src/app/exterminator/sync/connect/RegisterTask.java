package app.exterminator.sync.connect;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.widget.EditText;
import app.exterminator.GUI.control.LevelSelectScreen;
import app.exterminator.data.facades.Player;
import app.exterminator.data.utility.Globals;

/**
 * The Class RegisterTask handles the response from the server after a user
 * attempts to register. If the username, email or both were already taken, then
 * the user is prompted to try again. If the registration was a success, then
 * the player is converted to the new identity and the player is launched into
 * the level select screen.
 * 
 * @author carlchapman modified by Cole Groff
 */
public class RegisterTask extends AbstractConnectionTask {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * app.exterminator.sync.connect.ConnectionTask#handleResponse(org.apache
	 * .http.HttpResponse, app.exterminator.sync.connect.ConnectionData)
	 */
	@Override
	protected void handleResponse(HttpResponse response, ConnectionData data) {

		Document doc = null;
		String xmlResponse = null;
		HttpEntity resEntity = response.getEntity();
		int responseType;

		// //////////////////////////////////////////////////////////////////////////////////
		// Parse response from the server.

		ServerResponseParser parser = new ServerResponseParser();

		try {
			xmlResponse = EntityUtils.toString(resEntity);
			System.out.println("XML RESPONSE: " + xmlResponse);
			doc = parser.getDomElement(xmlResponse);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		NodeList nl = doc.getElementsByTagName("RegistrationResponse");
		Element e = (Element) nl.item(0);

		// Capture response type.
		responseType = Integer.parseInt(e.getAttribute("type"));
		System.out.println("RegistrationResponse type: " + responseType);

		// Response types:
		// type = 0: successful registration
		// type = 1: username already taken
		// type = 2: email already taken
		// type = 3: username and email already taken

		if (0 == responseType) { // successful registration

			Player player = ((Globals) parent.getApplicationContext())
					.getPlayer();
			String oldToken = player.getToken();
			
			if(player.isGuest()){
				player.setIsGuest(false);
				player.setUsername(parser.getValue(e, "Username"));
				player.setToken(parser.getValue(e, "AuthToken"));
				player.performUpdate();
			}else{
				player.setUsername(parser.getValue(e, "Username"));
				player.setToken(parser.getValue(e, "AuthToken"));
				player.setBugBucks(Player.INITIAL_BUGBUCKS);
				player.logoutOldUser(oldToken);
				player.performUpdate();
			}
			
			
			
			parent.runOnUiThread(new Runnable() {
				public void run() {
					parent.warnLaunchDialog(parent, "Successful Registration", "You have registered successfully", parent.new StartActivityOnClickListener(LevelSelectScreen.class,true), false);
				}
			});
		} else if (1 == responseType) { // username already taken

			final EditText usernameBox = parent.getUsernameBox();

			parent.runOnUiThread(new Runnable() {
				public void run() {
					parent.showAlert("Registration failed.",
							"Username not available.", usernameBox, null, null);
				}
			});

		} else if (2 == responseType) { // email already taken

			final EditText emailBox = parent.getEmailBox();
			final EditText emailBoxConfirmation = parent.getConfirmEmailBox();

			parent.runOnUiThread(new Runnable() {
				public void run() {
					parent.showAlert("Registration failed.",
							"Email not available.", emailBox,
							emailBoxConfirmation, null);
				}
			});

		} else if (3 == responseType) { // username and email already taken

			final EditText usernameBox = parent.getUsernameBox();
			final EditText emailBox = parent.getEmailBox();
			final EditText emailBoxConfirmation = parent.getConfirmEmailBox();

			parent.runOnUiThread(new Runnable() {
				public void run() {
					parent.showAlert("Registration failed.",
							"Username and email not available.", usernameBox,
							emailBox, emailBoxConfirmation);
				}
			});

		}

	}

}
