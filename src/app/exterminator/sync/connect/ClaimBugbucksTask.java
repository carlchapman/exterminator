package app.exterminator.sync.connect;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import app.exterminator.GUI.control.MarketScreen;
import app.exterminator.GUI.widget.CountView;
import app.exterminator.data.facades.Player;
import app.exterminator.data.utility.Globals;


/**
 * The Class ClaimBugbucksTask is a ConnectionTask that handles the server's
 * response to the name-value-pairs sent to it, which should trigger the server
 * to find any purchased bugbucks in the given user's tuple and then send them
 * to the user on this device before deleting them on the server.
 * 
 * @author carlchapman modified by Cole Groff
 */
public class ClaimBugbucksTask extends AbstractConnectionTask {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * app.exterminator.sync.connect.ConnectionTask#handleResponse(org.apache
	 * .http.HttpResponse, app.exterminator.sync.connect.ConnectionData)
	 */
	@Override
	protected void handleResponse(HttpResponse response, final ConnectionData data) {
		
		// Get the Player and perform an update in case bugbucks aren't
		// synced with the server-side database.
		final Player player = ((Globals) parent.getApplication()).getPlayer();

		Document doc = null;
		String xmlResponse = null;
		HttpEntity resEntity = response.getEntity();
		ServerResponseParser parser = new ServerResponseParser();
		
		try {
			xmlResponse = EntityUtils.toString(resEntity);
			System.out.println("XML RESPONSE: " + xmlResponse);
			doc = parser.getDomElement(xmlResponse);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		NodeList nl = doc.getElementsByTagName("ClaimBugbucksResponse");
		Element e = (Element) nl.item(0);
		player.addBugBucks(Integer.parseInt(parser.getValue(e, "Bugbucks")));
		
		parent.runOnUiThread(new Runnable() {
			public void run() {
				CountView counter = ((MarketScreen)data.getParent()).getCounter();
				counter.setNBugbucks(player.getBugBucks());
				counter.invalidate();	
				}
		});
		

	}
}
