package app.exterminator.sync.connect;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.widget.EditText;
import app.exterminator.GUI.control.LevelSelectScreen;
import app.exterminator.data.facades.DatabaseAdapter;
import app.exterminator.data.facades.Player;
import app.exterminator.data.structures.StarData;
import app.exterminator.data.utility.Globals;

/**
 * The Class LoginTask extends ConnectionTask and so it must implement
 * handleResponse(...) by informing the user what has happened and if login was
 * successful, launching the LevelSelectScreen.
 * 
 * @author carlchapman modified by Cole Groff
 */
public class LoginTask extends AbstractConnectionTask {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * app.exterminator.sync.connect.ConnectionTask#handleResponse(org.apache
	 * .http.HttpResponse, app.exterminator.sync.connect.ConnectionData)
	 */
	@Override
	protected void handleResponse(HttpResponse response, ConnectionData data) {

		Document doc = null;
		String xmlResponse = null;
		HttpEntity resEntity = response.getEntity();
		int responseType;

		// //////////////////////////////////////////////////////////////////////////////////
		// Parse response from the server.

		ServerResponseParser parser = new ServerResponseParser();

		try {
			xmlResponse = EntityUtils.toString(resEntity);
			System.out.println("XML RESPONSE: " + xmlResponse);
			doc = parser.getDomElement(xmlResponse);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		NodeList nl = doc.getElementsByTagName("AndroidLoginResponse");
		Element e = (Element) nl.item(0);

		// Capture response type.
		responseType = Integer.parseInt(e.getAttribute("type"));
		System.out.println("AndroidLoginResponse type: " + responseType);

		// Response types:
		// type = 0: successful login
		// type = 1: username and/or password incorrect
		// type = 2: already logged in

		if (0 == responseType) { // successful login

			Player player = ((Globals) parent.getApplication()).getPlayer();

//			if (!player.isGuest()) {
//				// If the old player was not a guest, then remember their token.
//				String rememberedToken = player.getToken();
//			}
//
//			// Receive all the data from the server, change this player into
//			// that player.
//			// Change the Player object's information, which will also update
//			// the database.
//			//TODO -player.setCoins(Integer.parseInt(parser.getValue(e, "Coins")));
//			player.setBugBucks(Integer.parseInt(parser.getValue(e, "Bugbucks")));
//			player.setToken(parser.getValue(e, "Token"));
//			player.setUsername(parser.getValue(e, "Username"));
//			player.setIsGuest(false);
//			player.setSoundOff(false);
//			player.setMusicOff(true);
//			player.resetUpdateMap();
//			//TODO - get the information from the server about this player's progress data
			
//			
//			System.out.println("starData size: " + starData.getLength());
//			System.out.println("towerData size: " + unlockData.getLength());
			
			//TODO - reset star data in preparation for loading logging in user's star data
			//TODO -  reset unlock data in preparation for loading the logging in user's unlock data
			
			//

			
			
			//TODO - if the old player was not a guest, add a LogoutUpdate to the
			// newly registered user's updatemap
			//player.performUpdate();
			
			String oldToken = player.getToken();
			@SuppressWarnings("unused")
			DatabaseAdapter db = player.getDatabaseAdapter();
			
			if(player.isGuest()){
				player.setUsername(parser.getValue(e, "Username"));
				player.setToken(parser.getValue(e, "AuthToken"));
				player.setBugBucks(Integer.parseInt(parser.getValue(e, "Bugbucks")));
				player.setIsGuest(false);
				player.blankSlate();
				NodeList starData = doc.getElementsByTagName("StarData");
				NodeList unlockData = doc.getElementsByTagName("UnlockData");
				for(int i = 0; i < starData.getLength(); i++) {
					Element s = (Element) starData.item(i);
					player.updateStar(Integer.parseInt(parser.getValue(s, "LevelID")), StarData.LEFT, Integer.parseInt(parser.getValue(s, "Star0")));
					player.updateStar(Integer.parseInt(parser.getValue(s, "LevelID")), StarData.CENTER, Integer.parseInt(parser.getValue(s, "Star1")));
					player.updateStar(Integer.parseInt(parser.getValue(s, "LevelID")), StarData.RIGHT, Integer.parseInt(parser.getValue(s, "Star2")));
				}
				for(int i = 0; i < unlockData.getLength(); i++) {
					Element u = (Element) unlockData.item(i);
					player.updateUnlockData(parser.getValue(u, "TowerID"),parser.getValue(u, "TowerState"));
				}
			}else{
				player.setUsername(parser.getValue(e, "Username"));
				player.setToken(parser.getValue(e, "AuthToken"));
				player.setBugBucks(Integer.parseInt(parser.getValue(e, "Bugbucks")));
				player.blankSlate();
				NodeList starData = doc.getElementsByTagName("StarData");
				NodeList unlockData = doc.getElementsByTagName("UnlockData");
				for(int i = 0; i < starData.getLength(); i++) {
					Element s = (Element) starData.item(i);
					player.updateStar(Integer.parseInt(parser.getValue(s, "LevelID")), StarData.LEFT, Integer.parseInt(parser.getValue(s, "Star0")));
					player.updateStar(Integer.parseInt(parser.getValue(s, "LevelID")), StarData.CENTER, Integer.parseInt(parser.getValue(s, "Star1")));
					player.updateStar(Integer.parseInt(parser.getValue(s, "LevelID")), StarData.RIGHT, Integer.parseInt(parser.getValue(s, "Star2")));
				}
				for(int i = 0; i < unlockData.getLength(); i++) {
					Element u = (Element) unlockData.item(i);
					player.updateUnlockData(parser.getValue(u, "TowerID"),parser.getValue(u, "TowerState"));
				}
				player.logoutOldUser(oldToken);
				player.performUpdate();
			}

			parent.runOnUiThread(new Runnable() {
				public void run() {
					parent.warnLaunchDialog(parent, "Successful Login", "You have logged in successfully", parent.new StartActivityOnClickListener(LevelSelectScreen.class,true), false);
				}
			});

		} else if (1 == responseType) { // username and/or password incorrect

			final EditText usernameBox = parent.getUsernameBox();
			final EditText passwordBox = parent.getPasswordBox();

			parent.runOnUiThread(new Runnable() {
				public void run() {
					parent.showAlert("Login failed.",
							"Username and/or password incorrect.", usernameBox,
							passwordBox, null);
				}
			});

		} else if (2 == responseType) { // already logged in

			final EditText usernameBox = parent.getUsernameBox();
			final EditText passwordBox = parent.getPasswordBox();

			System.out.println("Inside response type 2.");

			parent.runOnUiThread(new Runnable() {
				public void run() {
					parent.showAlert("Login failed.",
							"User is already logged in.", usernameBox,
							passwordBox, null);
				}
			});
		}
	}
}
