package app.exterminator.sync.connect;

import java.util.List;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * The Class ConnectionData is a package that holds information being passed
 * from an Activity to a ConnectionTask. The fields are: parent, progressDialog,
 * nameValuePairs, URLpath and target class to be launched when complete.
 * 
 * @author carlchapman
 */
public class ConnectionData {

	/** The parent. */
	private Activity parent;

	/** The progress dialog. */
	private ProgressDialog progressDialog;

	/** The name value pairs. */
	private List<NameValuePair> nameValuePairs;

	/** The path. */
	private String path;

	/** The target. */
	@SuppressWarnings("rawtypes")
	private Class target;

	/**
	 * Instantiates a new connection data.
	 * 
	 * @param parent
	 *            the parent
	 * @param progressDialog
	 *            the progress dialog
	 * @param nameValuePairs
	 *            the name value pairs
	 * @param path
	 *            the path
	 * @param target
	 *            the target
	 */
	@SuppressWarnings("rawtypes")
	public ConnectionData(Activity parent, ProgressDialog progressDialog,
			List<NameValuePair> nameValuePairs, String path, Class target) {
		this.parent = parent;
		this.progressDialog = progressDialog;
		this.nameValuePairs = nameValuePairs;
		this.path = path;
		this.target = target;
	}

	/**
	 * Gets the parent.
	 * 
	 * @return the parent
	 */
	public Activity getParent() {
		return parent;
	}

	/**
	 * Gets the progress dialog.
	 * 
	 * @return the progress dialog
	 */
	public ProgressDialog getProgressDialog() {
		return progressDialog;
	}

	/**
	 * Gets the name value pairs.
	 * 
	 * @return the name value pairs
	 */
	public List<NameValuePair> getNameValuePairs() {
		return nameValuePairs;
	}

	/**
	 * Gets the path.
	 * 
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Gets the target.
	 * 
	 * @return the target
	 */
	@SuppressWarnings("rawtypes")
	public Class getTarget() {
		return target;
	}

}
