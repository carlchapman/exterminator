package app.exterminator.sync.update;

/**
 * The Class Update is a format for other updates to follow, and holds the
 * constants that they use to identify themselves with.
 * 
 * @author carlchapman
 */
public abstract class AbstractUpdate implements ToXML {

	/** The Constant BUGBUCK_UPDATE. */
	public static final int BUGBUCK_UPDATE = 0;

	/** The Constant LEVEL_UPDATE. */
	public static final int STAR_UPDATE = 2;

	/** The Constant TOWER_UPDATE. */
	public static final int UNLOCK_UPDATE = 3;

	/** The Constant LOGOUT_UPDATE. */
	public static final int LOGOUT_UPDATE = 4;

	/**
	 * Gets the update type.
	 * 
	 * @return the update type
	 */
	public abstract int getUpdateType();

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.ToXML#toXML()
	 */
	public String toXML() {
		return "<Update type='" + getUpdateType() + "'>";
	}

	// example update XML:

	/*
	 * <Updates> <Update type=0><numberOfBugBucks>4</numberOfBugBucks></Update>
	 * <Update type=1><numberOfCoins>1001</numberOfCoins></Update> <Update
	 * type=2><levelID>0</levelID><S1>2</S1><S2>2</S2><S3>0</S3></Update>
	 * <Update
	 * type=2><levelID>1</levelID><S1>1</S1><S2>1</S2><S3>0</S3></Update>
	 * <Update
	 * type=2><levelID>2</levelID><S1>1</S1><S2>0</S2><S3>0</S3></Update>
	 * <Update
	 * type=2><levelID>3</levelID><S1>1</S1><S2>1</S2><S3>1</S3></Update>
	 * <Update type=3><towerID>0</TowerID><towerState>1</towerState></Update>
	 * <Update type=3><towerID>2</TowerID><towerState>1</towerState></Update>
	 * <Update type=3><towerID>3</TowerID><towerState>1</towerState></Update>
	 * <Update type=3><towerID>6</TowerID><towerState>1</towerState></Update>
	 * <Update type=3><towerID>8</TowerID><towerState>1</towerState></Update>
	 * <Update type="4"><token>moa3om94mclij8884ooie</token></Update> <---
	 * logout update </Updates>
	 */

}
