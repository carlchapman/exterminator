package app.exterminator.sync.update;


/**
 * The Interface ToXML simply specifies that a class must agree to create an XML
 * string representation of itself.
 * 
 * @author carlchapman
 */
public interface ToXML {

	/**
	 * To xml.
	 * 
	 * @return the string
	 */
	public String toXML();

}
