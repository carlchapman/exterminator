package app.exterminator.sync.update;

import app.exterminator.data.structures.StarData;

/**
 * The Class StarUpdate holds information about a certain level's star values,
 * and then is converted to XML and sent to the server.
 * 
 */
public class StarUpdate extends AbstractUpdate {

	/** The level data. */
	private StarData starData;

	/**
	 * Instantiates a new star update.
	 * 
	 * @param starData
	 *            the level data
	 */
	public StarUpdate(StarData starData) {
		this.starData = starData;
	}

	/**
	 * Instantiates a new star update.
	 * 
	 * @param levelID
	 *            the level id
	 * @param s1
	 *            the s1
	 * @param s2
	 *            the s2
	 * @param s3
	 *            the s3
	 */
	public StarUpdate(int levelID, int s1, int s2, int s3) {
		this.starData = new StarData(levelID, s1, s2, s3);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.Update#getUpdateType()
	 */
	@Override
	public int getUpdateType() {
		return STAR_UPDATE;
	}

	// unique hashcode for each level
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/**
	 * The hashcode is very important for all updates - every time an update of
	 * the same type is put into the UpdateMap using the hashcode to generate
	 * the key, it replaces the old update, thus limiting the size of the map
	 * and only allowing the most current state to be updated at any time.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = getUpdateType();
		result = prime * result + starData.getLevelID();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StarUpdate other = (StarUpdate) obj;
		if (starData == null) {
			if (other.starData != null)
				return false;
		} else if (!starData.equals(other.starData))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.Update#toXML()
	 */
	@Override
	public String toXML() {
		return super.toXML() + starData.toXML() + "</Update>";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LevelUpdate [starData=" + starData + "]";
	}

}
