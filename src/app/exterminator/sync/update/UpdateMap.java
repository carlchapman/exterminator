package app.exterminator.sync.update;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * The Class UpdateMap is a HashMap with IntegerKeys and values that extend
 * AbstractUpdate. This map holds the most recent changes that need to be backed
 * up on a server. It has synchronized methods so that the threads that access
 * it cannot interleave.
 * 
 * The update() method is particularly important. It parses the server response
 * that reflects the current state of the database and then constructs temporary
 * updates from that information, and then iterates through this list of
 * temporary updates and removes any equal updates from this map, thus ensuring
 * that if the connection was interrupted so that only some of the updates when
 * through, they would stay in the map after update() completed, hopefully
 * getting to the server next time.
 * 
 * Also, the entire UpdateMap can be converted to XML and stored in the local
 * data, and it can be re-assembled from an XML string.
 * 
 * @author carlchapman
 */
@SuppressWarnings("serial")
public class UpdateMap extends HashMap<Integer, AbstractUpdate> implements
		ToXML {

	/**
	 * If an update has taken this long, then it is time to stop trying to
	 * update.
	 */
	public static final long TIMEOUT = 20000;
	/**
	 * An XML document builder.
	 */
	private static final DocumentBuilderFactory dbf = DocumentBuilderFactory
			.newInstance();

	/**
	 * default constructor.
	 */
	public UpdateMap() {
		super();
	}

	/**
	 * Creates a new UpdateMap from an xml representation. Used to reload the
	 * map from stored memory.
	 * 
	 * @param xmlRepresentation
	 *            the xml representation
	 */
	public UpdateMap(String xmlRepresentation) {
		super();
		if (xmlRepresentation != null && !xmlRepresentation.equals("")) {
			System.out.println("how we get here?");
			InputSource inputSource = new InputSource();
			inputSource.setCharacterStream(new StringReader(xmlRepresentation));
			ArrayList<AbstractUpdate> updatesFromXML = getUpdatesFromXML(inputSource);
			for (AbstractUpdate u : updatesFromXML)
				this.put(u.hashCode(), u);
		}
	}

	/**
	 * gets a String containing all the updates in this UpdateMap in XML form.
	 * 
	 * @return A String of updates in XML form
	 */
	@Override
	public String toXML() {
		String xmlContentToSend = "<Updates>";
		for (AbstractUpdate x : this.values())
			xmlContentToSend += x.toXML();
		xmlContentToSend += "</Updates>";
		// System.out.println("xmlContentToSend: " + xmlContentToSend);
		return xmlContentToSend;
	}

	/**
	 * Puts a given update into the UpdateMap, and also puts a coin update.
	 * Updates always replace updates that exist in the map with the same
	 * hashcode, which is why the hashcode function of the updates has been
	 * carefully constructed so that different levels get different codes but
	 * not the same level with different state.
	 * 
	 * @param value
	 *            The update to put into the map
	 * @param coins
	 *            The current number of coins a user has
	 * @return the replaced value, if any
	 */
	public synchronized Object putUpdate(AbstractUpdate value) {
		// CoinUpdate coinUpdate = new CoinUpdate(coins);
		// this.put(coinUpdate.hashCode(), coinUpdate);
		System.out.println("putting update: " + value.toXML());
		return this.put(value.hashCode(), value);
	}

	/**
	 * update() tries to connect to the server to deliver updates, and checks
	 * the server state for the given player to make sure that it reflects the
	 * current database state. If the database information reflects information
	 * that was in an update, that update is removed from this UpdateMap. If
	 * there are still updates to make after the initial attempt, up to two more
	 * attempts will be made as long as it doesn't take less than TIMEOUT time.
	 * If no connection is made, then Updates should stay in the UpdateMap so
	 * that they can try to reach the database when another update() call is
	 * made.
	 * 
	 * Also, if this user is a guest that holds a logout update for an old user,
	 * then they connect using the old token only to log that user out.
	 * 
	 * @param token
	 *            The authentication token of the user.
	 * @return True if successful, false otherwise.
	 */
	public synchronized boolean update(String token, boolean isGuest) {

		// if this is a registered user, then perform an update
		if (!isGuest) {
			return connect(token, false);

			// if this is a guest with a logout update in their map, then send
			// only that to the server
		} else if (null != get(AbstractUpdate.LOGOUT_UPDATE)) {
			return connect(
					((LogoutUpdate) get(AbstractUpdate.LOGOUT_UPDATE))
							.getToken(),
					true);
		}
		return false;
	}

	private boolean connect(String token, boolean isLogoutOnly) {
		boolean updateSuccessful = false;
		int attemptNumber = 0;
		String urlArgument = "http://colegroff.com/cs309/exterminator/update/index?auth_token=" + token;
//		String urlArgument = "http://10.0.2.2:3000/update/index?auth_token=" + token;

		// set up the httpPost
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(urlArgument);
		httpPost.addHeader("Accept", "text/xml");
		httpPost.addHeader("Content-Type", "application/xml");

		// System.out.println("httpPost: " + httpPost);
		System.out.println("attempting update with token: " + token);
		// System.out.println(this.toString());

		// try until 3 attempts or timeout or successful
		long startTime = System.currentTimeMillis();
		while (attemptNumber < 3 && !updateSuccessful
				&& System.currentTimeMillis() - startTime < TIMEOUT) {
			System.out.println("Attempt number:" + attemptNumber);
			attemptNumber++;

			// set the Entity of httpPost to the XML, execute, check the
			// response
			try {
				if (isLogoutOnly) {
					httpPost.setEntity(new StringEntity("<Updates>"
							+ get(AbstractUpdate.LOGOUT_UPDATE).toXML()
							+ "</Updates>"));
				} else {
					httpPost.setEntity(new StringEntity(toXML()));
				}

				HttpResponse response = httpClient.execute(httpPost);
				updateSuccessful = interpretResponse(response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (updateSuccessful)
			System.out.println("Map is empty!!!!");
		return updateSuccessful;
	}

	/**
	 * clears this UpdateMap in a synchronized fashion
	 */
	@Override
	public synchronized void clear() {
		super.clear();
	}

	/**
	 * Parses an httpResponse object by getting the xml message it sent and
	 * creating updates from that information. If this UpdateMap contains
	 * updates equal to any in that collection, they are removed from this
	 * UpdateMap
	 * 
	 * @param response
	 *            the response
	 * @return true, if successful
	 */
	private boolean interpretResponse(HttpResponse response) {
		ArrayList<AbstractUpdate> currentMap = new ArrayList<AbstractUpdate>(
				this.values());
		// get the InputSource from the response
		InputSource inputSource = null;
		try {
			inputSource = new InputSource(response.getEntity().getContent());
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ArrayList<AbstractUpdate> updatesFromServer = getUpdatesFromXML(inputSource);

		// remove every update from the UpdateMap if an equal Update exists in
		// the list created from the server's current state
		for (AbstractUpdate x : currentMap)
			if (updatesFromServer.contains(x)) // true if x.equals(any)
				this.remove(x.hashCode());

		return this.isEmpty();
	}

	/**
	 * Gets the updates from xml.
	 * 
	 * @param inputSource
	 *            the input source
	 * @return the updates from xml
	 */
	private ArrayList<AbstractUpdate> getUpdatesFromXML(InputSource inputSource) {
		Document responseDocument = null;
		// System.out.println("getting inputSource from response");
		DocumentBuilder documentBuilder;
		try {
			documentBuilder = dbf.newDocumentBuilder();
			// System.out.println("parsing input to create document");
			responseDocument = documentBuilder.parse(inputSource);
			// System.out.println("document created: "+getStringFromDocument(responseDocument));
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		NodeList updateNodes = responseDocument.getElementsByTagName("Update");
		ArrayList<AbstractUpdate> updatesFromInput = new ArrayList<AbstractUpdate>();

		// iterate over the updatedNodes, creating a collection of Updates from
		// them
		int numberNodes = updateNodes.getLength();
		for (int i = 0; i < numberNodes; i++) {
			System.out.println("i: " + i);
			Element e = (Element) updateNodes.item(i); // is this cast ok?
			updatesFromInput.add(createTempUpdate(e));
		}
		return updatesFromInput;
	}

	/**
	 * Creates an update given an element of a Document-Object-Model XML
	 * document.
	 * 
	 * @param e
	 *            The given element.
	 * @return The update that would be created to convey the state of the
	 *         database.
	 */
	private AbstractUpdate createTempUpdate(Element e) {
		int type = Integer.parseInt(e.getAttribute("type"));
		System.out.println("creating update of type: " + type);
		AbstractUpdate toReturn = null;
		switch (type) {
		case AbstractUpdate.BUGBUCK_UPDATE:
			int numberOfBugBucks = Integer.parseInt(e
					.getElementsByTagName("numberOfBugBucks").item(0)
					.getTextContent());
			toReturn = new BugBuckUpdate(numberOfBugBucks);
			break;
		case AbstractUpdate.STAR_UPDATE:
			int levelID = Integer.parseInt(e.getElementsByTagName("levelID")
					.item(0).getTextContent());
			int S1 = Integer.parseInt(e.getElementsByTagName("S1").item(0)
					.getTextContent());
			int S2 = Integer.parseInt(e.getElementsByTagName("S2").item(0)
					.getTextContent());
			int S3 = Integer.parseInt(e.getElementsByTagName("S3").item(0)
					.getTextContent());
			toReturn = new StarUpdate(levelID, S1, S2, S3);//
			break;
		case AbstractUpdate.UNLOCK_UPDATE:
			int towerID = Integer.parseInt(e.getElementsByTagName("towerID")
					.item(0).getTextContent());
			int towerState = Integer.parseInt(e
					.getElementsByTagName("towerState").item(0)
					.getTextContent());
			toReturn = new TowerUpdate(towerID, towerState);
			break;
		case AbstractUpdate.LOGOUT_UPDATE:
			String oldToken = e.getElementsByTagName("AuthToken").item(0)
					.getTextContent();
			toReturn = new LogoutUpdate(oldToken);
			break;
		}
		System.out.println("update created from response with xml: "
				+ toReturn.toXML());
		return toReturn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractMap#toString()
	 */
	@Override
	public String toString() {
		String representation = "";
		for (AbstractUpdate x : this.values())
			representation += x.toXML() + ", ";
		return representation;
	}

	// method to convert Document to String
	/**
	 * Gets the document as a string - for debugging and viewing the document.
	 * 
	 * @param doc
	 *            the doc
	 * @return the string from document
	 */
	public String getStringFromDocument(Document doc) {
		try {
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (TransformerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

}
