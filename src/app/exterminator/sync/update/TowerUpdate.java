package app.exterminator.sync.update;

import app.exterminator.data.structures.UnlockData;

/**
 * The Class TowerUpdate holds information about the unlocking of towers - it is
 * converted to XML and sent to the server.
 * 
 * @author carlchapman
 */
public class TowerUpdate extends AbstractUpdate {

	/** The tower data. */
	private UnlockData towerData;

	/**
	 * Instantiates a new tower update.
	 * 
	 * @param towerData
	 *            the tower data
	 */
	public TowerUpdate(UnlockData towerData) {
		this.towerData = towerData;
	}

	/**
	 * Instantiates a new tower update.
	 * 
	 * @param towerID
	 *            the tower id
	 * @param towerState
	 *            the tower state
	 */
	public TowerUpdate(int towerID, int towerState) {
		this.towerData = new UnlockData(towerID, towerState);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.Update#getUpdateType()
	 */
	@Override
	public int getUpdateType() {
		return UNLOCK_UPDATE;
	}

	// unique hashcode for each type of tower
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/**
	 * The hashcode is very important for all updates - every time an update of
	 * the same type is put into the UpdateMap using the hashcode to generate
	 * the key, it replaces the old update, thus limiting the size of the map
	 * and only allowing the most current state to be updated at any time.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = getUpdateType();
		result = prime * result + towerData.getTowerID();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TowerUpdate other = (TowerUpdate) obj;
		if (towerData == null) {
			if (other.towerData != null)
				return false;
		} else if (!towerData.equals(other.towerData))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.Update#toXML()
	 */
	@Override
	public String toXML() {
		return super.toXML() + towerData.toXML() + "</Update>";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TowerUpdate [towerData=" + towerData + "]";
	}

}
