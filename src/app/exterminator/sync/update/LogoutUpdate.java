package app.exterminator.sync.update;

/**
 * The Class LogoutUpdate holds the old user's token, and is sent to the server
 * as XML to try and log the old user out.
 * 
 * @author carlchapman
 */
public class LogoutUpdate extends AbstractUpdate {

	/** The token. */
	private String token;

	/**
	 * Instantiates a new logout update.
	 * 
	 * @param token
	 *            the old user's token
	 */
	public LogoutUpdate(String token) {
		this.token = token;
	}

	/**
	 * Gets the token from this LogoutUpdate
	 * 
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.Update#getUpdateType()
	 */
	@Override
	public int getUpdateType() {
		return LOGOUT_UPDATE;
	}

	/**
	 * The hashcode is very important for all updates - every time an update of
	 * the same type is put into the UpdateMap using the hashcode to generate
	 * the key, it replaces the old update, thus limiting the size of the map
	 * and only allowing the most current state to be updated at any time.
	 * 
	 * @return the int
	 */
	@Override
	public int hashCode() {
		return getUpdateType();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogoutUpdate other = (LogoutUpdate) obj;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}

	/**
	 * To xml.
	 * 
	 * @return the string
	 */
	public String toXML() {
		return super.toXML() + "<AuthToken>" + token + "</AuthToken></Update>";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LogoutUpdate [AuthToken=" + token + "]";
	}
}
