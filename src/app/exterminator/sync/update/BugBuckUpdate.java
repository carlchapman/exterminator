package app.exterminator.sync.update;

/**
 * The Class BugBuckUpdate is an update for bugbucks - it simply holds the
 * number of bugbucks that a player has and is converted to XLM, then sent to
 * the server.
 * 
 * @author carlchapman
 */
public class BugBuckUpdate extends AbstractUpdate {

	/** The number of bug bucks. */
	private int numberOfBugBucks;

	/**
	 * Instantiates a new bug buck update.
	 * 
	 * @param numberOfBugBucks
	 *            the number of bug bucks
	 */
	public BugBuckUpdate(int numberOfBugBucks) {
		this.numberOfBugBucks = numberOfBugBucks;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.Update#getUpdateType()
	 */
	@Override
	public int getUpdateType() {
		return BUGBUCK_UPDATE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/**
	 * The hashcode is very important for all updates - every time an update of
	 * the same type is put into the UpdateMap using the hashcode to generate
	 * the key, it replaces the old update, thus limiting the size of the map
	 * and only allowing the most current state to be updated at any time.
	 */
	@Override
	public int hashCode() {
		return getUpdateType();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BugBuckUpdate other = (BugBuckUpdate) obj;
		if (numberOfBugBucks != other.numberOfBugBucks)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.Update#toXML()
	 */
	@Override
	public String toXML() {
		return super.toXML() + "<numberOfBugBucks>" + numberOfBugBucks
				+ "</numberOfBugBucks></Update>";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BugBuckUpdate [numberOfBugBucks=" + numberOfBugBucks + "]";
	}

}
