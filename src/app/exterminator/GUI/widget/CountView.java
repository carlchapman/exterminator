package app.exterminator.GUI.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import app.exterminator.data.utility.BMPS;

/**
 * The Class CountView is a widget that displays the bugbucks and star icons and
 * prints a corresponding number next to them to communicate to the user how
 * many of each they have.
 * 
 * @author carlchapman
 */
public class CountView extends View {

	/** The Constant PADDING. */
	private static final int PADDING = 6;

	/** The n bugbucks. */
	private int nBugbucks;

	/** The n stars. */
	private int nStars;

	/** The text paint. */
	private Paint textPaint;

	/**
	 * Instantiates a new count view.
	 * 
	 * @param context
	 *            the context
	 */
	public CountView(Context context) {
		this(context, null);
	}

	/**
	 * Constructs a CountView (used by the xml inflator).
	 * 
	 * @param context
	 *            the context
	 * @param attrs
	 *            the attrs
	 */
	public CountView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.nBugbucks = 0;
		this.nStars = 0;

		// initialize textPaint
		textPaint = new Paint();
		textPaint.setTextSize(32);
		textPaint.setFakeBoldText(false);
		textPaint.setColor(Color.WHITE);
	}

	/**
	 * Sets the n bugbucks.
	 * 
	 * @param newBugBucks
	 *            the new n bugbucks
	 */
	public void setNBugbucks(int newBugBucks) {
		this.nBugbucks = newBugBucks;
	}

	/**
	 * Sets the n stars.
	 * 
	 * @param newStars
	 *            the new n stars
	 */
	public void setNStars(int newStars) {
		this.nStars = newStars;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		Bitmap star = BMPS.getIcon(BMPS.STAR);
		Bitmap bugbuck = BMPS.getIcon(BMPS.BUGBUCK);
		int textHeigth = 3 * PADDING + star.getHeight() / 2;

		int left = PADDING;
		canvas.drawBitmap(star, left, PADDING, null);
		left += star.getWidth() + PADDING;
		String starCount = " " + nStars;
		canvas.drawText(starCount, left, textHeigth, textPaint);
		left += 60;
		canvas.drawBitmap(bugbuck, left, PADDING, null);
		left += bugbuck.getWidth() + PADDING;
		String buckCount = " " + nBugbucks;
		canvas.drawText(buckCount, left, textHeigth, textPaint);
	}

}
