package app.exterminator.GUI.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.Button;
import app.exterminator.data.utility.BMPS;
import app.exterminator.data.utility.Globals;

/**
 * The Class StarButton either draws stars on top of a button that reflect the
 * number of stars a user has for the given level, or draws the word 'market' in
 * case of the market screen.
 * 
 * @author carlchapman
 */
public class StarButton extends Button {

	/** The market. */
	private static Bitmap market;

	/** The yellow star. */
	private static Bitmap yellowStar;

	/** The grey star. */
	private static Bitmap greyStar;

	/** The level id. */
	private int levelID;

	/**
	 * Sets the level.
	 * 
	 * @param levelID
	 *            the new level
	 */
	public void setLevel(int levelID) {
		this.levelID = levelID;
	}

	/**
	 * Instantiates a new star button.
	 * 
	 * @param context
	 *            the context
	 * @param attrs
	 *            the attrs
	 */
	public StarButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		levelID = -1;
		market = BMPS.getIcon(BMPS.MARKET);
		yellowStar = BMPS.getIcon(BMPS.STAR);
		greyStar = BMPS.getIcon(BMPS.GREYSTAR);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.TextView#onDraw(android.graphics.Canvas)
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (levelID == 14) {
			canvas.drawBitmap(market, 30, 90, null);
		} else {
			int[] stars = ((Globals) this.getContext().getApplicationContext())
					.getPlayer().getStars(levelID);
			if (stars[0] == 0)
				canvas.drawBitmap(greyStar, 10, 85, null);
			else
				canvas.drawBitmap(yellowStar, 10, 85, null);
			if (stars[1] == 0)
				canvas.drawBitmap(greyStar, 60, 85, null);
			else
				canvas.drawBitmap(yellowStar, 60, 85, null);
			if (stars[2] == 0)
				canvas.drawBitmap(greyStar, 110, 85, null);
			else
				canvas.drawBitmap(yellowStar, 110, 85, null);
		}

	}

}
