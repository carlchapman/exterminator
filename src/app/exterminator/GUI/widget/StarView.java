package app.exterminator.GUI.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import app.exterminator.data.facades.Player;
import app.exterminator.data.utility.BMPS;

/**
 * StarView is a widget that displays all of the user's stars in a box. It
 * switches backgrounds depending on the orientation
 * 
 * @author carlchapman
 * 
 */
public class StarView extends View {

	/** The Constant PADDING. */
	private static final int PADDING = 3;
	
	/** The screen width. */
	private int screenWidth;
	
	/** The n stars. */
	private int nStars;
	
	/** The background. */
	private Bitmap background;

	/**
	 * Instantiates a new star view.
	 *
	 * @param context the context
	 * @param screenWidth the screen width
	 * @param player the player
	 */
	public StarView(Context context, int screenWidth, Player player) {
		this(context, null);

	}

	/**
	 * Instantiates a new star view.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public StarView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.screenWidth = 0;
		this.nStars = 0;
		setBackground();
	}

	// public int getBottomOfStars() {
	// return bottom;
	// }

	/* (non-Javadoc)
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		Bitmap star = BMPS.getIcon(BMPS.STAR);
		int starsPerRow = screenWidth / (star.getWidth() + PADDING);
		int starsThisRow = 0;
		int starsDrawn = 0;
		int top = 6 * PADDING + 2;
		int left = 5 * PADDING;
		canvas.drawBitmap(background, 0, 0, null);

		int orientation = getResources().getConfiguration().orientation;
		if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
			starsPerRow--;
		}

		// while there are stars left to draw
		while (nStars > starsDrawn) {

			// while there are stars left to draw and room in this row
			while (starsThisRow < starsPerRow && nStars > starsDrawn) {

				// draw the stars in this row
				canvas.drawBitmap(star, left, top, null);
				left += star.getWidth() + PADDING;
				starsDrawn++;
				starsThisRow++;
			}

			// prepare to start a new row
			starsThisRow = 0;
			top += star.getHeight() + 2 * PADDING + 2;
			left = 5 * PADDING;
		}
	}

	/**
	 * Sets the background.
	 */
	public void setBackground() {
		int orientation = getResources().getConfiguration().orientation;
		if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
			background = BMPS.getBackground(BMPS.STAR_VIEW_LANDSCAPE);
		} else if (orientation == Configuration.ORIENTATION_PORTRAIT) {
			background = BMPS.getBackground(BMPS.STAR_VIEW_PORTRAIT);
		}
	}

	/**
	 * Sets the screen width.
	 *
	 * @param widthPixels the new screen width
	 */
	public void setScreenWidth(int widthPixels) {
		this.screenWidth = widthPixels;
	}

	/**
	 * Sets the n stars.
	 *
	 * @param nStars the new n stars
	 */
	public void setNStars(int nStars) {
		this.nStars = nStars;
	}

}