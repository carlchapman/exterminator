package app.exterminator.GUI.control;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import app.exterminator.R;
import app.exterminator.data.utility.Globals;
import app.exterminator.sync.connect.AbstractConnectionTask;

/**
 * All GUI control screens extend the AbstractControl Screen, providing them all
 * with menu functionality and commonly used dialogs. Some EditTexts have also
 * been added to help with the RegistrationTask's clearing of text boxes in the
 * case of a username or email or both being taken.
 * 
 * @author carlchapman
 */
public abstract class AbstractControlScreen extends Activity {

	/** The username box. */
	protected EditText usernameBox;

	/** The email box. */
	protected EditText emailBox;

	/** The confirm email box. */
	protected EditText confirmEmailBox;

	/** The password box. */
	protected EditText passwordBox;

	// ///////////////////Register Helpers////////////////

	/**
	 * Gets the username box.
	 * 
	 * @return the username box
	 */
	public EditText getUsernameBox() {
		return usernameBox;
	}

	/**
	 * Gets the email box.
	 * 
	 * @return the email box
	 */
	public EditText getEmailBox() {
		return emailBox;
	}

	/**
	 * Gets the confirm email box.
	 * 
	 * @return the confirm email box
	 */
	public EditText getConfirmEmailBox() {
		return confirmEmailBox;
	}

	/**
	 * Gets the password box.
	 * 
	 * @return the password box
	 */
	public EditText getPasswordBox() {
		return passwordBox;
	}

	// //////////////////Dialogs//////////////////////////

	/**
	 * Shows an alert and then clears up to three EditText view elements, and if
	 * no such elements are needed, those arguments can be null.
	 * 
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 * @param toClear1
	 *            the EditText to clear 1
	 * @param toClear2
	 *            the EditText to clear 2
	 * @param toClear3
	 *            the EditText to clear 3
	 */
	public void showAlert(String title, String message, EditText toClear1,
			EditText toClear2, EditText toClear3) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setButton("OK", new ClearOnClickListener(toClear1,
				toClear2, toClear3));
		alertDialog.show();
	}

	/**
	 * Gets a progress dialog that can cancel a ConnectionTask if the user
	 * presses the cancel button.
	 * 
	 * @param caller
	 *            the calling Activity
	 * @param title
	 *            the title
	 * @param toCancel
	 *            the ConnectionTask to cancel
	 * @return the progress dialog
	 */
	protected ProgressDialog getProgressDialog(Activity caller, String title,
			AbstractConnectionTask toCancel) {
		ProgressDialog dialog = new ProgressDialog(caller);
		dialog.setTitle(title);
		dialog.setMessage("Starting");
		dialog.setCancelable(true);
		dialog.setButton("Cancel", new CancelConnectionListener(toCancel));
		dialog.show();
		return dialog;
	}

	/**
	 * Launches a warning dialog that allows the user to back out of doing
	 * something, or if they press ok, then the given listener will be
	 * triggered.
	 * 
	 * @param current
	 *            the current Activity
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 * @param listener
	 *            the listener
	 */
	public void warnLaunchDialog(Activity current, String title,
			String message, StartActivityOnClickListener listener,boolean cancelable) {
		AlertDialog alertDialog = new AlertDialog.Builder(current).create();
		alertDialog.setTitle(title);
		alertDialog.setButton("OK", listener);
		if(cancelable){
			alertDialog.setButton2("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
		}

		alertDialog.show();
	}

	// //////////////////Menu//////////////////////////

	/** The Constant MENU_SOUND. */
	public static final int MENU_SOUND = 0;

	/** The Constant MENU_MUSIC. */
	public static final int MENU_MUSIC = 1;

	/** The Constant MENU_QUIT. */
	public static final int MENU_QUIT = 2;

	/** The Constant MENU_REGISTER. */
	public static final int MENU_REGISTER = 3;
	
	/** The Constant MENU_SHOWHELP. */
	public static final int MENU_SHOWHELP = 4;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return menuChoice(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		if (((Globals) getApplicationContext()).getPlayer().isSoundOff())
			menu.add(0, MENU_SOUND, 0, "Sound").setIcon(R.drawable.a_sound);
		else
			menu.add(0, MENU_SOUND, 0, "Sound").setIcon(R.drawable.a_sound_off);
		if (((Globals) getApplicationContext()).getPlayer().isMusicOff())
			menu.add(0, MENU_MUSIC, 0, "Music").setIcon(R.drawable.a_music);
		else
			menu.add(0, MENU_MUSIC, 0, "Music").setIcon(R.drawable.a_music_off);
		menu.add(0, MENU_QUIT, 0, "Quit").setIcon(R.drawable.a_quit);
		if (((Globals) getApplicationContext()).getPlayer().isGuest())
			menu.add(0, MENU_REGISTER, 0, "Register").setIcon(
					R.drawable.a_register);
		return true;
	}

	/**
	 * Menu choice is a helper method that decides what to do after the user
	 * chooses a menu item.
	 * 
	 * @param item
	 *            the item that the user chose
	 * @return true, if successful
	 */
	private boolean menuChoice(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			if (((Globals) getApplicationContext()).getPlayer().isSoundOff()) {
				item.setIcon(R.drawable.a_sound_off);
				((Globals) getApplicationContext()).getPlayer().setSoundOff(
						false);
			} else {
				item.setIcon(R.drawable.a_sound);
				((Globals) getApplicationContext()).getPlayer().setSoundOff(
						true);
			}
			return true;
		case 1:
			if (((Globals) getApplicationContext()).getPlayer().isMusicOff()) {
				item.setIcon(R.drawable.a_music_off);
				((Globals) getApplicationContext()).getPlayer().setMusicOff(
						false);
			} else {
				item.setIcon(R.drawable.a_music);
				((Globals) getApplicationContext()).getPlayer().setMusicOff(
						true);
			}
			return true;
		case 2:
			warnLaunchDialog(this, "Confirm Quit",
					"Are you sure you want to quit?",
					new StartActivityOnClickListener(StartScreen.class, true),true);
			return true;
		case 3:
			warnLaunchDialog(this, "Confirm Register",
					"Are you sure you want to register?",
					new StartActivityOnClickListener(RegisterScreen.class),true);
			return true;
		}
		return false;
	}

	// ////////////OnClickListeners(for dialogs, etc.)/////////////

	/**
	 * A listener that clears the given EditText fields when OnClick(...) is
	 * triggered
	 * 
	 * @see ClearOnClickEvent
	 */
	class ClearOnClickListener implements OnClickListener {

		/** The to clear1. */
		private EditText toClear1;

		/** The to clear2. */
		private EditText toClear2;

		/** The to clear3. */
		private EditText toClear3;

		/**
		 * Instantiates a new clear on click listener.
		 * 
		 * @param toClear1
		 *            the to clear1
		 * @param toClear2
		 *            the to clear2
		 * @param toClear3
		 *            the to clear3
		 */
		public ClearOnClickListener(EditText toClear1, EditText toClear2,
				EditText toClear3) {
			this.toClear1 = toClear1;
			this.toClear2 = toClear2;
			this.toClear3 = toClear3;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.content.DialogInterface.OnClickListener#onClick(android.content
		 * .DialogInterface, int)
		 */
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (toClear1 != null)
				toClear1.setText("");
			if (toClear2 != null)
				toClear2.setText("");
			if (toClear3 != null)
				toClear3.setText("");
		}

	}

	/**
	 * A listener that cancels a ConnectionTask by calling cancel(false) on the
	 * ConnectionTask
	 * 
	 * @see CancelConnectionEvent
	 */
	class CancelConnectionListener implements OnClickListener {

		/** The ConnectionTask to cancel. */
		private AbstractConnectionTask toCancel;

		/**
		 * Instantiates a new cancel connection listener.
		 * 
		 * @param toCancel
		 *            the to cancel
		 */
		public CancelConnectionListener(AbstractConnectionTask toCancel) {
			this.toCancel = toCancel;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.content.DialogInterface.OnClickListener#onClick(android.content
		 * .DialogInterface, int)
		 */
		public void onClick(DialogInterface arg0, int arg1) {
			toCancel.cancel(false);
		}
	}

	/**
	 * A listener that will start a given activity, and optionally set the
	 * intent flag to FLAG_ACTIVITY_CLEAR_TOP if the constructor with a boolean
	 * is called and that boolean is true. This flag will clear all Activities
	 * on the Activity stack except the one launched and the root. This is used
	 * by the menu when a user presses the 'quit' button.
	 * 
	 * @see StartActivityOnClickEvent
	 */
	public class StartActivityOnClickListener implements OnClickListener {

		/** The target. */
		@SuppressWarnings("rawtypes")
		private Class target;

		/** The clear top boolean. */
		private boolean clearTop;

		/**
		 * Instantiates a new start activity on click listener.
		 * 
		 * @param target
		 *            the target
		 */
		@SuppressWarnings("rawtypes")
		public StartActivityOnClickListener(Class target) {
			this(target, false);
		}

		/**
		 * Instantiates a new start activity on click listener.
		 * 
		 * @param target
		 *            the target
		 * @param clearTop
		 *            the clear top
		 */
		@SuppressWarnings("rawtypes")
		public StartActivityOnClickListener(Class target, boolean clearTop) {
			this.target = target;
			this.clearTop = clearTop;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.content.DialogInterface.OnClickListener#onClick(android.content
		 * .DialogInterface, int)
		 */
		@Override
		public void onClick(DialogInterface dialog, int which) {
			Intent myIntent = new Intent(getBaseContext(), target);
			if (clearTop)
				myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			startActivityForResult(myIntent, 0);
		}
	}

}
