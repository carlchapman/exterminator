package app.exterminator.GUI.control;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import app.exterminator.R;
import app.exterminator.GUI.widget.StarButton;

/**
 * The Class LevelSelectScreen displays custom buttons that allow the user to
 * see how many stars they have earned for a given level, and which open that
 * level when the user presses them. There is also a button that goes to the
 * market screen.
 * 
 * Note that all of the buttons (except the market button) launch same Activity
 * and that a distinct level is chosen by putting an 'extra' into the intent
 * bundle which is recovered by the LevelScreen and used to create the correct
 * map that corresponds to that levelID.
 * 
 * @author carlchapman modified by Cole Groff
 */
public class LevelSelectScreen extends AbstractControlScreen {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.levelselectscreen);

		StarButton level0Button = (StarButton) findViewById(R.id.level0Button);
		level0Button.setLevel(0);
		level0Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 0;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});

		StarButton level1Button = (StarButton) findViewById(R.id.level1Button);
		level1Button.setLevel(1);
		level1Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 1;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});
		StarButton level2Button = (StarButton) findViewById(R.id.level2Button);
		level2Button.setLevel(2);
		level2Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 2;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});
		StarButton level3Button = (StarButton) findViewById(R.id.level3Button);
		level3Button.setLevel(3);
		level3Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 3;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});
		StarButton level4Button = (StarButton) findViewById(R.id.level4Button);
		level4Button.setLevel(4);
		level4Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 4;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});
		StarButton level5Button = (StarButton) findViewById(R.id.level5Button);
		level5Button.setLevel(5);
		level5Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 5;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});

		StarButton level6Button = (StarButton) findViewById(R.id.level6Button);
		level6Button.setLevel(6);
		level6Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 6;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});
		StarButton level7Button = (StarButton) findViewById(R.id.level7Button);
		level7Button.setLevel(7);
		level7Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 7;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});
		StarButton level8Button = (StarButton) findViewById(R.id.level8Button);
		level8Button.setLevel(8);
		level8Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 8;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});
		StarButton level9Button = (StarButton) findViewById(R.id.level9Button);
		level9Button.setLevel(9);
		level9Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 9;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});
		StarButton level10Button = (StarButton) findViewById(R.id.level10Button);
		level10Button.setLevel(10);
		level10Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 10;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});

		StarButton level11Button = (StarButton) findViewById(R.id.level11Button);
		level11Button.setLevel(11);
		level11Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 11;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});

		StarButton level12Button = (StarButton) findViewById(R.id.level12Button);
		level12Button.setLevel(12);
		level12Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 12;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});

		StarButton level13Button = (StarButton) findViewById(R.id.level13Button);
		level13Button.setLevel(13);
		level13Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				int levelID = 13;
				Intent myIntent = new Intent(view.getContext(),
						LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				// pass the levelID to the generic Level Activity
				startActivityForResult(myIntent, 0);
			}

		});

		// this is the button for the market screen
		StarButton level14Button = (StarButton) findViewById(R.id.level14Button);

		// setting the level to 14 will inform the StarButton to draw
		// the image of the word 'Market' instead of the three stars
		level14Button.setLevel(14);
		level14Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(),
						MarketScreen.class);
				startActivityForResult(myIntent, 0);
			}

		});

	}
}
