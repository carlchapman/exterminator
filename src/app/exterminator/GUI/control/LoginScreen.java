package app.exterminator.GUI.control;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import app.exterminator.R;
import app.exterminator.sync.connect.ConnectionData;
import app.exterminator.sync.connect.LoginTask;

/**
 * The Class LoginScreen is the Activity where the user can give their username
 * and password and log in by pressing the first button, thus changing the
 * current player to reflect the data that is passed from the remote server to
 * the LoginTask.
 * 
 * As with all ConnectionTasks, the LoginTask is cancelable and displays a
 * progressDialog that has an indeterminate progrssBar (cycling circle) and
 * messages that reflect the progress of the connection operation.
 * 
 * The second button changes to the RegisterScreen Activity
 * 
 * @author carlchapman
 */
public class LoginScreen extends AbstractControlScreen {

	/** The loginner. */
	private LoginTask loginner;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginscreen);
		Button page1 = (Button) findViewById(R.id.Button01);
		page1.setOnClickListener(new LoginListener());

		Button page2 = (Button) findViewById(R.id.Button02);
		page2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(),
						RegisterScreen.class);
				startActivityForResult(myIntent, 0);
			}

		});
	}

	/**
	 * This listener checks the values passed in by the user and shows an alert
	 * if it finds any problems with the input. Otherwise it starts the progress
	 * dialog and LoginTask that will check if the username/password combination
	 * is valid and if so, log the user in and then launch the
	 * LevelSelectScreen.
	 * 
	 * @see LoginEvent
	 */
	class LoginListener implements View.OnClickListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View arg0) {

			// get the EditText fields
			EditText usernameBox = (EditText) findViewById(R.id.usernameTextBox);
			passwordBox = (EditText) findViewById(R.id.passwordTextBox);

			// get their contents
			String username = usernameBox.getText().toString();
			String password = passwordBox.getText().toString();

			// check for bad input
			if (username == null || username.equals("")) {

				showAlert("Empty Username", "please enter a username", null,
						null, null);

			} else if (password == null || password.equals("")) {

				showAlert("Empty Password", "please enter a password", null,
						null, null);

			} else { // if input is ok, then create name value pairs and try to
						// log in

				List<NameValuePair> nvp = new ArrayList<NameValuePair>();
				nvp.add(new BasicNameValuePair("username", username));
				nvp.add(new BasicNameValuePair("password", password));

				String path = "http://colegroff.com/cs309/exterminator/android_login.xml";
//				String path = "http://10.0.2.2:3000/android_login.xml"; // path for testing locally
				ConnectionData data = new ConnectionData(LoginScreen.this,
						getProgressDialog(LoginScreen.this, "Logging in...",
								loginner), nvp, path, LevelSelectScreen.class);

				loginner = (LoginTask) new LoginTask().execute(data);
			}
		}
	}
}
