package app.exterminator.GUI.control;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import app.exterminator.R;
import app.exterminator.sync.connect.ConnectionData;
import app.exterminator.sync.connect.RegisterTask;

/**
 * The Class RegisterScreen is an Activity where the user can provide the
 * information necessary to register and then attempt to register on the remote
 * server. The values that they provide go through some basic checks and then
 * are sent to the server. If the server finds that the provided username or
 * email has already been used, then the user is informed of the problem and
 * those fields are automatically erased for the user. If the registration is a
 * success, then the user's data is sent to the server as an update and the
 * LevelSelectScreen is launched.
 * 
 * @author carlchapman modified by Cole Groff
 */
public class RegisterScreen extends AbstractControlScreen {

	/** The registerer. */
	private RegisterTask registerer;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.registerscreen);
		Button page1 = (Button) findViewById(R.id.Button01);
		page1.setOnClickListener(new RegisterListener());
		findViewById(R.id.usernameTextBox).requestFocus();

	}

	/**
	 * This listener checks the user input and then, if that input is valid, it
	 * launches a RegisterTask that will attempt to register the user on the
	 * remote server.
	 * 
	 * @see RegisterEvent
	 */
	class RegisterListener implements View.OnClickListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View arg0) {

			// get the EditText fields
			usernameBox = (EditText) findViewById(R.id.usernameTextBox);
			EditText passwordBox = (EditText) findViewById(R.id.passwordTextBox);
			EditText confirmPasswordBox = (EditText) findViewById(R.id.confirmPasswordTextBox);
			emailBox = (EditText) findViewById(R.id.emailTextBox);
			confirmEmailBox = (EditText) findViewById(R.id.confirmEmailTextBox);

			// get their contents
			String username = usernameBox.getText().toString();
			String password1 = passwordBox.getText().toString();
			String password2 = confirmPasswordBox.getText().toString();
			String email1 = emailBox.getText().toString();
			String email2 = confirmEmailBox.getText().toString();

			// Input validation.
			if (username == null || username.equals("")) {

				showAlert("Empty Username", "please enter a username", null,
						null, null);

			} else if (password1 == null || password1.equals("")) {

				showAlert("Empty Password", "please enter a password", null,
						null, null);

			} else if (password2 == null || password2.equals("")) {

				showAlert("Empty Confirm Password",
						"please confirm your password", null, null, null);

			} else if (email1 == null || email1.equals("")) {

				showAlert("Empty Email", "please enter an email", null, null,
						null);

			} else if (email2 == null || email2.equals("")) {

				showAlert("Empty Confirm Email", "please confirm your email",
						null, null, null);

			} else if (!password1.equals(password2)) {

				showAlert("Password Mismatch", "your passwords must match",
						confirmPasswordBox, null, null);

			} else if (!email1.equals(email2)) {

				showAlert("Email Mismatch", "your emails must match",
						confirmEmailBox, null, null);

			} else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email1)
					.matches()) {

				showAlert("Invalid Email", "your email must be valid",
						confirmEmailBox, null, null);

			} else {

				// Passing 4 name value pairs to the Rails application.
				List<NameValuePair> nvp = new ArrayList<NameValuePair>(4);

				// The passed parameters using Rails model conventions.
				// For this case, a User model, the convention is
				// user[attribute].
				// Typical Devise registration accepts a username, password,
				// password confirmation
				// and email.
				nvp.add(new BasicNameValuePair("user[username]", username));
				nvp.add(new BasicNameValuePair("user[password]", password1));
				nvp.add(new BasicNameValuePair("user[password_confirmation]",
						password2));
				nvp.add(new BasicNameValuePair("user[email]", email1));

				String path = "http://colegroff.com/cs309/exterminator/users.xml";
//				String path = "http://10.0.2.2:3000/users.xml"; // path for testing locally
				// for testing locally.

				ConnectionData data = new ConnectionData(RegisterScreen.this,
						getProgressDialog(RegisterScreen.this,
								"Registering...", registerer), nvp, path,
						LevelSelectScreen.class);

				registerer = (RegisterTask) new RegisterTask().execute(data);

			}

		}

	}

}
