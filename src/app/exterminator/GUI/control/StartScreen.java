package app.exterminator.GUI.control;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import app.exterminator.R;
import app.exterminator.data.utility.Globals;

/**
 * The Class StartScreen Activity offers the current player the option to start
 * playing the game right away or to change their identity.
 * 
 * @author carlchapman
 */
public class StartScreen extends AbstractControlScreen {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		createGUI();
	}

	/**
	 * Creates the gui.
	 */
	private void createGUI() {
		setContentView(R.layout.startscreen);

		// this block has been throwing exceptions from the Market Screen
		// recently, or perhaps it is just that Android restarts the StartScreen
		// at the bottom of the stack? Anyway it throws a null pointer exception
		// when trying to get the player, or the player's data. This unexplained
		// behavior is not neccesary and so I'm letting this block throw the
		// exception, and there are default text messages in the XML file that
		// will be displayed if this ever comes to the user's view (so far it's
		// all invisible).
		refreshUsername();

		Button page1 = (Button) findViewById(R.id.Button01);
		page1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(),
						LevelSelectScreen.class);
				startActivityForResult(myIntent, 0);
			}

		});

		Button page2 = (Button) findViewById(R.id.Button02);
		page2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(),
						PlayerSelectScreen.class);
				startActivityForResult(myIntent, 0);
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onBackPressed()
	 */
	/**
	 * This is called when a user presses back from the start screen - it closes
	 * the database, and is the way that the application should be shut down.
	 */
	public void onBackPressed() {
		((Globals) getApplicationContext()).getDatabaseAdapter().close();
		finish();
	}
	
	public void onResume(){
		super.onResume();
		refreshUsername();
	}
	
	private void refreshUsername(){
		try {
			TextView greeting = (TextView) findViewById(R.id.greetingTextBox);
			greeting.setText("Hello, "
					+ ((Globals) getApplicationContext()).getPlayer()
							.getUserName() + "!");
			TextView changeUser = (TextView) findViewById(R.id.changeUserTextBox);
			String newChangeUserText = "";
			if (!((Globals) getApplicationContext()).getPlayer().isGuest())
				newChangeUserText += "Not you?  ";
			newChangeUserText += "Log in or Register as a different user";
			if (!((Globals) getApplicationContext()).getPlayer().isGuest())
				newChangeUserText += " or guest";
			changeUser.setText(newChangeUserText);
			//System.out.println("username refreshed");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
