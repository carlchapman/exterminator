package app.exterminator.GUI.control;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.exterminator.R;
import app.exterminator.GUI.widget.CountView;
import app.exterminator.GUI.widget.StarView;
import app.exterminator.data.facades.Player;
import app.exterminator.data.utility.Globals;
import app.exterminator.sync.connect.ClaimBugbucksTask;
import app.exterminator.sync.connect.ConnectionData;

/**
 * The Class MarketScreen is an Activity where the user can trade stars they
 * have earned for bugbucks, or sync purchased bugbucks.
 * 
 * @author carlchapman
 */
public class MarketScreen extends AbstractControlScreen implements
		View.OnClickListener {

	// for setting up the view
	/** The Constant BUFFER. */
	static final int BUFFER = 8;

	/** The star view. */
	private StarView starView;

	/** The exchange stars button. */
	private Button exchangeStarsButton;

	/** The sych purchased button. */
	private Button sychPurchasedButton;

	/** The level select button. */
	private Button levelSelectButton;

	/** The counter. */
	private CountView counter;

	// for button press events
	/** The n bugbucks. */
	private int nBugbucks;

	/** The exchange rate. */
	private int exchangeRate;

	/** The bonus boolean - true if the user gets bonus bugbucks. */
	boolean bonus;

	/** The player. */
	private Player player;

	/** The claimer. */
	private ClaimBugbucksTask claimer;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.marketscreen);
		this.player = ((Globals) this.getApplicationContext()).getPlayer();
		initializeViewElements();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		if (v == exchangeStarsButton) {
			if (0 != nBugbucks) {
				if (bonus)
					player.addBugBucks(10);
				int nStarsToRemove = nBugbucks * exchangeRate;
				player.removeNStars(nStarsToRemove);
				player.addBugBucks(nBugbucks);
				nBugbucks = 0;
				int newNStars = player.getNumberOfStars();
				resetMessages(newNStars);
				starView.setNStars(newNStars);
				starView.invalidate();
				player.performUpdate();
			}
		} else if (v == levelSelectButton) {
			Intent myIntent = new Intent(v.getContext(),
					LevelSelectScreen.class);
			startActivityForResult(myIntent, 0);
		}
	}

	// //////////////////////Helper methods//////////////////////////////

	/**
	 * Initialize view elements.
	 */
	private void initializeViewElements() {

		// initialize or get a handle on view elements
		counter = (CountView) findViewById(R.id.counter);
		starView = (StarView) findViewById(R.id.starView);
		exchangeStarsButton = (Button) findViewById(R.id.exchangeButton);
		sychPurchasedButton = (Button) findViewById(R.id.synchButton);
		levelSelectButton = (Button) findViewById(R.id.levelButton);
		TextView registerMessage = (TextView) findViewById(R.id.registerMessage);
		exchangeStarsButton.setOnClickListener(this);
		sychPurchasedButton.setOnClickListener(new ClaimBugbucksListener());
		levelSelectButton.setOnClickListener(this);

		// guest-sensitive logic
		exchangeRate = 2;
//		if (player.isGuest()) {
//			exchangeRate = 3;
//		} else
		registerMessage.setVisibility(View.GONE);

		// depends on number of stars the player has
		int nStars = player.getNumberOfStars();
		nBugbucks = nStars / exchangeRate;
		starView.setNStars(nStars);
		bonus = false;
		if (nStars == 42)
			bonus = true;

		// help starView draw the right box of stars
		handleOrientation();

		// set text
		resetMessages(nStars);
		sychPurchasedButton.setText("Purchase 20 Bugbucks for $1");
		levelSelectButton.setText("Back To Level Select Screen");
	}

	/**
	 * Makes the text elements correct in the case of a singular noun.
	 * 
	 * @param nStars
	 *            the n stars
	 */
	private void resetMessages(int nStars) {
		String bugbucksString = " Bugbucks";
		if (nBugbucks == 1)
			bugbucksString = " Bugbuck";
		exchangeStarsButton.setText("Trade " + nStars + " Stars for "
				+ nBugbucks + bugbucksString);
		counter.setNBugbucks(player.getBugBucks());
		counter.setNStars(nStars);
		counter.invalidate();
	}

	/**
	 * Gets the screen width.
	 * 
	 * @return the screen width
	 */
	private int getScreenWidth() {
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		// System.out.println("width: " + dm.widthPixels + " height: "
		// + dm.heightPixels);
		return dm.widthPixels;
	}

	/**
	 * Handle orientation.
	 */
	private void handleOrientation() {
		starView.setScreenWidth(getScreenWidth());
		int orientation = getResources().getConfiguration().orientation;
		if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
			starView.setLayoutParams(new LinearLayout.LayoutParams(
					getScreenWidth(),
					(int) (((double) getScreenWidth()) * 0.24)));
			LinearLayout layoutHolder = (LinearLayout) findViewById(R.id.layoutHolder);
			layoutHolder.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout buttonHolder = (LinearLayout) findViewById(R.id.buttonHolder);
			buttonHolder.setLayoutParams(new LinearLayout.LayoutParams(500,
					LayoutParams.FILL_PARENT));
			starView.setBackground();
			starView.invalidate();
		} else {
			starView.setLayoutParams(new LinearLayout.LayoutParams(
					getScreenWidth(),
					(int) (((double) getScreenWidth()) * 0.46))); // 100%,39.6%
			LinearLayout layoutHolder = (LinearLayout) findViewById(R.id.layoutHolder);
			layoutHolder.setOrientation(LinearLayout.VERTICAL);
			LinearLayout buttonHolder = (LinearLayout) findViewById(R.id.buttonHolder);
			buttonHolder.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			starView.setBackground();
			starView.invalidate();
		}
	}
	
	public CountView getCounter(){
		return counter;
	}

	/**
	 * This Listener launches a ClaimBugBucksTask that will check the remote
	 * server to see if there are purchased bugbucks in the user table, and if
	 * so, pass them back to be added to this user.
	 * 
	 * @see ClaimBugbucksEvent
	 */
	class ClaimBugbucksListener implements View.OnClickListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			
			String authToken = player.getToken();
			
			List<NameValuePair> nvp = new ArrayList<NameValuePair>();
			
			String path = "http://colegroff.com/cs309/exterminator/claim_bugbucks.xml?auth_token=" + authToken;
//			String path = "http://10.0.2.2:3000/claim_bugbucks.xml?auth_token=" + authToken;
			ConnectionData data = new ConnectionData(MarketScreen.this,
					getProgressDialog(MarketScreen.this,
							"Claiming Bugbucks...", claimer), nvp, path,
					LevelSelectScreen.class);
			claimer = (ClaimBugbucksTask) new ClaimBugbucksTask().execute(data);

		}

	}

}
