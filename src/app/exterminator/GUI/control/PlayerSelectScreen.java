package app.exterminator.GUI.control;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import app.exterminator.R;
import app.exterminator.data.facades.Player;
import app.exterminator.data.utility.Globals;

/**
 * The Class PlayerSelectScreen is the Activity where the user has identified
 * that they want to change users, and then they are presented with three
 * choices: be new Guest, Register, or Log in.
 * 
 * @author carlchapman
 */
public class PlayerSelectScreen extends AbstractControlScreen {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.playerselectscreen);

		Button page1 = (Button) findViewById(R.id.Button01);
		page1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				String message = "Logging in as a guest again will erase all current guest data.";
				if (!((Globals) getApplicationContext()).getPlayer().isGuest())
					message = "This will erase "
							+ ((Globals) getApplicationContext()).getPlayer()
									.getUserName() + "'s data.";
				warnLaunchDialog(PlayerSelectScreen.this, "Log In As Guest",
						message, new SelectGuestOnClickListener(
								LevelSelectScreen.class),true);
			}

		});

		Button page2 = (Button) findViewById(R.id.Button02);
		page2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(),
						LoginScreen.class);
				startActivityForResult(myIntent, 0);
			}

		});

		Button page3 = (Button) findViewById(R.id.Button03);
		page3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(),
						RegisterScreen.class);
				startActivityForResult(myIntent, 0);
			}

		});

	}

	/**
	 * This listener logs the player in as a brand new guest, erasing the
	 * previous user's data.
	 * 
	 * @see SelectGuestOnClickEvent
	 */
	class SelectGuestOnClickListener extends StartActivityOnClickListener {

		/**
		 * Instantiates a new select guest on click listener.
		 * 
		 * @param target
		 *            the target
		 */
		@SuppressWarnings("rawtypes")
		public SelectGuestOnClickListener(Class target) {
			super(target);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * app.exterminator.GUI.control.ControlScreen.StartActivityOnClickListener
		 * #onClick(android.content.DialogInterface, int)
		 */
		@Override
		public void onClick(DialogInterface dialog, int which) {

			Player player = ((Globals) getApplication()).getPlayer();
			String oldToken = player.getToken();
			
			if(player.isGuest()){
				player.blankSlate();     
				player.setBugBucks(Player.INITIAL_BUGBUCKS);
			}else{
				player.blankSlate();
				player.setIsGuest(true);
				player.setUsername("Guest");
				player.setToken("");
				player.setBugBucks(Player.INITIAL_BUGBUCKS);
				player.logoutOldUser(oldToken);
				player.performUpdate();
			}
			super.onClick(dialog, which);
		}

	}

}
