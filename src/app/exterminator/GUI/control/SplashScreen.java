package app.exterminator.GUI.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import app.exterminator.R;
import app.exterminator.GUI.game.Bug;
import app.exterminator.GUI.game.Tower;
import app.exterminator.data.facades.DatabaseAdapter;
import app.exterminator.data.facades.Player;
import app.exterminator.data.utility.BMPS;
import app.exterminator.data.utility.Globals;
import app.exterminator.sync.connect.RunnableTask;

/**
 * The Class SplashScreen is the first thing that the user will see when they
 * load their application. After it is loaded and in memory, they may not see
 * the SplashScreen unless Android deletes some of the data and it needs to be
 * reloaded again.
 * 
 * @author carlchapman
 */
public class SplashScreen extends AbstractControlScreen {

	/** The progress bar. */
	private ProgressBar progressBar;

	AnimationDrawable antimation1;

	/**
	 * The start time - used for debugging to check what activity is taking what
	 * time.
	 */
	private long startTime;

	/**
	 * The total number of steps of loading all data if nothing is already
	 * loaded.
	 */
	private final int TOTAL_STEPS = 20;

	/** The number of steps remaining given the already-loaded data. */
	private int nSteps;

	/**
	 * Called upon creation of this method - in this case this creates a
	 * progress bar to show on the main thread while loading all the necessary
	 * information into classes on the background.
	 * 
	 * @param savedInstanceState
	 *            the saved instance state
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.splashscreen);
		ImageView leftAnts = (ImageView) findViewById(R.id.Animation1);
		leftAnts.setBackgroundResource(R.drawable.northanimation);
		antimation1 = (AnimationDrawable) leftAnts.getBackground();

		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		progressBar.setProgress(0);

		// check to see if all data is already loaded
		Player player = ((Globals) getApplicationContext()).getPlayer();
		DatabaseAdapter db = ((Globals) getApplicationContext())
				.getDatabaseAdapter();
		if (BMPS.areSetup() && player != null && db != null) {
			db.open();
			launchStartScreen();
		} else {
			// update the number of steps it will take to load the maps
			nSteps = TOTAL_STEPS - BMPS.getNextMapIndex();

			// put stuff into a package and pass it to a background thread
			LoadingData data = new LoadingData(this, progressBar);
			new LoadingTask().execute(data);
			new RunnableTask().execute(new StartAnimation());
		}
	}

	class StartAnimation implements Runnable {

		@Override
		public void run() {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			antimation1.start();
		}

	}

	/**
	 * This updates the progress bar on this thread - called by the background
	 * thread.
	 * 
	 * @param integer
	 *            the integer
	 */
	private void updateProgressBar(Integer integer) {
		int newProgress = progressBar.getProgress() + 100 / nSteps;
		progressBar.setProgress(newProgress);
	}

	public void launchStartScreen() {
		Intent myIntent = new Intent(getApplicationContext(), StartScreen.class);
		startActivityForResult(myIntent, 0);
	}

	/**
	 * A package class to pass parameters to the AsyncTask.
	 * 
	 * @author carlchapman
	 */
	class LoadingData {

		/** The parent. */
		private Activity parent;

		/** The progress bar. */
		private ProgressBar progressBar;

		/**
		 * Instantiates a new loading data.
		 * 
		 * @param parent
		 *            the parent
		 * @param pb
		 *            the pb
		 */
		public LoadingData(Activity parent, ProgressBar pb) {
			this.parent = parent;
			this.progressBar = pb;
		}

		/**
		 * Gets the parent.
		 * 
		 * @return the parent
		 */
		public Activity getParent() {
			return parent;
		}

		/**
		 * Gets the progress bar.
		 * 
		 * @return the progress bar
		 */
		public ProgressBar getProgressBar() {
			return progressBar;
		}
	}

	/**
	 * An asyncTask that loads the database, player and all images upon startup.
	 * 
	 * @author carlchapman
	 */
	class LoadingTask extends AsyncTask<LoadingData, Integer, Boolean> {

		/** The progress bar. */
		protected ProgressBar progressBar;

		/** The parent. */
		protected Activity parent;

		/**
		 * The main method of the AsyncTask, this is where the work is done, and
		 * also it publishes progress as it goes.
		 * 
		 * @param params
		 *            the params
		 * @return the boolean
		 */
		@Override
		protected Boolean doInBackground(LoadingData... params) {

			// get references from package passed in
			LoadingData data = params[0];
			this.progressBar = data.getProgressBar();
			this.parent = data.getParent();
			final Context context = parent.getApplicationContext();

			// Get an instance of the database adapter, open database.
			startTime = System.currentTimeMillis();
			DatabaseAdapter db = new DatabaseAdapter(context);
			((Globals) context).setDatabaseAdapter(db);
			System.out.println("time to laod db: "
					+ (System.currentTimeMillis() - startTime));// 4
			startTime = System.currentTimeMillis();
			db.open();
			System.out.println("time to open db: "
					+ (System.currentTimeMillis() - startTime));// 47
			startTime = System.currentTimeMillis();
			publishProgress(0);

			// load player
			((Globals) getApplicationContext()).setPlayer(new Player(db));
			System.out.println("time to laod player: "
					+ (System.currentTimeMillis() - startTime));// 609
			startTime = System.currentTimeMillis();
			publishProgress(0);

			// initialize map bitmaps - each takes about 600 ms to load, so
			// publish in between loading each map
			int i = BMPS.getNextMapIndex();
			while (i < BMPS.MAP_RESOURCES.length) {
				i = BMPS.initializeNextMap(getApplicationContext());
				System.out.println("time to init map " + i + " was: "
						+ (System.currentTimeMillis() - startTime));// about 600

				startTime = System.currentTimeMillis();
				publishProgress(0);
			}

			// initialize screen bitmaps
			BMPS.initializeScreenBackgrounds(getApplicationContext());
			System.out.println("time to init screens: "
					+ (System.currentTimeMillis() - startTime));// 219
			startTime = System.currentTimeMillis();
			publishProgress(0);

			// initialize icon bitmaps
			BMPS.initializeIcons(getApplicationContext());
			System.out.println("time to init icons: "
					+ (System.currentTimeMillis() - startTime));// 93
			startTime = System.currentTimeMillis();
			publishProgress(0);

			// initialize bug and tower bitmaps
			Tower.initializeTowerBitmaps(getApplicationContext());
			Bug.initializeBugBitmaps(getApplicationContext());
			System.out.println("time to init bugs: "
					+ (System.currentTimeMillis() - startTime));// 114
			publishProgress(0);

			// finally launch the start screen on the UI thread
			parent.runOnUiThread(new Runnable() {
				public void run() {
					launchStartScreen();
				}
			});
			return false;
		}

		/**
		 * run once doInBackground() is finished, this simply hides the progess
		 * bar.
		 * 
		 * @param result
		 *            the result
		 */
		protected void onPostExecute(Boolean result) {
			System.out.println("dismissing dialog");
			progressBar.setVisibility(ProgressBar.GONE);
		}

		/**
		 * A standard method of AsyncTask, this is what 'publish progress(x)'
		 * passes into, and the argument must be of the second type in the three
		 * generic fields of AsyncTask.
		 * 
		 * @param values
		 *            the values
		 */
		protected void onProgressUpdate(Integer... values) {
			parent.runOnUiThread(new UpdateProgress(values[0]));
		}

		/**
		 * A micro-class just to be able to pass a value to the main ui thread
		 * if neccesary later.
		 * 
		 * @author carlchapman
		 */
		class UpdateProgress implements Runnable {

			/** The n. */
			private int n;

			/**
			 * Instantiates a new update progress.
			 * 
			 * @param n
			 *            the n
			 */
			public UpdateProgress(int n) {
				this.n = n;
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				updateProgressBar(n);

			}

		}
	}

}
