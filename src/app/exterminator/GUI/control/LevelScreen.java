package app.exterminator.GUI.control;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;
import app.exterminator.R;
import app.exterminator.GUI.game.GameView;
import app.exterminator.GUI.game.Map;
import app.exterminator.GUI.game.Tower;
import app.exterminator.data.structures.TowerInfo;
import app.exterminator.data.utility.Globals;

/**
 * The Class LevelScreen is the Activity that holds the GameView and the tower
 * placement buttons.
 * 
 * @author carlchapman modified by Justin Landsgard
 */
public class LevelScreen extends Activity {

	/** The view. */
	private GameView view;

	/** The game. */
	private FrameLayout game;

	/** The Game buttons. */
	private RelativeLayout GameButtons;

	/** The status. */
	private int status;

	/** The Constant START_DRAGGING. */
	private final static int START_DRAGGING = 0;

	/** The Constant STOP_DRAGGING. */
	private final static int STOP_DRAGGING = 1;

	/** The image. */
	private ImageView image;

	/** The map. */
	private Map map;

	private int playerCoins;

	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 *            the saved instance state
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// get the levelID
		final Bundle extras = getIntent().getExtras();
		int levelID = -1;
		if (extras != null) {
			levelID = extras.getInt("levelID");
		}
		System.out.println("the extras bundle for key 'levelID' is: "
				+ extras.getInt("levelID", -1));// -1 is default value
		// set the view to display the map for that level
		Context context = getApplicationContext();
		map = new Map(levelID, context);

		view = new GameView(this, map);

		image = new ImageView(this);

		game = new FrameLayout(this);

		GameButtons = new RelativeLayout(this);

		playerCoins = map.getInfo().getCoins();

		final Button t1 = new Button(this);
		final Button t2 = new Button(this);
		final Button t3 = new Button(this);

		t1.setBackgroundResource(R.drawable.gasbutton);
		t2.setBackgroundResource(R.drawable.tapebutton);
		t3.setBackgroundResource(R.drawable.zapperbutton);

		t3.setId(123456);
		t2.setId(12345);
		t1.setId(1234);

		// Create layout parameters for the buttons.

		RelativeLayout.LayoutParams b1 = new LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		RelativeLayout.LayoutParams b2 = new LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		RelativeLayout.LayoutParams b3 = new LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		RelativeLayout.LayoutParams params = new LayoutParams(
				RelativeLayout.LayoutParams.FILL_PARENT,
				RelativeLayout.LayoutParams.FILL_PARENT);
		GameButtons.setLayoutParams(params);

		GameButtons.addView(t1);
		GameButtons.addView(t2);
		GameButtons.addView(t3);

		b1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
		b3.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

		b2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
		b3.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);

		t3.setLayoutParams(b3);
		b2.addRule(RelativeLayout.LEFT_OF, t3.getId());
		t2.setLayoutParams(b2);
		b1.addRule(RelativeLayout.LEFT_OF, t2.getId());
		t1.setLayoutParams(b1);

		t1.setDrawingCacheEnabled(true);
		t2.setDrawingCacheEnabled(true);
		t3.setDrawingCacheEnabled(true);

		t2.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent me) {
				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					status = START_DRAGGING;
					// draw attack shadow
					status = START_DRAGGING;

					image.setImageBitmap(t2.getDrawingCache());
					game.addView(image, new RelativeLayout.LayoutParams(
							RelativeLayout.LayoutParams.WRAP_CONTENT,
							RelativeLayout.LayoutParams.WRAP_CONTENT));
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					status = STOP_DRAGGING;
					// add tower to map
					if (!view.isInRangeOfTower((int) me.getRawX(),
							(int) me.getRawY())) {
						TowerInfo a = new TowerInfo(1, 0, (int) Math.round(me
								.getRawX()), (int) Math.round(me.getRawY()),
								((Globals) getApplicationContext())
										.getDatabaseAdapter(), extras
										.getInt("levelID"));

						Tower tower = new Tower(a);
						playerCoins = map.getInfo().getCoins();
						;
						if (tower.getCost() <= playerCoins) {
							view.addTower(tower);
							playerCoins -= tower.getCost();
							map.getInfo()
									.subtractCoins(tower.getCost());
						}else{
							String text = "you need " + tower.getCost()
									+ " coins";
							Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT)
									.show();
						}
					}
					game.removeView(image);
					image.invalidate();

				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					if (status == START_DRAGGING) {
						// System.out.println("Dragging");
						image.setPadding((int) me.getRawX(),
								(int) me.getRawY(), 0, 0);
						image.invalidate();

					}
				}
				return false;
			}
		});

		t1.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent me) {
				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					status = START_DRAGGING;
					// draw attack shadow
					status = START_DRAGGING;

					image.setImageBitmap(t1.getDrawingCache());
					game.addView(image, new RelativeLayout.LayoutParams(
							RelativeLayout.LayoutParams.WRAP_CONTENT,
							RelativeLayout.LayoutParams.WRAP_CONTENT));
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					status = STOP_DRAGGING;
					// add tower to map
					if (!view.isInRangeOfTower((int) me.getRawX(),
							(int) me.getRawY())) {
						Tower tower = new Tower(new TowerInfo(0, 0, (int) Math
								.round(me.getRawX()), (int) Math.round(me
								.getRawY()),
								((Globals) getApplicationContext())
										.getDatabaseAdapter(), extras
										.getInt("levelID")));
						playerCoins = map.getInfo().getCoins();
						if (tower.getCost() <= playerCoins) {
							view.addTower(tower);
							playerCoins -= tower.getCost();
							map.getInfo()
									.subtractCoins(tower.getCost());
						}else{
							String text = "you need " + tower.getCost()
									+ " coins";
							Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT)
									.show();
						}
					}
					game.removeView(image);
					image.invalidate();

				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					if (status == START_DRAGGING) {
						//System.out.println("Dragging");
						image.setPadding((int) me.getRawX(),
								(int) me.getRawY(), 0, 0);
						image.invalidate();

					}
				}
				return false;
			}
		});

		t3.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent me) {
				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					status = START_DRAGGING;
					// draw attack shadow
					status = START_DRAGGING;

					image.setImageBitmap(t3.getDrawingCache());
					game.addView(image, new RelativeLayout.LayoutParams(
							RelativeLayout.LayoutParams.WRAP_CONTENT,
							RelativeLayout.LayoutParams.WRAP_CONTENT));
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					status = STOP_DRAGGING;
					// add tower to map
					if (!view.isInRangeOfTower((int) me.getRawX(),
							(int) me.getRawY())) {
						Tower tower = new Tower(new TowerInfo(2, 0, (int) Math
								.round(me.getRawX()), (int) Math.round(me
								.getRawY()),
								((Globals) getApplicationContext())
										.getDatabaseAdapter(), extras
										.getInt("levelID")));
						playerCoins = map.getInfo().getCoins();
						if (tower.getCost() <= playerCoins) {
							view.addTower(tower);
							playerCoins -= tower.getCost();
							map.getInfo()
									.subtractCoins(tower.getCost());
						}else{
							String text = "you need " + tower.getCost()
									+ " coins";
							Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT)
									.show();
						}
					}
					game.removeView(image);
					image.invalidate();

				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					if (status == START_DRAGGING) {
						//System.out.println("Dragging");
						image.setPadding((int) me.getRawX(),
								(int) me.getRawY(), 0, 0);
						image.invalidate();

					}
				}
				return false;
			}
		});

		game.addView(view);
		game.addView(GameButtons);

		setContentView(game);
		System.out.println("finished Oncreate in XLevel");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// end the GameThread before finishing this activity
		view.endThread();
		finish();
	}
}