package app.exterminator.GUI.game;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import app.exterminator.data.facades.Player;
import app.exterminator.data.structures.MapInfo;
import app.exterminator.data.utility.BMPS;
import app.exterminator.data.utility.Globals;

/**
 * The Class Map creates the paths[0] and waves of bugs that characterize each
 * level depending on the levelID. This design allows for easy customization of
 * what bug will appear when so that individual levels can be easily tuned to
 * their desired difficulty.
 * 
 * The map is also responsible for painting the background image and a table in
 * the upper left part of the screen that displays the pause button, speed
 * button, the coins and player health in the onDraw(...) method.
 * 
 * @author carlchapman
 */
public class Map {

	// object variables
	/** The level id. */
	private int levelID;

	/** The background. */
	private Bitmap background;

	/** The paths available to build - initialized to empty. */
	private BugPath[] paths;

	private static final int MAXIMUM_NUMBER_PATHS = 25;

	/** The waves. */
	private ArrayList<Wave> waves;

	/** The player. */
	private Player player;

	// for painting the info table
	/** The Constant BUFFER. */
	private static final int BUFFER = 4;

	/** The text paint. */
	private Paint textPaint;

	/** The bugbuck icon */
	private Bitmap bugbuckIcon;

	/** The coin icon. */
	private Bitmap coinIcon;

	/** The life icon. */
	private Bitmap lifeIcon;

	/** The pause icon. */
	private Bitmap pauseIcon;

	/** The speed icon. */
	private Bitmap speedIcon[];

	/** The health text max width. */
	private int healthTextMaxWidth;

	/** The text height. */
	private int textHeight;

	/** The pause rect. */
	private Rect pauseRect;

	/** The speed rect. */
	private Rect speedRect;

	/** The animation speed. */
	private int animationSpeed;

	/**
	 * Instantiates a new map including the paths[0] and all of its waves of
	 * bugs. At the end of this method, the startTime of the current wave is set
	 * so that if a player saves a level, then when that level is restarted, it
	 * starts at the virtual time it was saved. Note that wave time does not
	 * attempt to correspond directly to real time, but is relative.
	 * 
	 * @param levelID
	 *            the level id
	 * @param context
	 *            the context
	 */
	public Map(int levelID, Context context) {
		this.levelID = levelID;
		this.player = ((Globals) context).getPlayer();
		waves = new ArrayList<Wave>();

		initializeTableGraphics(context);

		paths = new BugPath[MAXIMUM_NUMBER_PATHS];
		for (int i = 0; i < MAXIMUM_NUMBER_PATHS; i++) {
			paths[i] = new BugPath(i);
		}
		switch (levelID) {
		case 0:
			initializeLevel0(context);
			break;
		case 1:
			paths[0].add(new Point(800, 320));
			paths[0].add(new Point(680, 320));
			paths[0].add(new Point(680, 50));
			paths[0].add(new Point(520, 50));
			paths[0].add(new Point(520, 320));
			paths[0].add(new Point(400, 320));
			paths[0].add(new Point(400, 200));
			paths[0].add(new Point(500, 200));
			paths[0].add(new Point(500, 50));
			paths[0].add(new Point(120, 50));
			paths[0].add(new Point(120, 300));
			paths[0].add(new Point(-10, 300));
			paths[0].add(new Point(-100, 300));
			paths[0].setColor(Color.argb(120, 120, 120, 20));

			Wave wave1_1 = new Wave(20);
			wave1_1.put(20, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(20, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(30, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(60, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(70, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(80, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(100, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(110, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(120, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(140, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(150, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(160, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(180, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(190, new Bug(context, paths[0], Bug.ANT, levelID));
			wave1_1.put(200, new Bug(context, paths[0], Bug.ANT, levelID));
			waves.add(wave1_1);

			break;
		case 2:
			paths[0].add(new Point(800, 320));
			paths[0].add(new Point(600, 320));
			paths[0].add(new Point(600, 80));
			paths[0].add(new Point(400, 80));
			paths[0].add(new Point(400, 180));
			paths[0].add(new Point(200, 180));
			paths[0].add(new Point(200, 80));
			paths[0].add(new Point(100, 80));
			paths[0].add(new Point(100, 300));
			paths[0].add(new Point(-10, 300));
			paths[0].add(new Point(-100, 300));
			paths[0].setColor(Color.argb(120, 55, 204, 255));
			Wave wave2 = new Wave(41);

			wave2.put(20, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(30, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(60, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(70, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(80, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(100, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(110, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(120, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(140, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(150, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(160, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(180, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(190, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(200, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(220, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(230, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(240, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(250, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(260, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(270, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(280, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(290, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(300, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(310, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(320, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(330, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(340, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(350, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(360, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(370, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(380, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(390, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(400, new Bug(context, paths[0], Bug.ANT, levelID));
			wave2.put(410, new Bug(context, paths[0], Bug.ROACH, levelID));
			wave2.put(420, new Bug(context, paths[0], Bug.ROACH, levelID));
			wave2.put(430, new Bug(context, paths[0], Bug.ROACH, levelID));
			wave2.put(440, new Bug(context, paths[0], Bug.ROACH, levelID));
			waves.add(wave2);
			break;
		case 3:
			initializeLevel3(context);
			break;
		case 4:
			paths[0].add(new Point(800, 320));
			paths[0].add(new Point(600, 320));
			paths[0].add(new Point(600, 80));
			paths[0].add(new Point(400, 80));
			paths[0].add(new Point(400, 180));
			paths[0].add(new Point(200, 180));
			paths[0].add(new Point(200, 80));
			paths[0].add(new Point(100, 80));
			paths[0].add(new Point(100, 300));
			paths[0].add(new Point(-10, 300));
			paths[0].add(new Point(-100, 300));
			paths[0].setColor(Color.argb(120, 255, 51, 100));
			Wave wave4 = new Wave(76);

			wave4.put(20, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(30, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(60, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(70, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(80, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(100, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(110, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(120, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(140, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(150, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(160, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(180, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(190, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(200, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(220, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(230, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(240, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(250, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(260, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(270, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(280, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(290, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(300, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(310, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(320, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(330, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(340, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(350, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(360, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(370, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(380, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(390, new Bug(context, paths[0], Bug.ANT, levelID));
			wave4.put(400, new Bug(context, paths[0], Bug.ANT, levelID));

			wave4.put(420, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(430, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(440, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(450, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(460, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(470, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(480, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(490, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(500, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(510, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(520, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(530, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(540, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(550, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(560, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(570, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(580, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(590, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(600, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(620, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(630, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(640, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(650, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(660, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(670, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(680, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(690, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(700, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(710, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(720, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(730, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(740, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(750, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(760, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(770, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(780, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(790, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave4.put(800, new Bug(context, paths[0], Bug.RED_ANT, levelID));

			waves.add(wave4);
			break;
		case 5:
			initializeLevel5(context);
			break;
		case 6:
			initializeLevel6(context);
			break;
		case 7:
			initializeLevel7(context);
			break;
		case 8:
			paths[0].add(new Point(800, 80));
			paths[0].add(new Point(100, 80));
			paths[0].add(new Point(700, 80));
			paths[0].add(new Point(10, 80));
			paths[0].add(new Point(110, 80));
			paths[0].add(new Point(110, 330));
			paths[0].add(new Point(593, 330));
			paths[0].add(new Point(110, 330));
			paths[0].add(new Point(110, 80));
			paths[0].add(new Point(-10, 80));
			paths[0].add(new Point(-100, 80));
			paths[0].setColor(Color.argb(0, 120, 120, 20));

			paths[1].add(new Point(800, 330));
			paths[1].add(new Point(100, 330));
			paths[1].add(new Point(700, 330));
			paths[1].add(new Point(10, 330));
			paths[1].add(new Point(110, 330));
			paths[1].add(new Point(110, 80));
			paths[1].add(new Point(770, 80));
			paths[1].add(new Point(110, 80));
			paths[1].add(new Point(110, 330));
			paths[1].add(new Point(-10, 330));
			paths[1].add(new Point(-100, 330));
			paths[1].setColor(Color.argb(0, 120, 120, 20));

			Wave wave8 = new Wave(9);
			wave8.put(20, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave8.put(40, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave8.put(60, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave8.put(80, new Bug(context, paths[0], Bug.RED_ANT, levelID));

			wave8.put(21, new Bug(context, paths[1], Bug.BEE, levelID));
			wave8.put(41, new Bug(context, paths[1], Bug.BEE, levelID));
			wave8.put(61, new Bug(context, paths[1], Bug.BEE, levelID));
			wave8.put(81, new Bug(context, paths[1], Bug.BEE, levelID));
			wave8.put(101, new Bug(context, paths[1], Bug.BEE, levelID));

			waves.add(wave8);
			break;

		case 9:
			initializeLevel9(context);
			break;
		case 10:
			paths[0].add(new Point(800, 320));
			paths[0].add(new Point(600, 320));
			paths[0].add(new Point(600, 80));
			paths[0].add(new Point(400, 80));
			paths[0].add(new Point(400, 180));
			paths[0].add(new Point(200, 180));
			paths[0].add(new Point(200, 80));
			paths[0].add(new Point(100, 80));
			paths[0].add(new Point(100, 300));
			paths[0].add(new Point(-10, 300));
			paths[0].add(new Point(-100, 300));
			paths[0].setColor(Color.argb(0, 0, 0, 0));
			paths[1].add(new Point(800, 320));
			paths[1].add(new Point(-10, 320));
			paths[1].add(new Point(-100, 320));
			paths[1].setColor(Color.argb(120, 255, 255, 255));
			Wave wave10 = new Wave(10);

			wave10.put(20, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave10.put(40, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave10.put(60, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave10.put(80, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave10.put(80, new Bug(context, paths[1], Bug.ANT, levelID));
			wave10.put(100, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave10.put(120, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave10.put(130, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave10.put(140, new Bug(context, paths[0], Bug.RED_ANT, levelID));
			wave10.put(140, new Bug(context, paths[1], Bug.ANT, levelID));
			wave10.put(150, new Bug(context, paths[0], Bug.TICK,levelID));
			waves.add(wave10);
			break;
		case 11:
			initializeLevel11(context);

			break;
		case 12:
			initializeLevel12(context);
			break;
		case 13:
			initializeLevel13(context);
			break;
		default:
			// assign error values
			paths[0].add(new Point(-100, 300));
			waves.add(new Wave());
			levelID = 0;
			break;
		}

		background = BMPS.getMap(levelID);

		// get the current wave of any saved state to avoid recreating those
		// waves.

		// if a player saves and restarts, the currentWave 'pointer' will
		// start the game at that wave. To avoid creating bugs twice, set
		// the waveTime of that wave to its previous state. If the MapInfo
		// has never been used or has been reset, then these values should
		// be 0,0 and not change anything
		int currentWave = player.getMapInfo(levelID).getCurrentWave();
		int startTime = player.getMapInfo(levelID).getCurrentWaveTime();
		if (currentWave >= waves.size()) {
			System.err
					.println("an out of bounds currentWave index was fetched due to an improper level shutdown.  This index will be reset to zero.");
//			System.out.println("startTime,currrentwave,levelID" + startTime
//					+ ", " + currentWave + ", " + levelID);
			currentWave = 0;
		}

		waves.get(currentWave).setWaveTime(startTime);

	}

	/**
	 * Gets the MapInfo for this level.
	 * 
	 * @return the MapInfo for this level
	 */
	public MapInfo getInfo() {
		return player.getMapInfo(levelID);
	}

	/**
	 * Gets the player.
	 * 
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Gets n bugs spaced at intervals of 'spacing' time.
	 * 
	 * @param context
	 *            the context
	 * @param paths
	 *            [0] the paths[0]
	 * @param nBugs
	 *            the number of bugs to add
	 * @param startTime
	 *            the start time
	 * @param spacing
	 *            the spacing
	 * @param bugID
	 *            the bug id
	 * @param levelID
	 *            the level id
	 * @return the n spaced bugs as a wave
	 */
	public Wave getNSpacedBugs(Context context, BugPath path, int nBugs,
			int startTime, int spacing, int bugID, int levelID) {
		int key = startTime;
		Wave toReturn = new Wave(nBugs);
		for (int i = 0; i < nBugs; i++) {
			toReturn.put(key, new Bug(context, path, bugID, levelID));
			key += spacing;
		}
		return toReturn;
	}

	private Wave getCentipede(Context context, BugPath path, int levelID) {
		Wave toReturn = new Wave(16);
		toReturn.put(10, new Bug(context, path, Bug.CENTIPEDE_HEAD, levelID));
		toReturn.addAll(getNSpacedBugs(context, path, 15, 1, 1,
				Bug.CENTIPEDE_BODY, levelID));
		return toReturn;

	}

	public BugPath getPath(int index) {
		return paths[index];
	}

	/**
	 * Gets the number of waves.
	 * 
	 * @return the number of waves
	 */
	public int getNumberOfWaves() {
		return waves.size();
	}

	/**
	 * Gets the wave to run.
	 * 
	 * @return the wave to run
	 */
	public Wave getWaveToRun() {
		// This conditional prevents IndexOutOfBounds Errors that
		// have occurred during testing because a level was
		// not reset after it was finished, so its currentWave equal to
		// waves.size()
		// this has been fixed in the GameThread so that after reaching that
		// number, \
		// the level is immediately reset - but this is kept as a fail safe
		if (getInfo().getCurrentWave() >= waves.size()) {
			getInfo().reset();
			System.err.println("current waves was out of bounds for level: "
					+ levelID);
		}
		return waves.get(getInfo().getCurrentWave());
	}

	/**
	 * Gets the background.
	 * 
	 * @return the background
	 */
	public Bitmap getBackground() {
		return background;
	}

	/**
	 * Checks for move waves.
	 * 
	 * @return true, if successful
	 */
	public boolean hasMoveWaves() {
		return getInfo().getCurrentWave() < getNumberOfWaves();
	}

	public void increaseAllHealth(int amount) {
		for (Wave w : waves) {
			w.increaseHealth(amount);
		}
	}

	/**
	 * Checks if an x, y point are inside the pause button.
	 * 
	 * @param x
	 *            the x location
	 * @param y
	 *            the y location
	 * @return true, if inside pause button
	 */
	public boolean insidePauseButton(float x, float y) {
		return pauseRect.contains((int) x, (int) y);
	}

	/**
	 * Checks if an x, y point are inside the speed button.
	 * 
	 * @param x
	 *            the x location
	 * @param y
	 *            the y location
	 * @return true, if inside the speed button
	 */
	public boolean insideSpeedButton(float x, float y) {
		return speedRect.contains((int) x, (int) y);
	}

	/**
	 * On draw simply draws the background, then draws the paths[0] and the info
	 * table on top of that.
	 * 
	 * @param canvas
	 *            the canvas to draw on
	 */
	public void onDraw(Canvas canvas) {
		canvas.drawBitmap(background, 0, 0, null);
		for (BugPath p : paths)
			p.onDraw(canvas);
		drawTable(canvas);
	}

	/**
	 * Resets the level to default (starting) values.
	 */
	public void reset() {
		player.resetLevel(levelID);
		player.performUpdate();
	}

	/**
	 * Draws the info table.
	 * 
	 * @param canvas
	 *            the canvas to draw on
	 */
	private void drawTable(Canvas canvas) {
		int xWidth = BUFFER;

		// draw pause and fast forward buttons
		canvas.drawBitmap(pauseIcon, xWidth, BUFFER, null);
		xWidth += pauseIcon.getWidth() + BUFFER;
		canvas.drawBitmap(getSpeedIcon(), xWidth, BUFFER, null);
		xWidth += getSpeedIcon().getWidth() + BUFFER + BUFFER;

		// draw health icon and value
		canvas.drawBitmap(lifeIcon, xWidth, BUFFER, null);
		xWidth += lifeIcon.getWidth() + BUFFER;
		String playerLife = getInfo().getPlayerHealth() + "";
		canvas.drawText(playerLife, xWidth, BUFFER + BUFFER + textHeight,
				textPaint);
		xWidth += healthTextMaxWidth + BUFFER + BUFFER;

		// draw coins icon and value
		canvas.drawBitmap(coinIcon, xWidth, BUFFER, null);
		xWidth += coinIcon.getWidth() + BUFFER;
		String playerCoins = getInfo().getCoins() + "";
		canvas.drawText(playerCoins, xWidth, BUFFER + BUFFER + textHeight,
				textPaint);
		xWidth += healthTextMaxWidth + 4 * BUFFER;

		// draw bugbuck icon and value
		canvas.drawBitmap(bugbuckIcon, xWidth, BUFFER, null);
		xWidth += bugbuckIcon.getWidth() + BUFFER;
		String playerBugBucks = player.getBugBucks() + "";
		canvas.drawText(playerBugBucks, xWidth, BUFFER + BUFFER + textHeight,
				textPaint);
	}

	/**
	 * Saves the current state.
	 * 
	 * @param bugs
	 *            the live bugs
	 * @param towers
	 *            the placed towers
	 */
	public void save(CopyOnWriteArrayList<Bug> bugs,
			CopyOnWriteArrayList<Tower> towers) {
		getInfo().save(bugs, towers,
				waves.get(getInfo().getCurrentWave()).getWaveTime());

	}

	/**
	 * Gets the current speed icon.
	 * 
	 * @return the current speed icon
	 */
	private Bitmap getSpeedIcon() {
		return speedIcon[animationSpeed];
	}

	/**
	 * Sets the speed icon by setting the animation speed.
	 * 
	 * @param newSpeed
	 *            the new speed
	 */
	public void setSpeedIcon(int newSpeed) {
		this.animationSpeed = newSpeed;
	}

	/**
	 * Initializes table graphics elements.
	 * 
	 * @param context
	 *            the context
	 */
	private void initializeTableGraphics(Context context) {
		// initialize textPaint
		textPaint = new Paint();
		textPaint.setTextSize(20);
		textPaint.setFakeBoldText(true);
		textPaint.setColor(Color.WHITE);

		// initialize icons
		this.bugbuckIcon = BMPS.getIcon(BMPS.BUGBUCK);
		this.coinIcon = BMPS.getIcon(BMPS.COIN);
		this.lifeIcon = BMPS.getIcon(BMPS.HEART);
		this.pauseIcon = BMPS.getIcon(BMPS.PAUSE);
		this.speedIcon = new Bitmap[3];
		speedIcon[GameView.SLOW] = BMPS.getIcon(BMPS.SLOW);
		speedIcon[GameView.MEDIUM] = BMPS.getIcon(BMPS.MEDIUM);
		speedIcon[GameView.FAST] = BMPS.getIcon(BMPS.FAST);

		// the speedIcon to be displayed when the animation is at the given
		// speed
		this.animationSpeed = GameView.MEDIUM;

		// find text height and width of max health
		String playerLife = getInfo().getPlayerHealth() + "";
		Rect bounds = new Rect();
		textPaint.getTextBounds(playerLife, 0, playerLife.length(), bounds);
		healthTextMaxWidth = bounds.width();
		textHeight = coinIcon.getHeight() / 2 + BUFFER;

		// create rectangles corresponding to pause and speedIcon button (all
		// should have the same dimensions)
		int left = BUFFER;
		pauseRect = new Rect(left, BUFFER, left + pauseIcon.getWidth(), BUFFER
				+ pauseIcon.getHeight());// (int left, int top, int right, int
											// bottom)
		left += pauseIcon.getWidth() + BUFFER;
		speedRect = new Rect(left, BUFFER, left + getSpeedIcon().getWidth(),
				BUFFER + getSpeedIcon().getHeight());

	}

	private void initializeLevel0(Context context) {
		paths[0].add(new Point(800, 340));
		paths[0].add(new Point(275, 340));
		paths[0].add(new Point(275, 95));
		paths[0].add(new Point(490, 95));
		paths[0].add(new Point(490, 270));
		paths[0].add(new Point(345, 270));
		paths[0].add(new Point(345, 165));
		paths[0].add(new Point(420, 165));
		paths[0].add(new Point(420, 200));
		paths[0].add(new Point(380, 200));
		paths[0].add(new Point(380, 235));
		paths[0].add(new Point(455, 235));
		paths[0].add(new Point(455, 130));
		paths[0].add(new Point(310, 130));
		paths[0].add(new Point(310, 305));
		paths[0].add(new Point(525, 305));
		paths[0].add(new Point(525, 60));
		paths[0].add(new Point(-10, 60));
		paths[0].add(new Point(-100, 60));
		paths[0].setWidth(18);
		paths[0].setColor(Color.argb(135, 23, 86, 0));

		Wave wave0_1 = new Wave(4);
		wave0_1.put(20, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_1.put(30, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_1.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_1.put(50, new Bug(context, paths[0], Bug.ANT, levelID));

		Wave wave0_2 = new Wave(11);
		wave0_2.put(20, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(25, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(30, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(35, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(70, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(80, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(100, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(110, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.put(120, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_2.increaseHealth(60);

		Wave wave0_3 = getNSpacedBugs(context, paths[0], 12, 10, 8, Bug.ANT,
				levelID);
		wave0_3.increaseHealth(150);

		Wave wave0_4 = new Wave(7);
		Bug e1 = new Bug(context, paths[0], Bug.EARWIG, levelID);
		e1.incrementHealth(350);
		Bug e2 = new Bug(context, paths[0], Bug.EARWIG, levelID);
		e2.incrementHealth(400);
		wave0_4.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_4.put(70, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_4.put(80, e1);
		wave0_4.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_4.put(100, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_4.put(110, new Bug(context, paths[0], Bug.ANT, levelID));
		wave0_4.put(120, e2);
		wave0_4.increaseHealth(200);

		Wave wave0_5 = getNSpacedBugs(context, paths[0], 12, 30, 20,
				Bug.EARWIG, levelID);
		wave0_5.increaseHealth(550);

		Wave wave0_6 = getNSpacedBugs(context, paths[0], 5, 10, 12, Bug.EARWIG,
				levelID);
		wave0_6.increaseHealth(500);
		wave0_6.addAll(getNSpacedBugs(context, paths[0], 5, 10, 12, Bug.ANT,
				levelID));
		wave0_6.increaseHealth(250);

		waves.add(wave0_1);
		waves.add(wave0_2);
		waves.add(wave0_3);
		waves.add(wave0_4);
		waves.add(wave0_5);
		waves.add(wave0_6);
	}

	private void initializeLevel3(Context context) {
		paths[7].add(new Point(800, 63));
		paths[7].add(new Point(-100, 63));
		paths[7].setWidth(3);
		paths[7].setColor(Color.argb(135, 200, 135, 5));

		paths[8].add(new Point(800, 117));
		paths[8].add(new Point(-100, 117));
		paths[8].setWidth(3);
		paths[8].setColor(Color.argb(135, 200, 135, 5));

		paths[9].add(new Point(800, 175));
		paths[9].add(new Point(-100, 175));
		paths[9].setWidth(3);
		paths[9].setColor(Color.argb(135, 200, 135, 5));

		paths[10].add(new Point(800, 233));
		paths[10].add(new Point(-100, 233));
		paths[10].setWidth(3);
		paths[10].setColor(Color.argb(135, 200, 135, 5));

		paths[11].add(new Point(800, 290));
		paths[11].add(new Point(-100, 290));
		paths[11].setWidth(3);
		paths[11].setColor(Color.argb(135, 200, 135, 5));

		paths[6].add(new Point(800, 346));
		paths[6].add(new Point(-100, 346));
		paths[6].setWidth(3);
		paths[6].setColor(Color.argb(135, 200, 135, 5));

		//

		paths[0].add(new Point(800, 63));
		paths[0].add(new Point(736, 63));
		paths[0].add(new Point(736, 290));
		paths[0].add(new Point(705, 290));
		paths[0].add(new Point(705, 233));
		paths[0].add(new Point(508, 233));
		paths[0].add(new Point(508, 175));
		paths[0].add(new Point(410, 175));
		paths[0].add(new Point(410, 290));
		paths[0].add(new Point(345, 290));
		paths[0].add(new Point(345, 117));
		paths[0].add(new Point(152, 117));
		paths[0].add(new Point(152, 175));
		paths[0].add(new Point(-10, 175));
		paths[0].add(new Point(-100, 175));
		paths[0].setWidth(3);
		paths[0].setColor(Color.argb(0, 200, 135, 5));

		paths[1].add(new Point(800, 117));
		paths[1].add(new Point(607, 117));
		paths[1].add(new Point(607, 175));
		paths[1].add(new Point(508, 175));
		paths[1].add(new Point(508, 117));
		paths[1].add(new Point(345, 117));
		paths[1].add(new Point(345, 346));
		paths[1].add(new Point(152, 346));
		paths[1].add(new Point(152, 233));
		paths[1].add(new Point(57, 233));
		paths[1].add(new Point(57, 346));
		paths[1].add(new Point(-10, 346));
		paths[1].add(new Point(-100, 346));
		paths[1].setWidth(3);
		paths[1].setColor(Color.argb(0, 120, 120, 20));

		paths[2].add(new Point(800, 175));
		paths[2].add(new Point(736, 175));
		paths[2].add(new Point(736, 345));
		paths[2].add(new Point(607, 345));
		paths[2].add(new Point(607, 63));
		paths[2].add(new Point(508, 63));
		paths[2].add(new Point(508, 346));
		paths[2].add(new Point(410, 346));
		paths[2].add(new Point(410, 233));
		paths[2].add(new Point(345, 233));
		paths[2].add(new Point(345, 175));
		paths[2].add(new Point(152, 175));
		paths[2].add(new Point(152, 117));
		paths[2].add(new Point(-10, 117));
		paths[2].add(new Point(-100, 117));
		paths[2].setWidth(3);
		paths[2].setColor(Color.argb(0, 120, 120, 20));

		paths[3].add(new Point(800, 233));
		paths[3].add(new Point(705, 233));
		paths[3].add(new Point(705, 63));
		paths[3].add(new Point(607, 63));
		paths[3].add(new Point(607, 290));
		paths[3].add(new Point(152, 290));
		paths[3].add(new Point(152, 175));
		paths[3].add(new Point(57, 175));
		paths[3].add(new Point(57, 290));
		paths[3].add(new Point(-10, 290));
		paths[3].add(new Point(-100, 290));
		paths[3].setWidth(3);
		paths[3].setColor(Color.argb(0, 120, 120, 20));

		paths[4].add(new Point(800, 290));
		paths[4].add(new Point(736, 290));
		paths[4].add(new Point(736, 117));
		paths[4].add(new Point(705, 117));
		paths[4].add(new Point(705, 346));
		paths[4].add(new Point(345, 346));
		paths[4].add(new Point(345, 290));
		paths[4].add(new Point(57, 290));
		paths[4].add(new Point(57, 175));
		paths[4].add(new Point(-10, 175));
		paths[4].add(new Point(-100, 175));
		paths[4].setWidth(3);
		paths[4].setColor(Color.argb(0, 120, 120, 20));

		paths[5].add(new Point(800, 346));
		paths[5].add(new Point(508, 346));
		paths[5].add(new Point(508, 233));
		paths[5].add(new Point(152, 233));
		paths[5].add(new Point(152, 63));
		paths[5].add(new Point(-10, 63));
		paths[5].add(new Point(-100, 63));
		paths[5].setWidth(3);
		paths[5].setColor(Color.argb(0, 120, 120, 20));

		Wave wave3_1 = new Wave(27);
		wave3_1.put(20, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_1.put(40, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_1.put(60, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_1.put(80, new Bug(context, paths[0], Bug.BEE, levelID));

		wave3_1.put(21, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_1.put(41, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_1.put(61, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_1.put(81, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_1.put(101, new Bug(context, paths[1], Bug.BEE, levelID));

		wave3_1.put(22, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_1.put(42, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_1.put(62, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_1.put(82, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_1.put(102, new Bug(context, paths[2], Bug.BEE, levelID));

		wave3_1.put(23, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_1.put(43, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_1.put(63, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_1.put(83, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_1.put(103, new Bug(context, paths[3], Bug.BEE, levelID));

		wave3_1.put(24, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_1.put(44, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_1.put(64, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_1.put(84, new Bug(context, paths[4], Bug.BEE, levelID));

		wave3_1.put(25, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_1.put(45, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_1.put(65, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_1.put(85, new Bug(context, paths[5], Bug.BEE, levelID));

		Wave wave3_2 = new Wave(60);
		wave3_2.put(20, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.put(40, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.put(60, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.put(80, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.put(100, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.put(120, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.put(140, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.put(160, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.put(180, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.put(200, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_2.increaseHealth(50);

		wave3_2.put(21, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.put(41, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.put(61, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.put(81, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.put(101, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.put(121, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.put(141, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.put(161, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.put(181, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.put(201, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_2.increaseHealth(100);

		wave3_2.put(22, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_2.put(42, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_2.put(62, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_2.put(82, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_2.put(102, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_2.put(122, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_2.put(142, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_2.put(162, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_2.put(182, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_2.put(202, new Bug(context, paths[2], Bug.BEE, levelID));
		// wave3_2.increaseHealth(150);

		wave3_2.put(23, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_2.put(43, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_2.put(63, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_2.put(83, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_2.put(103, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_2.put(123, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_2.put(143, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_2.put(163, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_2.put(183, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_2.put(203, new Bug(context, paths[3], Bug.BEE, levelID));
		// wave3_2.increaseHealth(200);

		wave3_2.put(24, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_2.put(44, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_2.put(64, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_2.put(84, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_2.put(104, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_2.put(124, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_2.put(144, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_2.put(164, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_2.put(184, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_2.put(204, new Bug(context, paths[4], Bug.BEE, levelID));
		// wave3_2.increaseHealth(250);

		wave3_2.put(25, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_2.put(45, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_2.put(65, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_2.put(85, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_2.put(105, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_2.put(125, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_2.put(145, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_2.put(165, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_2.put(185, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_2.put(205, new Bug(context, paths[5], Bug.BEE, levelID));
		// wave3_2.increaseHealth(500);

		Wave wave3_3 = new Wave(60);
		wave3_3.put(20, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_3.put(40, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_3.put(60, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_3.put(80, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_3.put(100, new Bug(context, paths[0], Bug.BEE, levelID));
		// wave3_3.increaseHealth(200);
		wave3_3.put(120, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_3.put(140, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_3.put(160, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_3.put(180, new Bug(context, paths[0], Bug.BEE, levelID));
		wave3_3.put(200, new Bug(context, paths[0], Bug.BEE, levelID));
		// wave3_3.increaseHealth(400);

		wave3_3.put(21, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_3.put(41, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_3.put(61, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_3.put(81, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_3.put(101, new Bug(context, paths[1], Bug.BEE, levelID));
		// wave3_3.increaseHealth(400);
		wave3_3.put(121, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_3.put(141, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_3.put(161, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_3.put(181, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_3.put(201, new Bug(context, paths[1], Bug.BEE, levelID));
		wave3_3.increaseHealth(300);

		wave3_3.put(22, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.put(42, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.put(62, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.put(82, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.put(102, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.increaseHealth(100);
		wave3_3.put(122, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.put(142, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.put(162, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.put(182, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.put(202, new Bug(context, paths[2], Bug.BEE, levelID));
		wave3_3.increaseHealth(300);

		wave3_3.put(23, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_3.put(43, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_3.put(63, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_3.put(83, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_3.put(103, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_3.increaseHealth(200);
		wave3_3.put(123, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_3.put(143, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_3.put(163, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_3.put(183, new Bug(context, paths[3], Bug.BEE, levelID));
		wave3_3.put(203, new Bug(context, paths[3], Bug.BEE, levelID));

		wave3_3.put(24, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_3.put(44, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_3.put(64, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_3.put(84, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_3.put(104, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_3.put(124, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_3.put(144, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_3.put(164, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_3.put(184, new Bug(context, paths[4], Bug.BEE, levelID));
		wave3_3.put(204, new Bug(context, paths[4], Bug.BEE, levelID));

		wave3_3.put(25, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_3.put(45, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_3.put(65, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_3.put(85, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_3.put(105, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_3.put(125, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_3.put(145, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_3.put(165, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_3.put(185, new Bug(context, paths[5], Bug.BEE, levelID));
		wave3_3.put(205, new Bug(context, paths[5], Bug.BEE, levelID));
		// wave3_3.increaseHealth(900);

		waves.add(wave3_1);
		waves.add(wave3_2);
		waves.add(wave3_3);
	}

	private void initializeLevel5(Context context) {
		// ////////////////////////////////////////////////////////////////////////////////////////
		// cole's dotted map

		paths[0].setColor(Color.argb(0, 275, 135, 5));
		paths[0].add(new Point(800, 20));
		paths[0].add(new Point(500, 20));
		paths[0].add(new Point(500, 100));
		paths[0].add(new Point(350, 100));
		paths[0].add(new Point(350, 200));
		paths[0].add(new Point(250, 200));
		paths[0].add(new Point(250, 350));
		paths[0].add(new Point(100, 350));
		paths[0].add(new Point(-10, 350));
		paths[0].add(new Point(-100, 350));

		paths[1].setColor(Color.argb(0, 200, 35, 5));
		paths[1].add(new Point(800, 250));
		paths[1].add(new Point(600, 250));
		paths[1].add(new Point(600, 350));
		paths[1].add(new Point(400, 350));
		paths[1].add(new Point(400, 250));
		paths[1].add(new Point(200, 250));
		paths[1].add(new Point(200, 200));
		paths[1].add(new Point(-10, 200));
		paths[1].add(new Point(-100, 200));

		paths[2].setColor(Color.argb(0, 200, 300, 5));
		paths[2].add(new Point(50, 450));
		paths[2].add(new Point(50, 270));
		paths[2].add(new Point(-10, 270));
		paths[2].add(new Point(-100, 270));

		paths[3].setColor(Color.argb(0, 24, 200, 5));
		paths[3].add(new Point(203, 0));
		paths[3].add(new Point(203, 115));
		paths[3].add(new Point(110, 115));
		paths[3].add(new Point(110, 90));
		paths[3].add(new Point(-10, 90));
		paths[3].add(new Point(-100, 90));

		paths[4].setColor(Color.argb(0, 200, 135, 5));
		paths[4].add(new Point(800, 130));
		paths[4].add(new Point(700, 130));
		paths[4].add(new Point(700, 190));
		paths[4].add(new Point(500, 190));
		paths[4].add(new Point(500, 100));
		paths[4].add(new Point(350, 100));
		paths[4].add(new Point(350, 70));
		paths[4].add(new Point(203, 70));
		paths[4].add(new Point(203, 115));
		paths[4].add(new Point(110, 115));
		paths[4].add(new Point(110, 90));
		paths[4].add(new Point(-10, 90));
		paths[4].add(new Point(-100, 90));

		paths[5].setColor(Color.argb(0, 135, 135, 135));
		paths[5].add(new Point(800, 250));
		paths[5].add(new Point(400, 250));
		paths[5].add(new Point(-10, 250));
		paths[5].add(new Point(-100, 250));

		// ////////////////////////////////////////////////////////////////

		Wave wave5_1 = new Wave(10);

		wave5_1.put(10, new Bug(context, paths[0], Bug.ANT, levelID));
		wave5_1.put(30, new Bug(context, paths[0], Bug.BEE, levelID));
		wave5_1.put(50, new Bug(context, paths[0], Bug.BEE, levelID));
		wave5_1.put(21, new Bug(context, paths[0], Bug.BEDBUG, levelID));
		wave5_1.put(41, new Bug(context, paths[0], Bug.ANT, levelID));

		wave5_1.put(20, new Bug(context, paths[1], Bug.BEE, levelID));
		wave5_1.put(40, new Bug(context, paths[1], Bug.BEE, levelID));
		wave5_1.put(11, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave5_1.put(31, new Bug(context, paths[1], Bug.ROACH, levelID));
		wave5_1.put(51, new Bug(context, paths[1], Bug.ANT, levelID));

		waves.add(wave5_1);

		// ////////////////////////////////////////////////////////////////

		Wave wave5_2 = new Wave(2);
		wave5_2.put(10, new Bug(context, paths[2], Bug.EARWIG, levelID));
		wave5_2.addAll(getCentipede(context, paths[0], levelID));
		wave5_2.addAll(getCentipede(context, paths[1], levelID));

		Wave wave5_2_2 = new Wave(8);
		wave5_2_2.increaseHealth(120);
		wave5_2_2.put(20, new Bug(context, paths[4], Bug.ANT, levelID));
		wave5_2_2.put(30, new Bug(context, paths[4], Bug.ANT, levelID));
		wave5_2_2.put(40, new Bug(context, paths[4], Bug.EARWIG, levelID));
		wave5_2_2.put(50, new Bug(context, paths[4], Bug.ANT, levelID));
		wave5_2_2.put(60, new Bug(context, paths[4], Bug.ANT, levelID));
		wave5_2_2.put(70, new Bug(context, paths[4], Bug.EARWIG, levelID));
		wave5_2_2.put(90, new Bug(context, paths[2], Bug.ANT, levelID));
		wave5_2_2.put(90, new Bug(context, paths[3], Bug.ANT, levelID));

		wave5_2.addAll(wave5_2_2);

		waves.add(wave5_2);

		// ////////////////////////////////////////////////////////////////

		Wave wave5_3 = new Wave(6);
		wave5_3.put(25, new Bug(context, paths[0], Bug.BEDBUG, levelID));
		Bug annoying_tick = new Bug(context, paths[5], Bug.TICK, levelID);
		annoying_tick.incrementHealth(100);
		wave5_3.put(35, annoying_tick);
		wave5_3.put(25, new Bug(context, paths[1], Bug.BEDBUG, levelID));
		wave5_3.put(55, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave5_3.addAll(getCentipede(context, paths[2], levelID));
		wave5_3.increaseHealth(300);

		waves.add(wave5_3);

		// ////////////////////////////////////////////////////////////////

		// ////////////////////////////////////////////////////////////////////////////////////////
	}

	private void initializeLevel6(Context context) {

		paths[0].add(new Point(800, 80));
		paths[0].add(new Point(100, 80));
		paths[0].add(new Point(700, 80));
		paths[0].add(new Point(10, 80));
		paths[0].add(new Point(110, 80));
		paths[0].add(new Point(110, 330));
		paths[0].add(new Point(593, 330));
		paths[0].add(new Point(110, 330));
		paths[0].add(new Point(110, 80));
		paths[0].add(new Point(-10, 80));
		paths[0].add(new Point(-100, 80));
		paths[0].setColor(Color.argb(0, 120, 120, 20));

		paths[1].add(new Point(800, 330));
		paths[1].add(new Point(100, 330));
		paths[1].add(new Point(700, 330));
		paths[1].add(new Point(10, 330));
		paths[1].add(new Point(110, 330));
		paths[1].add(new Point(110, 80));
		paths[1].add(new Point(770, 80));
		paths[1].add(new Point(110, 80));
		paths[1].add(new Point(110, 330));
		paths[1].add(new Point(-10, 330));
		paths[1].add(new Point(-100, 330));
		paths[1].setColor(Color.argb(0, 120, 120, 20));

		paths[2].add(new Point(770, -10));
		paths[2].add(new Point(770, 80));
		paths[2].add(new Point(110, 80));
		paths[2].add(new Point(110, 330));
		paths[2].add(new Point(-10, 330));
		paths[2].add(new Point(-100, 330));
		paths[2].setColor(Color.argb(0, 120, 120, 20));

		paths[3].add(new Point(593, 410));
		paths[3].add(new Point(593, 330));
		paths[3].add(new Point(110, 330));
		paths[3].add(new Point(110, 80));
		paths[3].add(new Point(-10, 80));
		paths[3].add(new Point(-100, 80));
		paths[3].setColor(Color.argb(0, 120, 120, 20));

		paths[4].add(new Point(800, 80));
		paths[4].add(new Point(100, 80));
		paths[4].add(new Point(700, 80));
		paths[4].add(new Point(-10, 80));
		paths[4].add(new Point(-100, 80));
		paths[4].setColor(Color.argb(0, 120, 120, 20));

		paths[5].add(new Point(424, 330));
		paths[5].add(new Point(110, 330));
		paths[5].add(new Point(110, 80));
		paths[5].add(new Point(10, 80));
		paths[5].add(new Point(100, 80));
		paths[5].add(new Point(-10, 80));
		paths[5].add(new Point(-100, 80));
		paths[5].setColor(Color.argb(0, 120, 120, 20));

		Wave wave6_1 = getCentipede(context, paths[2], levelID);
		wave6_1.addAll(getNSpacedBugs(context, paths[0], 5, 10, 12, Bug.EARWIG,
				levelID));
		wave6_1.addAll(getNSpacedBugs(context, paths[1], 5, 10, 12, Bug.ANT,
				levelID));
		wave6_1.addAll(getNSpacedBugs(context, paths[3], 5, 10, 12, Bug.EARWIG,
				levelID));
		wave6_1.addAll(getNSpacedBugs(context, paths[0], 5, 10, 12, Bug.ROACH,
				levelID));

		Wave wave6_2 = getCentipede(context, paths[4], levelID);
		wave6_2.addAll(getNSpacedBugs(context, paths[1], 5, 10, 12,
				Bug.RED_ANT, levelID));
		wave6_2.addAll(getNSpacedBugs(context, paths[5], 5, 10, 12, Bug.ANT,
				levelID));
		wave6_2.addAll(getNSpacedBugs(context, paths[2], 5, 10, 12, Bug.EARWIG,
				levelID));
		wave6_2.addAll(getNSpacedBugs(context, paths[0], 5, 10, 12, Bug.TICK,
				levelID));
		wave6_2.increaseHealth(200);

		Wave wave6_3 = getCentipede(context, paths[5], levelID);
		wave6_3.addAll(getNSpacedBugs(context, paths[2], 5, 10, 12,
				Bug.RED_ANT, levelID));
		wave6_3.addAll(getNSpacedBugs(context, paths[3], 5, 10, 12, Bug.ANT,
				levelID));
		wave6_3.addAll(getNSpacedBugs(context, paths[4], 5, 10, 12, Bug.EARWIG,
				levelID));
		wave6_3.addAll(getNSpacedBugs(context, paths[0], 5, 10, 12, Bug.BEDBUG,
				levelID));
		wave6_3.addAll(getNSpacedBugs(context, paths[1], 5, 10, 12, Bug.TICK,
				levelID));
		wave6_3.increaseHealth(300);

		Wave wave6_4 = getNSpacedBugs(context, paths[0], 2, 10, 12, Bug.BEE,
				levelID);
		wave6_4.addAll(getNSpacedBugs(context, paths[1], 5, 10, 12,
				Bug.RED_ANT, levelID));
		wave6_4.addAll(getNSpacedBugs(context, paths[4], 5, 10, 12, Bug.ANT,
				levelID));
		wave6_4.addAll(getNSpacedBugs(context, paths[2], 5, 10, 12, Bug.ROACH,
				levelID));
		wave6_4.addAll(getCentipede(context, paths[5], levelID));
		wave6_4.addAll(getNSpacedBugs(context, paths[3], 5, 10, 12, Bug.BEDBUG,
				levelID));
		wave6_4.addAll(getNSpacedBugs(context, paths[4], 5, 10, 12, Bug.TICK,
				levelID));
		wave6_4.increaseHealth(400);

		waves.add(wave6_1);
		waves.add(wave6_2);
		waves.add(wave6_3);
		waves.add(wave6_4);
	}

	private void initializeLevel7(Context context) {
		// ////////////////////////////////////////////////////////////////////////////////////////
		// cole's paint chip map
		paths[0].setWidth(10);
		paths[0].setColor(Color.argb(0, 98, 194, 170));
		paths[0].add(new Point(800, 50));
		paths[0].add(new Point(500, 50));
		paths[0].add(new Point(500, 350));
		paths[0].add(new Point(300, 350));
		paths[0].add(new Point(-10, 350));
		paths[0].add(new Point(-100, 350));

		paths[1].setWidth(10);
		paths[1].setColor(Color.argb(0, 208, 300, 100));
		paths[1].add(new Point(800, 200));
		paths[1].add(new Point(400, 200));
		paths[1].add(new Point(400, 100));
		paths[1].add(new Point(200, 100));
		paths[1].add(new Point(-10, 100));
		paths[1].add(new Point(-100, 100));

		paths[2].setWidth(10);
		paths[2].setColor(Color.argb(0, 25, 194, 6));
		paths[2].add(new Point(800, 300));
		paths[2].add(new Point(400, 300));
		paths[2].add(new Point(200, 300));
		paths[2].add(new Point(-10, 300));
		paths[2].add(new Point(-100, 300));

		paths[3].setWidth(10);
		paths[3].setColor(Color.argb(0, 208, 194, 170));
		paths[3].add(new Point(300, -10));
		paths[3].add(new Point(300, 200));
		paths[3].add(new Point(200, 200));
		paths[3].add(new Point(-10, 200));
		paths[3].add(new Point(-100, 200));

		paths[4].setWidth(10);
		paths[4].setColor(Color.argb(0, 315, 200, 170));
		paths[4].add(new Point(100, 420));
		paths[4].add(new Point(100, 250));
		paths[4].add(new Point(600, 250));
		paths[4].add(new Point(600, 75));
		paths[4].add(new Point(200, 75));
		paths[4].add(new Point(200, 200));
		paths[4].add(new Point(400, 200));
		paths[4].add(new Point(400, 75));
		paths[4].add(new Point(-10, 75));
		paths[4].add(new Point(-100, 75));

		//

		Wave wave7_1 = new Wave(20);
		wave7_1.put(20, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(30, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(40, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(50, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(60, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(70, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(80, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(90, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(100, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(110, new Bug(context, paths[4], Bug.ANT, levelID));
		wave7_1.put(21, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.put(31, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.put(41, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.put(51, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.put(61, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.put(71, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.put(81, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.put(91, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.put(101, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.put(111, new Bug(context, paths[2], Bug.ANT, levelID));
		wave7_1.addAll(getCentipede(context, paths[3], levelID));
		waves.add(wave7_1);

		Wave wave7_2 = new Wave(11);
		wave7_2.put(20, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(30, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(40, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(50, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(60, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(70, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(80, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(90, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(100, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(110, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_2.put(51, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave7_2.addAll(getCentipede(context, paths[0], levelID));
		wave7_2.addAll(getCentipede(context, paths[1], levelID));
		wave7_2.addAll(getCentipede(context, paths[2], levelID));
		wave7_2.addAll(getCentipede(context, paths[2], levelID));
		wave7_2.addAll(getCentipede(context, paths[0], levelID));
		wave7_2.addAll(getCentipede(context, paths[3], levelID));
		wave7_2.addAll(getCentipede(context, paths[1], levelID));
		wave7_2.addAll(getCentipede(context, paths[1], levelID));

		waves.add(wave7_2);

		Wave wave7_3 = new Wave(44);

		wave7_3.put(20, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_3.put(30, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave7_3.put(40, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave7_3.put(50, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(60, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave7_3.put(70, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_3.put(80, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave7_3.put(90, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(100, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(110, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(110, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(21, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_3.put(31, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave7_3.put(41, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave7_3.put(51, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(61, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave7_3.put(71, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_3.put(81, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave7_3.put(91, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(101, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(111, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(111, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(22, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_3.put(32, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave7_3.put(42, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave7_3.put(52, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(62, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave7_3.put(72, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_3.put(82, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave7_3.put(92, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(102, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(112, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(112, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(23, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_3.put(33, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave7_3.put(43, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave7_3.put(53, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(63, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave7_3.put(73, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave7_3.put(83, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave7_3.put(93, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(103, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(113, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.put(113, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave7_3.increaseHealth(200);

		waves.add(wave7_3);

		// ////////////////////////////////////////////////////////////////////////////////////////
	}

	private void initializeLevel9(Context context) {
		paths[0].add(new Point(800, 163));
		paths[0].add(new Point(600, 163));
		paths[0].add(new Point(700, 163));
		paths[0].add(new Point(500, 163));
		paths[0].add(new Point(600, 163));
		paths[0].add(new Point(400, 163));
		paths[0].add(new Point(500, 163));
		paths[0].add(new Point(300, 163));
		paths[0].add(new Point(400, 163));
		paths[0].add(new Point(200, 163));
		paths[0].add(new Point(300, 163));
		paths[0].add(new Point(100, 163));
		paths[0].add(new Point(200, 163));
		paths[0].add(new Point(-10, 163));
		paths[0].add(new Point(-100, 163));
		paths[0].setWidth(3);
		paths[0].setColor(Color.argb(0, 120, 120, 20));

		paths[1].add(new Point(249, 450));
		paths[1].add(new Point(249, 341));
		paths[1].add(new Point(100, 341));
		paths[1].add(new Point(249, 341));
		paths[1].add(new Point(249, 400));
		paths[1].add(new Point(249, 341));
		paths[1].add(new Point(-10, 341));
		paths[1].add(new Point(-100, 341));
		paths[1].setWidth(3);
		paths[1].setColor(Color.argb(0, 120, 120, 20));

		paths[2].add(new Point(800, 163));
		paths[2].add(new Point(-10, 163));
		paths[2].add(new Point(-100, 163));
		paths[2].setWidth(3);
		paths[2].setColor(Color.argb(0, 120, 120, 20));

		paths[3].add(new Point(249, 450));
		paths[3].add(new Point(249, 341));
		paths[3].add(new Point(-10, 341));
		paths[3].add(new Point(-100, 341));
		paths[3].setWidth(3);
		paths[3].setColor(Color.argb(0, 120, 120, 20));

		paths[4].add(new Point(800, 341));
		paths[4].add(new Point(-10, 341));
		paths[4].add(new Point(-100, 341));
		paths[4].setWidth(3);
		paths[4].setColor(Color.argb(0, 120, 120, 20));

		Wave wave9_1 = getCentipede(context, paths[0], levelID);
		wave9_1.addAll(getCentipede(context, paths[1], levelID));
		Wave wave9_2 = getNSpacedBugs(context, paths[0], 9, 5, 12, Bug.RED_ANT,
				levelID);
		wave9_2.addAll(getCentipede(context, paths[1], levelID));
		wave9_2.put(100, new Bug(context, paths[0], Bug.BEDBUG, levelID));
		wave9_2.put(101, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave9_2.increaseHealth(200);

		Wave wave9_3 = new Wave(96);
		wave9_3.put(50, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_3.put(51, new Bug(context, paths[1], Bug.BEE, levelID));
		wave9_3.put(52, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave9_3.put(53, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave9_3.put(54, new Bug(context, paths[4], Bug.TICK, levelID));

		wave9_3.put(70, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_3.put(71, new Bug(context, paths[1], Bug.BEE, levelID));
		wave9_3.put(72, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave9_3.put(73, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave9_3.put(74, new Bug(context, paths[4], Bug.TICK, levelID));

		wave9_3.put(90, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_3.put(91, new Bug(context, paths[1], Bug.BEE, levelID));
		wave9_3.put(92, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave9_3.put(93, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave9_3.put(94, new Bug(context, paths[4], Bug.TICK, levelID));
		wave9_3.put(95, new Bug(context, paths[0], Bug.ANT, levelID));

		wave9_3.addAll(getCentipede(context, paths[0], levelID));
		wave9_3.addAll(getCentipede(context, paths[1], levelID));
		wave9_3.addAll(getCentipede(context, paths[2], levelID));
		wave9_3.addAll(getCentipede(context, paths[3], levelID));
		wave9_3.addAll(getCentipede(context, paths[4], levelID));
		wave9_3.increaseHealth(350);

		Wave wave9_4 = new Wave();
		wave9_4.put(50, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_4.put(51, new Bug(context, paths[1], Bug.BEE, levelID));
		wave9_4.put(52, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave9_4.put(53, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave9_4.put(54, new Bug(context, paths[4], Bug.TICK, levelID));

		wave9_4.put(70, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_4.put(71, new Bug(context, paths[1], Bug.BEE, levelID));
		wave9_4.put(72, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave9_4.put(73, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave9_4.put(74, new Bug(context, paths[4], Bug.TICK, levelID));

		wave9_4.put(90, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_4.put(91, new Bug(context, paths[1], Bug.BEE, levelID));
		wave9_4.put(92, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave9_4.put(93, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave9_4.put(94, new Bug(context, paths[4], Bug.TICK, levelID));
		wave9_4.put(95, new Bug(context, paths[0], Bug.ANT, levelID));

		wave9_4.put(110, new Bug(context, paths[2], Bug.ROACH, levelID));
		wave9_4.put(120, new Bug(context, paths[2], Bug.BEE, levelID));
		wave9_4.put(130, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave9_4.put(140, new Bug(context, paths[2], Bug.BEDBUG, levelID));
		wave9_4.put(150, new Bug(context, paths[2], Bug.TICK, levelID));

		wave9_4.put(111, new Bug(context, paths[3], Bug.ROACH, levelID));
		wave9_4.put(121, new Bug(context, paths[3], Bug.BEE, levelID));
		wave9_4.put(131, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave9_4.put(142, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave9_4.put(153, new Bug(context, paths[3], Bug.TICK, levelID));

		wave9_4.put(112, new Bug(context, paths[4], Bug.ROACH, levelID));
		wave9_4.put(122, new Bug(context, paths[4], Bug.BEE, levelID));
		wave9_4.put(133, new Bug(context, paths[4], Bug.RED_ANT, levelID));
		wave9_4.put(144, new Bug(context, paths[4], Bug.BEDBUG, levelID));
		wave9_4.put(155, new Bug(context, paths[4], Bug.TICK, levelID));
		Bug a1 = new Bug(context, paths[4], Bug.ANT, levelID);
		a1.incrementHealth(200);
		wave9_4.put(180, a1);
		wave9_4.increaseHealth(300);

		wave9_4.addAll(getCentipede(context, paths[0], levelID));
		wave9_4.addAll(getCentipede(context, paths[1], levelID));
		wave9_4.addAll(getCentipede(context, paths[2], levelID));
		wave9_4.addAll(getCentipede(context, paths[3], levelID));
		wave9_4.addAll(getCentipede(context, paths[4], levelID));

		Wave wave9_5 = new Wave();
		wave9_5.put(50, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_5.put(51, new Bug(context, paths[1], Bug.ROACH, levelID));
		wave9_5.put(52, new Bug(context, paths[2], Bug.ROACH, levelID));
		wave9_5.put(53, new Bug(context, paths[3], Bug.ROACH, levelID));
		wave9_5.put(54, new Bug(context, paths[4], Bug.ROACH, levelID));

		wave9_5.put(70, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_5.put(71, new Bug(context, paths[1], Bug.ROACH, levelID));
		wave9_5.put(72, new Bug(context, paths[2], Bug.ROACH, levelID));
		wave9_5.put(73, new Bug(context, paths[3], Bug.ROACH, levelID));
		wave9_5.put(74, new Bug(context, paths[4], Bug.ROACH, levelID));

		wave9_5.put(90, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_5.put(91, new Bug(context, paths[1], Bug.ROACH, levelID));
		wave9_5.put(92, new Bug(context, paths[2], Bug.ROACH, levelID));
		wave9_5.put(93, new Bug(context, paths[3], Bug.ROACH, levelID));
		wave9_5.put(94, new Bug(context, paths[4], Bug.ROACH, levelID));
		wave9_5.put(95, new Bug(context, paths[0], Bug.ROACH, levelID));
		wave9_5.increaseHealth(550);

		Wave wave9_6 = new Wave();
		wave9_6.put(10, new Bug(context, paths[2], Bug.ANT, levelID));
		wave9_6.put(21, new Bug(context, paths[2], Bug.ANT, levelID));
		wave9_6.put(32, new Bug(context, paths[2], Bug.ANT, levelID));
		wave9_6.put(43, new Bug(context, paths[2], Bug.ANT, levelID));
		wave9_6.put(54, new Bug(context, paths[2], Bug.ANT, levelID));
		wave9_6.increaseHealth(200);

		wave9_6.put(11, new Bug(context, paths[3], Bug.EARWIG, levelID));
		wave9_6.put(20, new Bug(context, paths[3], Bug.EARWIG, levelID));
		wave9_6.put(30, new Bug(context, paths[3], Bug.EARWIG, levelID));
		wave9_6.put(40, new Bug(context, paths[3], Bug.EARWIG, levelID));
		wave9_6.put(50, new Bug(context, paths[3], Bug.EARWIG, levelID));
		wave9_6.increaseHealth(200);

		wave9_6.put(12, new Bug(context, paths[4], Bug.RED_ANT, levelID));
		wave9_6.put(22, new Bug(context, paths[4], Bug.RED_ANT, levelID));
		wave9_6.put(31, new Bug(context, paths[4], Bug.RED_ANT, levelID));
		wave9_6.put(41, new Bug(context, paths[4], Bug.RED_ANT, levelID));
		wave9_6.put(51, new Bug(context, paths[4], Bug.RED_ANT, levelID));
		wave9_6.put(61, new Bug(context, paths[4], Bug.RED_ANT, levelID));
		wave9_6.increaseHealth(350);

		Wave wave9_7 = new Wave();
		wave9_7.put(10, new Bug(context, paths[2], Bug.TICK, levelID));
		wave9_7.put(21, new Bug(context, paths[2], Bug.TICK, levelID));
		wave9_7.put(32, new Bug(context, paths[2], Bug.TICK, levelID));
		wave9_7.put(43, new Bug(context, paths[2], Bug.TICK, levelID));
		wave9_7.put(54, new Bug(context, paths[2], Bug.TICK, levelID));
		wave9_7.increaseHealth(200);

		wave9_7.put(11, new Bug(context, paths[3], Bug.TICK, levelID));
		wave9_7.put(20, new Bug(context, paths[3], Bug.TICK, levelID));
		wave9_7.put(30, new Bug(context, paths[3], Bug.TICK, levelID));
		wave9_7.put(40, new Bug(context, paths[3], Bug.TICK, levelID));
		wave9_7.put(50, new Bug(context, paths[3], Bug.TICK, levelID));
		wave9_7.increaseHealth(200);

		wave9_7.put(12, new Bug(context, paths[4], Bug.TICK, levelID));
		wave9_7.put(22, new Bug(context, paths[4], Bug.TICK, levelID));
		wave9_7.put(31, new Bug(context, paths[4], Bug.TICK, levelID));
		wave9_7.put(41, new Bug(context, paths[4], Bug.TICK, levelID));
		wave9_7.put(51, new Bug(context, paths[4], Bug.TICK, levelID));
		wave9_7.put(61, new Bug(context, paths[4], Bug.TICK, levelID));
		wave9_7.increaseHealth(350);

		waves.add(wave9_1);
		waves.add(wave9_2);
		waves.add(wave9_3);
		waves.add(wave9_4);
		waves.add(wave9_5);
		waves.add(wave9_6);
		waves.add(wave9_7);
	}

	private void initializeLevel13(Context context) {
		
		paths[0].add(new Point(800, 10));
		paths[0].add(new Point(572, 10));
		paths[0].add(new Point(372, 10));
		paths[0].add(new Point(-10, 10));
		paths[0].add(new Point(-100, 10));
		paths[0].setColor(Color.argb(0, 120, 120, 20));
		paths[0].setWidth(10);
		
		paths[1].add(new Point(800, 100));
		paths[1].add(new Point(572, 100));
		paths[1].add(new Point(572, 200));
		paths[1].add(new Point(372, 200));
		paths[1].add(new Point(-10, 200));
		paths[1].add(new Point(-100, 200));
		paths[1].setColor(Color.argb(0, 120, 120, 20));
		paths[1].setWidth(10);
		
		paths[2].add(new Point(800, 200));
		paths[2].add(new Point(572, 200));
		paths[2].add(new Point(372, 200));
		paths[2].add(new Point(-10, 200));
		paths[2].add(new Point(-100, 200));
		paths[2].setColor(Color.argb(0, 120, 120, 20));
		paths[2].setWidth(10);
		
		paths[3].add(new Point(800, 300));
		paths[3].add(new Point(572, 300));
		paths[3].add(new Point(372, 300));
		paths[3].add(new Point(-10, 300));
		paths[3].add(new Point(-100, 300));
		paths[3].setColor(Color.argb(0, 120, 120, 20));
		paths[3].setWidth(10);
		
		paths[4].add(new Point(800, 150));
		paths[4].add(new Point(572, 150));
		paths[4].add(new Point(372, 150));
		paths[4].add(new Point(-10, 150));
		paths[4].add(new Point(-100, 150));
		paths[4].setColor(Color.argb(0, 120, 120, 20));
		paths[4].setWidth(10);
		
		paths[5].add(new Point(800, 250));
		paths[5].add(new Point(572, 250));
		paths[5].add(new Point(372, 250));
		paths[5].add(new Point(-10, 250));
		paths[5].add(new Point(-100, 250));
		paths[5].setColor(Color.argb(0, 120, 120, 20));
		paths[5].setWidth(10);
		
		paths[6].add(new Point(800, 150));
		paths[6].add(new Point(572, 150));
		paths[6].add(new Point(572, 90));
		paths[6].add(new Point(372, 90));
		paths[6].add(new Point(-10, 90));
		paths[6].add(new Point(-100, 90));
		paths[6].setColor(Color.argb(0, 120, 120, 20));
		paths[6].setWidth(10);
		
		paths[7].add(new Point(800, 300));
		paths[7].add(new Point(572, 300));
		paths[7].add(new Point(572, 375));
		paths[7].add(new Point(372, 375));
		paths[7].add(new Point(-10, 375));
		paths[7].add(new Point(-100, 375));
		paths[7].setColor(Color.argb(0, 120, 120, 20));
		paths[7].setWidth(10);
		
		Wave wave13_1 = new Wave(20);
		wave13_1.put(10, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_1.put(20, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_1.put(30, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_1.put(40, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_1.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_1.put(60, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_1.put(70, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_1.put(80, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_1.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_1.put(100, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_1.put(110, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_1.put(120, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_1.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_1.put(140, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_1.put(150, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_1.put(160, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_1.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_1.put(180, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_1.put(190, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_1.put(200, new Bug(context, paths[3], Bug.ANT, levelID));
		waves.add(wave13_1);
		
		Wave wave13_2 = new Wave(20);
		wave13_2.put(10, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave13_2.put(20, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave13_2.put(30, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave13_2.put(40, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_2.put(50, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave13_2.put(60, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave13_2.put(70, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave13_2.put(80, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_2.put(90, new Bug(context, paths[0], Bug.BEDBUG, levelID));
		wave13_2.put(100, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave13_2.put(110, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave13_2.put(120, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_2.put(130, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave13_2.put(140, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave13_2.put(150, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave13_2.put(160, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_2.put(170, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave13_2.put(180, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave13_2.put(190, new Bug(context, paths[2], Bug.BEDBUG, levelID));
		wave13_2.put(200, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_2.increaseHealth(100);
		waves.add(wave13_2);
		
		Wave wave13_3 = new Wave(20);
		wave13_3.put(10, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave13_3.put(20, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave13_3.put(30, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave13_3.put(40, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_3.put(50, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave13_3.put(60, new Bug(context, paths[1], Bug.BEDBUG, levelID));
		wave13_3.put(70, new Bug(context, paths[2], Bug.EARWIG, levelID));
		wave13_3.put(80, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_3.put(90, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave13_3.put(100, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave13_3.put(110, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave13_3.put(120, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_3.put(130, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave13_3.put(140, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave13_3.put(150, new Bug(context, paths[2], Bug.BEDBUG, levelID));
		wave13_3.put(160, new Bug(context, paths[3], Bug.EARWIG, levelID));
		wave13_3.put(170, new Bug(context, paths[0], Bug.EARWIG, levelID));
		wave13_3.put(180, new Bug(context, paths[1], Bug.BEDBUG, levelID));
		wave13_3.put(190, new Bug(context, paths[2], Bug.RED_ANT, levelID));
		wave13_3.put(200, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_3.increaseHealth(100);
		wave13_3.addAll(getCentipede(context, paths[6], levelID));
		wave13_3.addAll(getCentipede(context, paths[6], levelID));
		wave13_3.addAll(getCentipede(context, paths[5], levelID));
		wave13_3.addAll(getCentipede(context, paths[5], levelID));
		wave13_3.increaseHealth(200);
		waves.add(wave13_3);
		
		Wave wave13_4 = new Wave(20);
		wave13_4.put(10, new Bug(context, paths[0], Bug.BEDBUG, levelID));
		wave13_4.put(20, new Bug(context, paths[1], Bug.BEDBUG, levelID));
		wave13_4.put(30, new Bug(context, paths[2], Bug.BEDBUG, levelID));
		wave13_4.put(40, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave13_4.put(50, new Bug(context, paths[0], Bug.TICK, levelID));   
		wave13_4.put(60, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_4.put(70, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_4.put(80, new Bug(context, paths[3], Bug.BEDBUG, levelID));
		wave13_4.put(90, new Bug(context, paths[0], Bug.BEDBUG, levelID));
		wave13_4.put(100, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_4.put(110, new Bug(context, paths[2], Bug.BEDBUG, levelID));
		wave13_4.put(120, new Bug(context, paths[3], Bug.ANT, levelID));   
		wave13_4.put(130, new Bug(context, paths[0], Bug.BEDBUG, levelID));
		wave13_4.put(140, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_4.put(150, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_4.put(160, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_4.put(170, new Bug(context, paths[0], Bug.BEDBUG, levelID));
		wave13_4.put(180, new Bug(context, paths[1], Bug.BEDBUG, levelID));
		wave13_4.put(190, new Bug(context, paths[2], Bug.BEDBUG, levelID));
		wave13_4.put(200, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_4.increaseHealth(100);
		wave13_3.addAll(getCentipede(context, paths[6], levelID));
		wave13_3.addAll(getCentipede(context, paths[6], levelID));
		wave13_3.addAll(getCentipede(context, paths[5], levelID));
		wave13_3.addAll(getCentipede(context, paths[5], levelID));
		wave13_3.increaseHealth(350);
		wave13_3.increaseHealth(200);
		waves.add(wave13_4);
		
		Wave wave13_5 = new Wave(20);
		wave13_5.put(10, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_5.put(20, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_5.put(30, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_5.put(40, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_5.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_5.put(60, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_5.put(70, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_5.put(80, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_5.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_5.put(100, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_5.put(110, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_5.put(120, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_5.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_5.put(140, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_5.put(150, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_5.put(160, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_5.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
		wave13_5.put(180, new Bug(context, paths[1], Bug.ANT, levelID));
		wave13_5.put(190, new Bug(context, paths[2], Bug.ANT, levelID));
		wave13_5.put(200, new Bug(context, paths[3], Bug.ANT, levelID));
		wave13_5.increaseHealth(100);
		waves.add(wave13_5);
		
	}

	private void initializeLevel11(Context context) {
		paths[0].add(new Point(800, 10));
		paths[0].add(new Point(572, 10));
		paths[0].add(new Point(372, 10));
		paths[0].add(new Point(-10, 10));
		paths[0].add(new Point(-100, 10));
		paths[0].setColor(Color.argb(0, 120, 120, 20));
		paths[0].setWidth(10);
		paths[1].add(new Point(800, 100));
		paths[1].add(new Point(572, 100));
		paths[1].add(new Point(372, 100));
		paths[1].add(new Point(-10, 100));
		paths[1].add(new Point(-100, 100));
		paths[1].setColor(Color.argb(0, 120, 120, 20));
		paths[1].setWidth(10);
		paths[2].add(new Point(800, 10));
		paths[2].add(new Point(572, 200));
		paths[2].add(new Point(372, 200));
		paths[2].add(new Point(-10, 200));
		paths[2].add(new Point(-100, 200));
		paths[2].setColor(Color.argb(0, 120, 120, 20));
		paths[2].setWidth(10);
		paths[3].add(new Point(800, 300));
		paths[3].add(new Point(572, 300));
		paths[3].add(new Point(372, 300));
		paths[3].add(new Point(-10, 300));
		paths[3].add(new Point(-100, 300));
		paths[3].setColor(Color.argb(0, 120, 120, 20));
		paths[3].setWidth(10);
		Wave wave11_1 = new Wave(20);
		wave11_1.put(10, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(20, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(30, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(40, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(60, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(70, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(80, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(100, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(110, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(120, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(140, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(150, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(160, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(180, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(190, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(200, new Bug(context, paths[3], Bug.ANT, levelID));
		waves.add(wave11_1);
		wave11_1 = new Wave(20);
		wave11_1.put(10, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(20, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(30, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(40, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(60, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(70, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(80, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(100, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(110, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(120, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(140, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(150, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(160, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(180, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(190, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(200, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.increaseHealth(100);
		waves.add(wave11_1);
		wave11_1 = new Wave(20);
		wave11_1.put(10, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(20, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(30, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(40, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(60, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(70, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(80, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(100, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(110, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(120, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(140, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(150, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(160, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(180, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(190, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(200, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.increaseHealth(100);
		waves.add(wave11_1);
		wave11_1 = new Wave(20);
		wave11_1.put(10, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(20, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(30, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(40, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(60, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(70, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(80, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(100, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(110, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(120, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(140, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(150, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(160, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(180, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(190, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(200, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.increaseHealth(100);
		waves.add(wave11_1);
		wave11_1 = new Wave(20);
		wave11_1.put(10, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(20, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(30, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(40, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(50, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(60, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(70, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(80, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(90, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(100, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(110, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(120, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(130, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(140, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(150, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(160, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.put(170, new Bug(context, paths[0], Bug.ANT, levelID));
		wave11_1.put(180, new Bug(context, paths[1], Bug.ANT, levelID));
		wave11_1.put(190, new Bug(context, paths[2], Bug.ANT, levelID));
		wave11_1.put(200, new Bug(context, paths[3], Bug.ANT, levelID));
		wave11_1.increaseHealth(100);
		waves.add(wave11_1);
	}

	private void initializeLevel12(Context context) {

		paths[0].add(new Point(800, 241));
		paths[0].add(new Point(572, 241));
		paths[0].add(new Point(572, 66));
		paths[0].add(new Point(382, 66));
		paths[0].add(new Point(382, 332));
		paths[0].add(new Point(192, 332));
		paths[0].add(new Point(192, 154));
		paths[0].add(new Point(-10, 154));
		paths[0].add(new Point(-100, 154));
		paths[0].setColor(Color.argb(0, 120, 120, 20));

		paths[1].add(new Point(663, 410));
		paths[1].add(new Point(663, 66));
		paths[1].add(new Point(478, 66));
		paths[1].add(new Point(478, 241));
		paths[1].add(new Point(288, 241));
		paths[1].add(new Point(288, 66));
		paths[1].add(new Point(95, 66));
		paths[1].add(new Point(95, 241));
		paths[1].add(new Point(-10, 241));
		paths[1].add(new Point(-100, 241));
		paths[1].setColor(Color.argb(0, 120, 120, 20));

		paths[2].add(new Point(572, -10));
		paths[2].add(new Point(572, 154));
		paths[2].add(new Point(757, 154));
		paths[2].add(new Point(757, 332));
		paths[2].add(new Point(382, 332));
		paths[2].add(new Point(382, 154));
		paths[2].add(new Point(192, 154));
		paths[2].add(new Point(192, 332));
		paths[2].add(new Point(-10, 332));
		paths[2].add(new Point(-100, 332));
		paths[2].setColor(Color.argb(0, 120, 120, 20));

		paths[3].add(new Point(800, 66));
		paths[3].add(new Point(572, 66));
		paths[3].add(new Point(572, 154));
		paths[3].add(new Point(478, 154));
		paths[3].add(new Point(478, 241));
		paths[3].add(new Point(572, 241));
		paths[3].add(new Point(572, 332));
		paths[3].add(new Point(478, 332));
		paths[3].add(new Point(478, 154));
		paths[3].add(new Point(382, 154));
		paths[3].add(new Point(382, 66));
		paths[3].add(new Point(288, 66));
		paths[3].add(new Point(288, 332));
		paths[3].add(new Point(192, 332));
		paths[3].add(new Point(192, 241));
		paths[3].add(new Point(288, 241));
		paths[3].add(new Point(288, 66));
		paths[3].add(new Point(192, 66));
		paths[3].add(new Point(192, 241));
		paths[3].add(new Point(95, 241));
		paths[3].add(new Point(95, 66));
		paths[3].add(new Point(-10, 66));
		paths[3].add(new Point(-100, 66));
		paths[3].setColor(Color.argb(0, 120, 120, 20));

		Wave wave12_1 = new Wave(46);
		Bug b = new Bug(context, paths[0], Bug.ANT, levelID);
		b.incrementHealth(300);
		wave12_1.put(20, b);
		wave12_1.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_1.put(60, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_1.put(80, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_1.put(100, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_1.put(120, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_1.put(140, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_1.put(160, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_1.put(180, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_1.put(200, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_1.put(220, new Bug(context, paths[0], Bug.ANT, levelID));

		wave12_1.put(122, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_1.put(142, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_1.put(162, new Bug(context, paths[1], Bug.RED_ANT, levelID));

		wave12_1.put(41, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_1.put(61, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_1.put(81, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_1.put(101, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_1.put(121, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_1.put(141, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_1.put(161, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_1.put(181, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_1.put(201, new Bug(context, paths[2], Bug.ANT, levelID));

		wave12_1.put(110, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_1.put(111, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_1.put(112, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_1.put(130, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_1.put(131, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_1.put(132, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_1.put(150, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_1.put(151, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_1.put(152, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_1.put(170, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_1.put(171, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_1.put(172, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_1.put(190, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_1.put(191, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_1.put(192, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_1.put(210, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_1.put(211, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_1.put(212, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_1.put(213, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_1.put(214, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_1.put(215, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		Bug superAnt = new Bug(context, paths[3], Bug.RED_ANT, levelID);
		superAnt.incrementHealth(250);
		wave12_1.put(225, superAnt);
		wave12_1.increaseHealth(300);

		//
		Wave wave12_2 = new Wave(56);
		Bug b2 = new Bug(context, paths[0], Bug.ANT, levelID);
		b.incrementHealth(400);
		wave12_2.put(20, b2);
		wave12_2.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_2.put(60, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_2.put(80, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_2.put(100, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_2.put(120, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_2.put(140, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_2.put(160, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_2.put(180, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_2.put(200, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_2.put(220, new Bug(context, paths[0], Bug.ANT, levelID));

		wave12_2.put(122, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_2.put(142, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_2.put(162, new Bug(context, paths[1], Bug.RED_ANT, levelID));

		wave12_2.put(41, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_2.put(61, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_2.put(81, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_2.put(101, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_2.put(121, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_2.put(141, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_2.put(161, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_2.put(181, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_2.put(201, new Bug(context, paths[2], Bug.ANT, levelID));

		wave12_2.put(44, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_2.put(64, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_2.put(84, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_2.put(104, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_2.put(124, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_2.put(144, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_2.put(164, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_2.put(184, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_2.put(204, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_2.put(224, new Bug(context, paths[3], Bug.RED_ANT, levelID));

		wave12_2.put(110, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_2.put(111, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_2.put(112, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_2.put(130, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_2.put(131, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_2.put(132, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_2.put(150, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_2.put(151, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_2.put(152, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_2.put(170, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_2.put(171, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_2.put(172, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_2.put(190, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_2.put(191, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_2.put(192, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_2.put(210, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_2.put(211, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_2.put(212, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_2.put(213, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_2.put(214, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_2.put(215, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		Bug superAnt2 = new Bug(context, paths[3], Bug.RED_ANT, levelID);
		superAnt.incrementHealth(350);
		wave12_2.put(225, superAnt2);
		wave12_2.increaseHealth(400);
		//
		Wave wave12_3 = new Wave(66);
		Bug b3 = new Bug(context, paths[0], Bug.ANT, levelID);
		b.incrementHealth(600);
		wave12_3.put(20, b3);
		wave12_3.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_3.put(60, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_3.put(80, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_3.put(100, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_3.put(120, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_3.put(140, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_3.put(160, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_3.put(180, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_3.put(200, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_3.put(220, new Bug(context, paths[0], Bug.ANT, levelID));

		wave12_3.put(122, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_3.put(142, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_3.put(162, new Bug(context, paths[1], Bug.RED_ANT, levelID));

		wave12_3.put(41, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_3.put(61, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_3.put(81, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_3.put(101, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_3.put(121, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_3.put(141, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_3.put(161, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_3.put(181, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_3.put(201, new Bug(context, paths[2], Bug.ANT, levelID));

		wave12_3.put(44, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(64, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(84, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(104, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(124, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(144, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(164, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(184, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(204, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(224, new Bug(context, paths[3], Bug.RED_ANT, levelID));

		wave12_3.put(59, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(79, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(99, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(119, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(139, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(159, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(179, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(199, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(219, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_3.put(239, new Bug(context, paths[3], Bug.RED_ANT, levelID));

		wave12_3.put(110, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_3.put(111, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_3.put(112, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_3.put(130, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_3.put(131, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_3.put(132, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_3.put(150, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_3.put(151, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_3.put(152, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_3.put(170, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_3.put(171, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_3.put(172, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_3.put(190, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_3.put(191, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_3.put(192, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_3.put(210, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_3.put(211, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_3.put(212, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_3.put(213, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_3.put(214, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_3.put(215, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		Bug superAnt3 = new Bug(context, paths[3], Bug.RED_ANT, levelID);
		superAnt.incrementHealth(450);
		wave12_3.put(225, superAnt3);
		wave12_3.increaseHealth(600);
		//
		Wave wave12_4 = new Wave(76);
		Bug b4 = new Bug(context, paths[0], Bug.ANT, levelID);
		b.incrementHealth(700);
		wave12_4.put(20, b4);
		wave12_4.put(40, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_4.put(60, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_4.put(80, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_4.put(100, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_4.put(120, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_4.put(140, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_4.put(160, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_4.put(180, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_4.put(200, new Bug(context, paths[0], Bug.ANT, levelID));
		wave12_4.put(220, new Bug(context, paths[0], Bug.ANT, levelID));

		wave12_4.put(122, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_4.put(142, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_4.put(162, new Bug(context, paths[1], Bug.RED_ANT, levelID));

		wave12_4.put(41, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_4.put(61, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_4.put(81, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_4.put(101, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_4.put(121, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_4.put(141, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_4.put(161, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_4.put(181, new Bug(context, paths[2], Bug.ANT, levelID));
		wave12_4.put(201, new Bug(context, paths[2], Bug.ANT, levelID));

		wave12_4.put(44, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(64, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(84, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(104, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(124, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(144, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(164, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(184, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(204, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(224, new Bug(context, paths[3], Bug.RED_ANT, levelID));

		wave12_4.put(59, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(79, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(99, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(119, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(139, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(159, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(179, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(199, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(219, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(239, new Bug(context, paths[3], Bug.RED_ANT, levelID));

		wave12_4.put(71, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(91, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(111, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(131, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(151, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(171, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(191, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(211, new Bug(context, paths[3], Bug.RED_ANT, levelID));
		wave12_4.put(231, new Bug(context, paths[3], Bug.RED_ANT, levelID));

		wave12_4.put(110, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_4.put(111, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_4.put(112, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_4.put(130, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_4.put(131, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_4.put(132, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_4.put(150, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_4.put(151, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_4.put(152, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_4.put(170, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_4.put(171, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_4.put(172, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_4.put(190, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_4.put(191, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_4.put(192, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_4.put(210, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_4.put(211, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_4.put(212, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		wave12_4.put(213, new Bug(context, paths[0], Bug.RED_ANT, levelID));
		wave12_4.put(214, new Bug(context, paths[1], Bug.RED_ANT, levelID));
		wave12_4.put(215, new Bug(context, paths[2], Bug.RED_ANT, levelID));

		Bug superAnt4 = new Bug(context, paths[3], Bug.RED_ANT, levelID);
		superAnt.incrementHealth(550);
		wave12_4.put(225, superAnt4);
		wave12_4.increaseHealth(900);
		//
		waves.add(wave12_1);
		waves.add(wave12_2);
		waves.add(wave12_3);
	}
}
