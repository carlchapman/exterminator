package app.exterminator.GUI.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import app.exterminator.R;
import app.exterminator.data.structures.BugInfo;
import app.exterminator.data.utility.BMPS;
import app.exterminator.data.utility.Globals;

/**
 * The Class Bug represents a single bug of a given type. Depending on the bugID
 * passed into the constructor, it will create a bug with the desired constants.
 * Most importantly a bug moves from one point on it's path to the next at a
 * speed that can be changed. It can be damaged and die or it can get past the
 * left of the screen and escape. The animation is accomplished by using a
 * bitmap that typically has 4 rows and multiple columns. Each row corresponds
 * to one of four directions, and each column represents a frame of animation,
 * so that as the bug moves, the current frame being drawn cycles through the
 * columns, giving the illusion of movement for a properly drawn bitmap.
 * 
 * 
 * @author Justin Landsgard, modified by carlchapman
 */
public class Bug {

	// ///////////////////static variables/////////////////////

	// direction = 0 up, 1 left, 2 down, 3 right,
	// animation = 3 back, 1 left, 0 front, 2 right
	/** The DIRECTIO n_ t o_ animatio n_ map. */
	private static final int[] DIRECTION_TO_ANIMATION_MAP = { 3, 1, 0, 2 };

	// these constants correspond to the bugID
	/** The Constant ANT. */
	public static final int ANT = 0;

	/** The Constant ROACH. */
	public static final int ROACH = 1;

	/** The Constant BEE. */
	public static final int BEE = 2;

	/** The Constant CENTIPEDE_BODY. */
	public static final int CENTIPEDE_BODY = 3;

	/** The Constant CENTIPEDE_HEAD. */
	public static final int CENTIPEDE_HEAD = 4;

	/** The Constant RED_ANT. */
	public static final int RED_ANT = 5;

	/** The Constant BEDBUG. */
	public static final int BEDBUG = 6;

	/** The Constant EARWIG. */
	public static final int EARWIG = 7;

	/** The Constant TICK. */
	public static final int TICK = 8;

	/** The Constant BMP_ROWS. */
	private static final int[] BMP_ROWS = { 4, 4, 4, 4, 4, 4, 4, 4, 4 };

	/** The Constant BMP_COLUMNS. */
	private static final int[] BMP_COLUMNS = { 4, 4, 17, 4, 4, 4, 4, 4, 4 };

	/** The Constant MAX_HEALTH. */
	private static final int[] MAX_HEALTH = { 100, 350, 200, 150, 300, 200,
			450, 170, 400 };

	/** The Constant NUMBER_OF_BUGTYPES. */
	public final static int NUMBER_OF_BUGTYPES = 9;

	/** The BUG_BITMAP array. */
	public static Bitmap[] BUG_BITMAP = null;

	/** The Constant BUG_BITMAP_RESOURCE. */
	public static final int[] BUG_BITMAP_RESOURCE = { R.drawable.b_ant,
			R.drawable.b_roach, R.drawable.b_bee, R.drawable.b_centipede_body,
			R.drawable.b_centipede_head, R.drawable.b_red_ant,
			R.drawable.b_bedbug, R.drawable.b_earwig, R.drawable.b_tick };

	/** The bug's bitmap dimensions. */
	// where x is width, y is height
	public static Point[] BUG_BITMAP_DIMENSIONS = null;

	// ///////////////////instance variables/////////////////////

	// object variables
	/** The info that would be used to recreate this bug if it is saved. */
	private BugInfo info;

	/** The path. */
	private BugPath path;

	// dependent variables depend on what type of bug is instantiated
	/** The base speed. */
	private double baseSpeed;

	/** The width. */
	private int width;

	/** The height. */
	private int height;

	/** The coins earned. */
	private int coinsEarned;

	/** The damage. */
	private int damage;

	/** The slowingTowerKey. */
	private int slowerKey;

	/**
	 * The first slot is the amount to reset walking to the second slot is the
	 * amount to reset waiting to the third slot is a counter for how long it
	 * has been walking that is decremented till it reaches zero the fourth slot
	 * is a counter for how long it has been waiting that is decremented till it
	 * reaches zero the fifth slot is a switch between walking and waiting
	 */
	private int[] walkWait;

	/**
	 * Initialize bug bitmaps.
	 * 
	 * @param context
	 *            the context
	 */
	public static void initializeBugBitmaps(Context context) {
		BUG_BITMAP = new Bitmap[NUMBER_OF_BUGTYPES];
		BUG_BITMAP_DIMENSIONS = new Point[NUMBER_OF_BUGTYPES];

		for (int i = 0; i < NUMBER_OF_BUGTYPES; i++) {
			BUG_BITMAP[i] = BitmapFactory.decodeResource(
					context.getResources(), BUG_BITMAP_RESOURCE[i]);
			BUG_BITMAP_DIMENSIONS[i] = new Point(BUG_BITMAP[i].getWidth()
					/ BMP_COLUMNS[i], BUG_BITMAP[i].getHeight() / BMP_ROWS[i]);
		}
	}

	/**
	 * Returns true if all bug bitmaps are setup.
	 * 
	 * @return true if all bug bitmaps are setup.
	 */
	public static boolean areSetup() {
		if (null == BUG_BITMAP || null == BUG_BITMAP_DIMENSIONS)
			return false;
		for (int i = 0; i < NUMBER_OF_BUGTYPES; i++) {
			if (null == BUG_BITMAP[i] || null == BUG_BITMAP_DIMENSIONS[i])
				return false;
		}
		return true;
	}

	/**
	 * Constructs of a new bug of the desired type.
	 * 
	 * @param context
	 *            the context
	 * @param path
	 *            the path
	 * @param bugID
	 *            the bug id
	 * @param levelID
	 *            the level id
	 */
	public Bug(Context context, BugPath path, int bugID, int levelID) {
		this(context, path, new BugInfo(bugID, path.getX(0), path.getY(0), 0,
				1.0, 1.0, MAX_HEALTH[bugID], BMPS.getGen().nextInt(
						Integer.MAX_VALUE)
						% BMP_COLUMNS[bugID], path.getIndex(),
				((Globals) context).getPlayer().getDatabaseAdapter(), levelID));
	}

	/**
	 * Constructs an existing bug (or called by other constructors).
	 * 
	 * @param context
	 *            the context
	 * @param path
	 *            the path
	 * @param info
	 *            the info
	 */
	public Bug(Context context, BugPath path, BugInfo info) {
		this.info = info;
		this.path = path;
		this.slowerKey = -1;
		this.walkWait = new int[5];
		for (int i = 0; i < walkWait.length; i++)
			walkWait[i] = 0;

		switch (info.getBugID()) {
		case ANT:
			this.baseSpeed = 10;
			this.coinsEarned = 10;
			this.damage = 1;
			break;
		case ROACH:
			this.baseSpeed = 8;
			this.coinsEarned = 40;
			this.damage = 3;
			break;
		case BEE:
			this.baseSpeed = 5;
			this.coinsEarned = 20;
			this.damage = 2;
			break;
		case CENTIPEDE_BODY:
			this.baseSpeed = 12;
			this.coinsEarned = 4;
			this.damage = 1;
			break;
		case CENTIPEDE_HEAD:
			this.baseSpeed = 12;
			this.coinsEarned = 45;
			this.damage = 3;
			break;
		case RED_ANT:
			this.baseSpeed = 14;
			this.coinsEarned = 30;
			this.damage = 6;
			break;
		case BEDBUG:
			this.baseSpeed = 15;
			this.coinsEarned = 80;
			this.damage = 12;
			// set up the amount for a bedbug to walk and wait
			walkWait[0] = BMPS.getGen().nextInt(Integer.MAX_VALUE) % 25 + 10;
			walkWait[1] = BMPS.getGen().nextInt(Integer.MAX_VALUE) % 15 + 10;
			resetWalkWait();
			break;
		case EARWIG:
			this.baseSpeed = 5;
			this.coinsEarned = 25;
			this.damage = 2;
			break;
		case TICK:
			this.baseSpeed = 4;
			this.coinsEarned = 100;
			this.damage = 25;
			// set up the amount for a bedbug to walk and wait
			walkWait[0] = BMPS.getGen().nextInt(Integer.MAX_VALUE) % 45 + 25;
			walkWait[1] = BMPS.getGen().nextInt(Integer.MAX_VALUE) % 20 + 10;
			resetWalkWait();
			break;
		default:

			// throw exception
			break;
		}

		this.width = BUG_BITMAP_DIMENSIONS[info.getBugID()].getX();
		this.height = BUG_BITMAP_DIMENSIONS[info.getBugID()].getY();
	}

	/**
	 * On draw, draw the Bitmap into the rectangle.
	 * 
	 * @param canvas
	 *            the given canvas
	 */
	public void onDraw(Canvas canvas) {
		if (!isDead()) {
			// if bug is not a bedbug and not a tick, or it is a walking
			// bedbug/tick, update position
			if ((info.getBugID() != BEDBUG && info.getBugID() != TICK)
					|| walkWait[2] > 0) {
				updatePosition();
				// if it is a waiting bedbug/tick, decrement wait counter
			} else if (walkWait[3] > 0) {
				walkWait[3]--;
				// otherwise both counters are zero, so reset one of them
			} else {
				resetWalkWait();
			}
			int x = info.getX() - width / 2;
			int y = info.getY() - height / 2;
			int srcX = info.getCurrentFrame() * width;
			int srcY = getAnimationRow() * height;
			Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
			Rect dst = new Rect(x, y, x + width, y + height);
			canvas.drawBitmap(BUG_BITMAP[info.getBugID()], src, dst, null);
		}
	}

	/**
	 * Update position.
	 */
	private void updatePosition() {

		// fetch once only for efficiency
		int x = info.getX();
		int y = info.getY();
		double xSpeedScalar = info.getxSpeedScalar();
		double ySpeedScalar = info.getySpeedScalar();
		int nPoints = path.size();
		int pointIndex = info.getNodeIndex();

		// if the bug still has points to visit and has passed a node
		if (pointIndex < nPoints) {
			// if going left and bug's next position will be left of (or on)
			// current point
			if (xSpeedScalar < 0
					&& (x + baseSpeed * xSpeedScalar) <= path.getX(pointIndex))
				nextPoint(pointIndex);

			// if going right and bug's next position will be right of (or on)
			// current point
			else if (xSpeedScalar > 0
					&& (x + baseSpeed * xSpeedScalar) >= path.getX(pointIndex))
				nextPoint(pointIndex);

			// if going down and bug's next position will be below (or on)
			// current point
			else if (ySpeedScalar > 0
					&& (y + baseSpeed * ySpeedScalar) >= path.getY(pointIndex)) // up
				nextPoint(pointIndex);

			// if going up and bug's next position will be above (or on) current
			// point
			else if (ySpeedScalar < 0
					&& (y + baseSpeed * ySpeedScalar) <= path.getY(pointIndex)) // down
				nextPoint(pointIndex);

			// // in any case, always advance bug to a new position
			// System.out.println("path x: " + path.getX(pointIndex) +
			// " path y:"
			// + path.getY(pointIndex));
			// System.out.println("current x: " + x + " plus baseSpeed: "
			// + baseSpeed + " times xSpeedScalar: " + xSpeedScalar
			// + "equals: " + (x + baseSpeed * xSpeedScalar));
			// System.out.println("current y: " + y + " plus baseSpeed: "
			// + baseSpeed + " times ySpeedScalar: " + ySpeedScalar
			// + "equals: " + (y + baseSpeed * ySpeedScalar));

			// if going left
			if (xSpeedScalar < 0) {
				info.setX((int) (x + baseSpeed * xSpeedScalar));
				info.setY(path.getY(pointIndex));
				// if going right
			} else if (xSpeedScalar > 0) {
				info.setX((int) (x + baseSpeed * xSpeedScalar));
				info.setY(path.getY(pointIndex));
				// if going down
			} else if (ySpeedScalar > 0) {
				info.setX(path.getX(pointIndex));
				info.setY((int) (y + baseSpeed * ySpeedScalar));
				// if going up
			} else if (ySpeedScalar < 0) {
				info.setX(path.getX(pointIndex));
				info.setY((int) (y + baseSpeed * ySpeedScalar));
			}

			// advance the column of the bitmap frame
			info.incrementCurrentBitmapFrame(BMP_COLUMNS[info.getBugID()]);
			if (info.getBugID() == BEDBUG || info.getBugID() == TICK)
				walkWait[2]--;
		} else {
			System.err
					.println("Bug: updatePosition()...bug is trying to access pointIndex: "
							+ pointIndex + " from a path of size: " + nPoints);
		}
	}

	/**
	 * Increment next point and change direction.
	 * 
	 * @param pointIndex
	 *            the point index
	 * @return
	 */
	private boolean nextPoint(int pointIndex) {
		Point oldPt = path.get(pointIndex);
		Point curPoint = path.get(pointIndex + 1);
		// System.out.println();
		// System.out.println("************");
		// Log.w("xSpeedScalar, ySpeedScalar", info.getxSpeedScalar() + " , "
		// + info.getySpeedScalar());
		// Log.w("x, y", info.getX() + " , " + info.getY());
		// Log.w("point index", "Cur at: " + pointIndex);
		// Log.w("last point", oldPt.getX() + " , " + oldPt.getY());
		// Log.w("next point", curPoint.getX() + " , " + curPoint.getY());

		if (oldPt.getX() > curPoint.getX()) {
			info.go(BugInfo.LEFT);
		}
		if (oldPt.getX() < curPoint.getX()) {
			info.go(BugInfo.RIGHT);
		}
		if (oldPt.getY() > curPoint.getY()) {
			info.go(BugInfo.UP);
		}
		if (oldPt.getY() < curPoint.getY()) {
			info.go(BugInfo.DOWN);
		}
		info.setNodeIndex(++pointIndex);
		return true;
	}

	/**
	 * Gets the animation row.
	 * 
	 * @return the animation row
	 */
	private int getAnimationRow() {
		double xSpeed = baseSpeed * info.getxSpeedScalar();
		double ySpeed = baseSpeed * info.getySpeedScalar();
		double dirDouble = (Math.atan2(xSpeed, ySpeed) / (Math.PI / 2) + 2);
		int direction = (int) Math.round(dirDouble) % BMP_ROWS[info.getBugID()];
		return DIRECTION_TO_ANIMATION_MAP[direction];
	}

	/**
	 * Coins earned.
	 * 
	 * @return the number of coins earned for killing this bug.
	 */
	public int coinsEarned() {
		return coinsEarned;
	}

	/**
	 * Slow speed scalar of this bug by some given amount.
	 * 
	 * @param slowBy
	 *            the slow by amount
	 */
	public void slowSpeed(double slowBy) {
		info.slowSpeed(slowBy);
	}

	/**
	 * Resume normal speed.
	 */
	public void resumeSpeed() {
		info.resumeSpeed();
	}

	private void resetWalkWait() {
		if (walkWait[4] == 1) {
			walkWait[3] = walkWait[1];
			walkWait[4] = 0;
		} else if (walkWait[4] == 0) {
			walkWait[2] = walkWait[0];
			walkWait[4] = 1;
		}
	}

	/**
	 * Reduce bug's health by some given damage.
	 * 
	 * @param damage
	 *            the damage
	 */
	public void hit(int damage) {
		info.decrementHealth(damage);
	}

	public void incrementHealth(int amount) {
		info.incrementHealth(amount);
	}

	/**
	 * Gets the damage caused to the player by this bug.
	 * 
	 * @return the damage
	 */
	public int getDamage() {
		return damage;
	}

	/**
	 * Checks if bug is dead.
	 * 
	 * @return true, if is dead
	 */
	public boolean isDead() {
		return info.getHealth() <= 0;
	}

	/**
	 * Gets the slower key.
	 * 
	 * @return the slower key
	 */
	public int getSlowerKey() {
		return slowerKey;
	}

	/**
	 * Sets the slower key.
	 * 
	 * @param newSlowerKey
	 *            the new slower key
	 */
	public void setSlowerKey(int newSlowerKey) {
		this.slowerKey = newSlowerKey;
	}

	/**
	 * Checks if bug has escaped.
	 * 
	 * @return true, if bug has escaped
	 */
	public boolean hasEscaped() {
		return info.getX() + width <= 0;
	}

	/**
	 * Gets the x location.
	 * 
	 * @return the x location
	 */
	public int getX() {
		return info.getX();
	}

	/**
	 * Gets the y location.
	 * 
	 * @return the y location
	 */
	public int getY() {
		return info.getY();
	}

	/**
	 * Checks if this bug is slowed.
	 * 
	 * @return true, if this bug is slowed
	 */
	public boolean isSlowed() {
		return info.isSlowed();
	}

	/**
	 * Gets the a copy of the BugInfo.
	 * 
	 * @return the info copy
	 */
	public BugInfo getInfoCopy() {
		return info.deepCopy();
	}
}
