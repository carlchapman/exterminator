package app.exterminator.GUI.game;

import java.util.concurrent.CopyOnWriteArrayList;

import android.util.SparseArray;

/**
 * The Class Wave can be visualized as a number line that has bugs at certain
 * points on the line. The end of the line is the last bug, but the spacing in
 * between the bugs is what is important.
 * 
 * Every bug can be fetched from this data structure using the time that it
 * should appear, thus as waveTime is advanced one unit at a time, bugs are
 * gotten in the order and with the spacing that they were put into the Wave
 * with.
 * 
 * @author carlchapman
 */
public class Wave extends SparseArray<Bug> {

	/** The wave time. */
	private int waveTime;

	/** The end time. */
	private int endTime;

	/**
	 * Instantiates a new wave.
	 */
	public Wave() {
		this(0);
	}

	/**
	 * Instantiates a new wave with a specific capacity - this is the best thing
	 * to use if it is known how many bugs will be put into a wave, because then
	 * the wave will never have to resize itself.
	 * 
	 * @param initialCapacity
	 *            the initial capacity
	 */
	public Wave(int initialCapacity) {
		super(initialCapacity);
		waveTime = 0;
		endTime = -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.util.SparseArray#put(int, java.lang.Object)
	 */
	@Override
	/**
	 * this adjusts the endTime to be the highest time value of all bugs that are put into the wave.
	 */
	public void put(int key, Bug value) {
		if (endTime < key)
			endTime = key;
		super.put(key, value);
	}

	/**
	 * Advance time - gets the bug at the current waveTime, if any and then
	 * increments time. If any bug exists at that time, it is added to the array
	 * that is passed in.
	 * 
	 * @param bugs
	 *            the bugs
	 */
	public void advanceTime(CopyOnWriteArrayList<Bug> bugs) {
		Bug nextBug = get(waveTime++);
		if (nextBug != null) {
			bugs.add(nextBug);
			// System.out.println("added a bug at time: "+waveTime +
			// "at location...x:"+nextBug.getX()+", y:"+nextBug.getY());
		}
	}

	/**
	 * Sets the wave time, in case it is necessary to start in the middle due to
	 * saving.
	 * 
	 * @param startTime
	 *            the new wave time
	 */
	public void setWaveTime(int startTime) {
		waveTime = startTime;
	}

	/**
	 * Gets the wave time - used when saving.
	 * 
	 * @return the wave time
	 */
	public int getWaveTime() {
		return waveTime;
	}

	/**
	 * Checks if this wave is empty.
	 * 
	 * @return true, if this wave is is empty
	 */
	public boolean isEmpty() {
		return waveTime > endTime;
	}

	/**
	 * Adds another wave to the end of this wave, by adding the oldEndTime to
	 * every key of every other bug in the other wave.
	 * 
	 * @param other
	 *            the other Wave
	 */
	public void addAll(Wave other) {
		int oldEndTime = endTime;
		int otherSize = other.size();
		for (int i = 0; i < otherSize; i++)
			put(oldEndTime + other.keyAt(i), other.valueAt(i));
	}

	public void increaseHealth(int amount) {
		for (int i = 0; i < size(); i++) {
			Bug b = valueAt(i);
			if (b != null)
				valueAt(i).incrementHealth(amount);
		}
	}
}
