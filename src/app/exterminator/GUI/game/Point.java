package app.exterminator.GUI.game;

/**
 * The Class Point represents an x and y pair of ints.
 * 
 * @author Justin Landsgard
 */
public class Point {

	/** The x value. */
	private final int x;
	
	/** The y value. */
	private final int y;

	/**
	 * Instantiates a new point.
	 *
	 * @param x the x value
	 * @param y the y value
	 */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;

	}

	/**
	 * Gets the x value.
	 *
	 * @return the x value
	 */
	public int getX() {
		return x;
	}

	/**
	 * Gets the y value.
	 *
	 * @return the y value
	 */
	public int getY() {
		return y;
	}

}
