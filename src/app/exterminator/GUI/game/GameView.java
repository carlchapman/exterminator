package app.exterminator.GUI.game;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import app.exterminator.R;
import app.exterminator.GUI.control.LevelScreen;
import app.exterminator.GUI.control.LevelSelectScreen;
import app.exterminator.data.facades.Player;
import app.exterminator.data.structures.BugInfo;
import app.exterminator.data.structures.MapInfo;
import app.exterminator.data.structures.StarData;
import app.exterminator.data.structures.TowerInfo;
import app.exterminator.data.structures.UnlockData;
import app.exterminator.data.utility.BMPS;
import app.exterminator.data.utility.Globals;

/**
 * The Class GameView is where the map is drawn and decisions are made about the
 * next action to take based on the health and locations of the bugs and the
 * player's health within the current level.
 * 
 * The GameThread inner class is very closely tied to the game view, in that it
 * provides the timing of the drawings and decisions about what to do next.
 * 
 * @author Justin Landsgard, modified by carlchapman
 */
public class GameView extends SurfaceView {

	/** The map. */
	private Map map;

	/** The towers. */
	private CopyOnWriteArrayList<Tower> towers;

	/** The bugs. */
	private CopyOnWriteArrayList<Bug> bugs;

	/** The game thread. */
	private GameThread gameThread;

	/** The parent. */
	private Activity parent;

	// for pausing
	/** The last click. */
	private long lastClick;

	/** The is paused. */
	private boolean isPaused;

	// speed variables
	/** The ticks ps. */
	private long ticksPS;

	/** The FPS. */
	final long[] FPS = { 8, 12, 16 };

	/** The Constant SLOW. */
	static final int SLOW = 0;

	/** The Constant MEDIUM. */
	static final int MEDIUM = 1;

	/** The Constant FAST. */
	static final int FAST = 2;

	/** The animation speed. */
	private int animationSpeed;

	private UpgradeTowerDialog upgradeDialog;

	/**
	 * Instantiates a new game view.
	 * 
	 * @param context
	 *            the context
	 * @param map
	 *            the map
	 */
	public GameView(Context context, Map map) {
		super(context);
		this.parent = (Activity) context;
		this.map = map;
		this.towers = new CopyOnWriteArrayList<Tower>();
		this.bugs = new CopyOnWriteArrayList<Bug>();
		this.gameThread = new GameThread();
		this.lastClick = System.currentTimeMillis();
		this.animationSpeed = MEDIUM;
		this.isPaused = false;
		getHolder().addCallback(new CustomCallback());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		// draw background and path
		map.onDraw(canvas);
		
		// draw and activate all towers
		for (Tower tower : towers)
			tower.onDraw(canvas, bugs);


		// eliminate dead or escaped bugs awarding coins and draw live bugs
		ArrayList<Bug> toRemove = new ArrayList<Bug>();
		for (Bug bug : bugs) {
			if (bug.isDead()) {
				map.getInfo().addCoins(bug.coinsEarned());
				toRemove.add(bug);
			} else if (bug.hasEscaped()) {
				toRemove.add(bug);
				map.getInfo().reducePlayerHealth(bug.getDamage());
			} else
				bug.onDraw(canvas);
		}
		bugs.removeAll(toRemove);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View#onTouchEvent(android.view.MotionEvent)
	 */
	public boolean onTouchEvent(MotionEvent event) {

		if (System.currentTimeMillis() - lastClick > 300) {
			lastClick = System.currentTimeMillis();
			float x = event.getX();
			float y = event.getY();
			synchronized (getHolder()) {
				if (map.insidePauseButton(x, y) && !isPaused) {
					isPaused = true;

					// custom pauseDialog
					PauseDialog pauseDialog = new PauseDialog(getContext(),
							"PAUSED");
					pauseDialog.show();
				} else if (map.insideSpeedButton(x, y)) {
					gameThread.changeSpeed();
				}

			}

			if (isInRangeOfTower((int) x, (int) y)) {
				final Tower up = getTowerAtPoint((int) x, (int) y);
				if (upgradeDialog != null && upgradeDialog.isShowing()) {
					// do nothing
				} else {
					upgradeDialog = new UpgradeTowerDialog(getContext(), up);
					upgradeDialog.show();
				}
			}

		}
		return true;
	}

	// ///////////////////////End of level Methods///////////////////////

	/**
	 * Launch the ending dialog, which may be you win or you lose depending on
	 * your health. Also this is where stars are earned and towers are unlocked.
	 * 
	 * @param title
	 *            the title
	 */
	public void launchEndingDialog(String title) {
		int levelID = map.getInfo().getLevelID();
		int[] stars = ((Globals) this.getContext().getApplicationContext())
				.getPlayer().getStars(levelID);
		double possibleHealth = MapInfo.START_HEALTH[levelID];
		double healthRemaining = map.getInfo().getPlayerHealth();
		double percentHealth = healthRemaining / possibleHealth;
		Player player = ((Globals) this.getContext().getApplicationContext())
				.getPlayer();
		// System.out.println("percentHealth: "+percentHealth +
		// "healthRemaining: "+healthRemaining+" possibleHealth: "+possibleHealth);
		boolean showUnlockMessage = false;
		if (percentHealth > 0.98 && stars[2] == 0)
			player.updateStar(levelID, StarData.RIGHT, 1);
		if (percentHealth > 0.90 && stars[1] == 0)
			player.updateStar(levelID, StarData.CENTER, 1);
		if (percentHealth > 0.65 && stars[0] == 0)
			player.updateStar(levelID, StarData.LEFT, 1);
		if (healthRemaining > 0) {
			showUnlockMessage = tryUnlockingTowers(player, levelID, title);
		}
		if (!showUnlockMessage) {
			ExitRestartDialog dialog = new ExitRestartDialog(getContext(),
					title);
			dialog.show();
		}

	}

	/**
	 * Try unlocking towers - depending on what level this is, and if other
	 * towers have previously been unlocked, new towers might be unlocked..
	 * 
	 * @param player
	 *            the player
	 * @param levelID
	 *            the level id
	 */
	private boolean tryUnlockingTowers(Player player, int levelID, String title) {
		switch (levelID) {
		case 3:
//			System.out
//					.println("survived level three.  player tower 0 is unlocked: "
//							+ player.towerIsUnlocked(0));
			if (!player.towerIsUnlocked(Tower.BASIC)) {
				launchDialog("You have unlocked the orange poison tower!!!",
						title);
				player.unlockTower(Tower.BASIC);
				return true;
			}
			break;
		case 4:
			if (!player.towerIsUnlocked(Tower.SLOW)) {
				launchDialog("You have unlocked the very sticky tower!!!",
						title);
				player.unlockTower(Tower.SLOW);
				return true;
			}
			break;
		case 5:
			if (!player.towerIsUnlocked(Tower.RELOAD)) {
				launchDialog("You have unlocked the blue zapper tower!!!",
						title);
				player.unlockTower(Tower.RELOAD);
				return true;
			}
			break;
		case 6:
			if (player.towerIsUnlocked(Tower.BASIC)) {
				if (!player.towerIsMaxUpped(Tower.BASIC)) {
					launchDialog(
							"You have unlocked the green poison tower!!!",
							title);
					player.maxUpTower(Tower.BASIC);
					return true;
				}
			} else {
				launchDialog(
						"Survive the honeycomb level to get the orange poison tower, then this level will unlock the green poison tower.",
						title);
				return true;
			}
			break;
		case 7:
			if (player.towerIsUnlocked(Tower.SLOW)) {
				if (!player.towerIsMaxUpped(Tower.SLOW)) {
					launchDialog("You have unlocked the super sticky tower!!!",
							title);
					player.maxUpTower(Tower.SLOW);
					return true;
				}

			} else {
				launchDialog(
						"Survive the pink silk level to get the very sticky tower, then this level will unlock the very sticky tower.",
						title);
				return true;
			}
			break;
		case 8:
			if (player.towerIsUnlocked(Tower.RELOAD)) {
				if (!player.towerIsMaxUpped(Tower.RELOAD)) {
					launchDialog(
							"You have unlocked the ultraviolet zapper tower!!!",
							title);
					player.maxUpTower(Tower.RELOAD);
					return true;
				}

			} else {
				launchDialog(
						"Survive the purple plastic level to get the blue zapper tower, then this level will unlock the ultraviolet tower.",
						title);
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * Launch dialog - a helper to the unlock towers method, this launches a
	 * dialog for the player to see..
	 * 
	 * @param message
	 *            the message
	 */
	private void launchDialog(String message, String title) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
		builder.setMessage(message)
				// "Survive the orange level to get the blue zapper tower, then this level will unlock another tower."
				.setCancelable(false)
				.setPositiveButton("OK", new LaunchExitRestartOnClick(title));
		AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * End thread by trying to join it.
	 */
	public void endThread() {
		boolean retry = true;
		gameThread.setRunning(false);
		while (retry) {
			try {
				System.out.println("trying to join thread");
				gameThread.join();
				System.out.println("thread joined");
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}

	// ///////////////////////Tower Buy/Sell Methods////////////////////

	/**
	 * Adds the tower.
	 * 
	 * @param tower
	 *            the tower
	 */
	public void addTower(Tower tower) {
		this.towers.add(tower);
	}

	/**
	 * Checks if a touch at x and y is in range of tower.
	 * 
	 * @param x
	 *            the x location
	 * @param y
	 *            the y location
	 * @return true, if is in range of tower
	 */
	public boolean isInRangeOfTower(int x, int y) {
		for (Tower tower : towers) {
			if (x >= tower.getInfo().getX() - (tower.getRange() + 10)
					&& x < tower.getInfo().getX() + (tower.getRange() + 10)) {
				if (y >= tower.getInfo().getY() - (tower.getRange() + 10)
						&& y <= tower.getInfo().getY()
								+ (tower.getRange() + 10)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Gets the tower at point x,y.
	 * 
	 * @param x
	 *            the x location
	 * @param y
	 *            the y location
	 * @return the tower at point x,y
	 */
	public Tower getTowerAtPoint(int x, int y) {
		for (Tower tower : towers) {
			if (x >= tower.getInfo().getX() - (tower.getRange())
					&& x < tower.getInfo().getX() + (tower.getRange())) {
				if (y >= tower.getInfo().getY() - (tower.getRange())
						&& y <= tower.getInfo().getY() + (tower.getRange())) {
					return tower;
				}
			}
		}
		return null;
	}

	// //////////////////inner Classes////////////////////////

	/**
	 * The Class PauseDialog is displayed whenever the user presses the pause
	 * button. It allows the user to chose from four options: restart, resume,
	 * exit or save (and exit)
	 * 
	 * @author carlchapman
	 */
	class PauseDialog extends Dialog implements
			android.view.View.OnClickListener {

		/** The restart button. */
		private ImageButton restartButton;

		/** The resume button. */
		private ImageButton resumeButton;

		/** The exit button. */
		private ImageButton exitButton;

		/** The save button. */
		private ImageButton saveButton;

		/**
		 * Instantiates a new pause dialog.
		 * 
		 * @param context
		 *            the context
		 * @param title
		 *            the title
		 */
		public PauseDialog(Context context, String title) {
			super(context);
			setTitle("Paused");
			setContentView(R.layout.pausedialog);

			restartButton = (ImageButton) findViewById(R.id.Button01);
			restartButton.setOnClickListener(this);

			resumeButton = (ImageButton) findViewById(R.id.Button02);
			resumeButton.setOnClickListener(this);

			exitButton = (ImageButton) findViewById(R.id.Button03);
			exitButton.setOnClickListener(this);

			saveButton = (ImageButton) findViewById(R.id.Button04);
			saveButton.setOnClickListener(this);

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			if (v == resumeButton) {
				isPaused = false;
				dismiss();
			} else if (v == restartButton) {
				AlertDialog alertDialog = new AlertDialog.Builder(getContext())
						.create();
				alertDialog.setTitle("Are you sure?");
				alertDialog
						.setMessage("This will erase your progress in this map and start over.");
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								map.reset();
								int levelID = map.getInfo().getLevelID();
								Intent myIntent = new Intent(getContext(),
										LevelScreen.class);
								dismiss();
								myIntent.putExtra("levelID", levelID);
								parent.startActivityForResult(myIntent, 0);
							}
						});
				alertDialog.setButton2("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
				alertDialog.show();
			} else if (v == exitButton) {
				map.getInfo().reset();
				dismiss();
				Intent myIntent = new Intent(getContext(),
						LevelSelectScreen.class);
				parent.startActivityForResult(myIntent, 0);

			} else if (v == saveButton) {
				map.save(bugs, towers);
				dismiss();
				Intent myIntent = new Intent(getContext(),
						LevelSelectScreen.class);
				parent.startActivityForResult(myIntent, 0);
			}
		}

	}

	/**
	 * The Class ExitRestartDialog is a dialog that allows the user to pick
	 * between two options: exit or restart.
	 * 
	 * @author carlchapman
	 */
	class ExitRestartDialog extends Dialog implements
			android.view.View.OnClickListener {

		/** The restart button. */
		private ImageButton restartButton;

		/** The exit button. */
		private ImageButton exitButton;

		/**
		 * Instantiates a new exit restart dialog.
		 * 
		 * @param context
		 *            the context
		 * @param title
		 *            the title
		 */
		public ExitRestartDialog(Context context, String title) {
			super(context);
			setTitle(title);
			setContentView(R.layout.exitrestart);

			exitButton = (ImageButton) findViewById(R.id.Button01);
			exitButton.setOnClickListener(this);

			restartButton = (ImageButton) findViewById(R.id.Button02);
			restartButton.setOnClickListener(this);
			if (upgradeDialog != null && upgradeDialog.isShowing()) {
				upgradeDialog.dismiss();
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			if (v == restartButton) {
				map.reset();
				int levelID = map.getInfo().getLevelID();
				Intent myIntent = new Intent(getContext(), LevelScreen.class);
				myIntent.putExtra("levelID", levelID);
				parent.startActivityForResult(myIntent, 0);
				dismiss();
			} else if (v == exitButton) {
				map.reset();
				Intent myIntent = new Intent(getContext(),
						LevelSelectScreen.class);
				parent.startActivityForResult(myIntent, 0);
				dismiss();
			}
		}

	}

	class UpgradeTowerDialog extends Dialog implements
			android.view.View.OnClickListener {

		/** The resume button. */
		private ImageButton resumeButton;

		/** The sell button. */
		private ImageButton sellButton;

		/** The upgrade button. */
		private ImageButton upgradeButton;

		/** The maxUp button. */
		private ImageButton maxUpButton;

		/** The tower to adjust */
		private Tower tower;

		public UpgradeTowerDialog(Context context, Tower tower) {
			super(context);
			this.tower = tower;
			setTitle("Sell/Upgrade");
			setContentView(R.layout.sellupgrade);

			sellButton = (ImageButton) findViewById(R.id.Button01);
			sellButton.setOnClickListener(this);

			resumeButton = (ImageButton) findViewById(R.id.Button02);
			resumeButton.setOnClickListener(this);

			upgradeButton = (ImageButton) findViewById(R.id.Button03);
			if (map.getPlayer().towerIsUnlocked(tower.getInfo().getTowerID())) {
				upgradeButton.setOnClickListener(this);
				//System.out.println("player can unlock");
			} else {
				upgradeButton.setImageBitmap(BMPS.getIcon(BMPS.UP_GREY));
			}

			maxUpButton = (ImageButton) findViewById(R.id.Button04);
			if (map.getPlayer().towerIsMaxUpped(tower.getInfo().getTowerID())) {
				maxUpButton.setOnClickListener(this);
				//System.out.println("player can maxup");
			} else {
				maxUpButton.setImageBitmap(BMPS.getIcon(BMPS.UPUP_GREY));
			}

			isPaused = true;
		}

		@Override
		public void onClick(View v) {
			if (v == sellButton) {
				isPaused = false;
				towers.remove(tower);
				map.getInfo().addCoins(tower.getCost());
				String text = "+" + tower.getCost() + " coins";
				Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
				dismiss();
			} else if (v == resumeButton) {
				isPaused = false;
				dismiss();
			} else if (v == upgradeButton) {
				// the below case should never be possible because the
				// listener was never added...
				if (!map.getPlayer().towerIsUnlocked(
						tower.getInfo().getTowerID())) {
					String text = "upgrade is locked";
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
							.show();
				} else if (map.getInfo().getCoins() < tower.getUpgradeCost()) {
					String text = "you need " + tower.getUpgradeCost()
							+ " coins to upgrade";
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
							.show();
				} else if(tower.getInfo().getLevel()==UnlockData.UPGRADED){
					String text = "this tower is already upgraded";
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
							.show();
				}else{
					map.getInfo().subtractCoins(tower.getUpgradeCost());
					tower.upgrade();
					String text = "-" + tower.getUpgradeCost() + " coins";
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
							.show();
				}
				isPaused = false;
				dismiss();
			} else if (v == maxUpButton) {
				if (tower.canUpgrade()) {
					String text = "tower must be upgraded first";
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
							.show();
					// the below case should never be possible because the
					// listener was never added...
				} else if (!map.getPlayer().towerIsMaxUpped(
						tower.getInfo().getTowerID())) {
					String text = "max-up is locked";
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
							.show();
				} else if (map.getPlayer().getBugBucks() < tower.getMaxUpCost()) {
					String text = "you need " + tower.getMaxUpCost()
							+ " Bugbuckx to max-up";
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
							.show();
				} else if(tower.getInfo().getLevel()==UnlockData.MAXUPPED){
					String text = "this tower is already max-upped";
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
							.show();
				}else{
					map.getPlayer().subtractBugBucks(tower.getMaxUpCost());
					tower.maxUp();
					String text = "-" + tower.getMaxUpCost() + " Bugbuckx";
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
							.show();
				}
				isPaused = false;
				dismiss();

			}
		}
	}

	/**
	 * The Class GameThread essentially controls what is drawn on the screen by
	 * switching through different actions depending on the state of the current
	 * wave, the player's health and the array of bugs.
	 * 
	 * Users can also change the speed of the animation, which depends on how
	 * long the thread waits between calling the onDraw() in the view.
	 * 
	 * @author Justin Landsgard, modified by carlchapman
	 */
	class GameThread extends Thread {

		/** The Constant MESSAGE_DURRATION. */
		static final long MESSAGE_DURRATION = 25;

		/** The message time. */
		private long messageTime;

		/** The message x. */
		private int messageX;

		// actions
		/** The Constant WAVE_RUN. */
		static final int WAVE_RUN = 0;

		/** The Constant WAVE_OVER. */
		static final int WAVE_OVER = 1;

		/** The Constant LOSE_LEVEL. */
		static final int LOSE_LEVEL = 2;

		/** The Constant WIN_LEVEL. */
		static final int WIN_LEVEL = 3;

		/** The action. */
		private int action;

		// object variables
		/** The wave. */
		private Wave wave;

		/** The running. */
		private boolean running;

		/** The text paint. */
		private Paint textPaint;

		/**
		 * Instantiates a new game thread.
		 */
		public GameThread() {
			this.wave = map.getWaveToRun();
			this.running = false;
			this.action = WAVE_RUN;
			this.messageTime = 0;
			resetMessageX();
			ticksPS = 1000 / FPS[animationSpeed];

			textPaint = new Paint();
			textPaint.setTextSize(90);
			textPaint.setColor(Color.WHITE);
		}

		/**
		 * Sets the running boolean.
		 * 
		 * @param run
		 *            the new running
		 */
		public void setRunning(boolean run) {
			running = run;
		}

		/**
		 * Change speed.
		 */
		public void changeSpeed() {
			animationSpeed = (animationSpeed + 1) % FPS.length;
			map.setSpeedIcon(animationSpeed);
			ticksPS = 1000 / FPS[animationSpeed];
			//System.out.println("speed set to: " + animationSpeed);
		}

		/**
		 * Checks for completed.
		 * 
		 * @return true, if level completed
		 */
		public boolean hasCompleted() {
			return bugs.size() == 0 && wave.isEmpty();
		}

		/**
		 * Reset message x.
		 */
		public void resetMessageX() {
			this.messageX = 180;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			long startTime;
			long sleepTime;
			Canvas c = null;
			while (running) {
				if (isPaused) {
					try {
						sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					c = null;
					startTime = System.currentTimeMillis();
					try {
						c = getHolder().lockCanvas();
						synchronized (getHolder()) {
							if (map.getInfo().isDead()){
								this.action = LOSE_LEVEL;
							}
							switch (action) {
							case WAVE_RUN:
								onDraw(c);

								// if there are more bugs, make them appear
								if (!wave.isEmpty()) {
									wave.advanceTime(bugs);

									// if there are no more bugs and no bugs on
									// screen...
								} else if (bugs.isEmpty()) {

									// the wave is over, so increment the
									// current wave
									map.getInfo().incrementCurrentWave();

									// if the map has more waves, show the wave
									// over message

									if (map.hasMoveWaves()){
										action = WAVE_OVER;

									// if there are no more waves, the level is
									// complete
									}else{
										action = WIN_LEVEL;
									}
								}

								break;
							case WAVE_OVER:
								// print the message for messageTime
								if (messageTime++ < MESSAGE_DURRATION) {
									map.onDraw(c);
									c.drawText("wave complete", messageX -= 4,
											200, textPaint);
								}

								// prepare to run the next wave
								else {
									wave = map.getWaveToRun();
									messageTime = 0;
									resetMessageX();
									action = WAVE_RUN;
								}
								break;
							case LOSE_LEVEL:
								map.onDraw(c);
								setRunning(false);
								parent.runOnUiThread(new Runnable() {
									public void run() {
										launchEndingDialog("......YOU DIED :(");
									}
								});

								break;
							case WIN_LEVEL:
								map.onDraw(c);
								setRunning(false);
								parent.runOnUiThread(new Runnable() {
									public void run() {
										launchEndingDialog("YOU WON!!!!!!!");
									}
								});
								break;
							}
						}

					} finally {
						if (c != null) {
							getHolder().unlockCanvasAndPost(c);
						}
					}
					// here the thread finds out how much time it took to draw
					// everything and then sleeps for an additional amount to
					// make a consistent spacing of canvas postings in time
					long elapsedTime = System.currentTimeMillis() - startTime;
					sleepTime = ticksPS - (elapsedTime);
					try {
						if (sleepTime > 0)
							sleep(sleepTime);
						else
							sleep(10);
					} catch (Exception e) {
					}
				}
			}
		}
	}

	/**
	 * The Class CustomWaveCallback is a class that helps decide what to do when
	 * the surface is destroyed or created.
	 */
	class CustomCallback implements SurfaceHolder.Callback {

		/**
		 * Instantiates a new custom wave callback.
		 * 
		 * @author Justin Landsgard, modified by carlchapman
		 */
		public CustomCallback() {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.SurfaceHolder.Callback#surfaceChanged(android.view.
		 * SurfaceHolder, int, int, int)
		 */
		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.SurfaceHolder.Callback#surfaceCreated(android.view.
		 * SurfaceHolder)
		 */
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			if (map.getInfo().wasSaved()) {
				ArrayList<BugInfo> bugInfo = map.getInfo().getBugInfo();
				for (BugInfo b : bugInfo)
					bugs.add(new Bug(getContext(), map.getPath(b.getPathIndex()), b));
				ArrayList<TowerInfo> towerInfo = map.getInfo().getTowerInfo();
				for (TowerInfo t : towerInfo){
					towers.add(new Tower(t));
					//System.out.println("loaded tower "+t.getPrimaryKey());
				}
					
			}
			if (gameThread.getState().equals(Thread.State.TERMINATED)) {
				gameThread = new GameThread();
			}
			gameThread.setRunning(true);
			gameThread.start();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.view.SurfaceHolder.Callback#surfaceDestroyed(android.view
		 * .SurfaceHolder)
		 */
		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			//reset map if it is being destroyed
			if(!map.getInfo().wasSaved())
				map.reset();
			endThread();
		}
	}

	class LaunchExitRestartOnClick implements DialogInterface.OnClickListener {
		private String title;

		public LaunchExitRestartOnClick(String title) {
			this.title = title;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			ExitRestartDialog exitRestartDialog = new ExitRestartDialog(
					getContext(), title);
			exitRestartDialog.show();

		}
	}
}
