package app.exterminator.GUI.game;

import java.util.concurrent.CopyOnWriteArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import app.exterminator.R;
import app.exterminator.data.structures.TowerInfo;
import app.exterminator.data.structures.UnlockData;
import app.exterminator.data.utility.BMPS;

/**
 * The Class Tower represents a tower that can attack or otherwise effect bugs,
 * depending on its towerID.
 * 
 * @author Justin Landsgard, modified by carlchapman
 */
public class Tower {

	// tower identifiers

	/** The Constant BASIC. */
	public static final int BASIC = 0;

	/** The Constant SLOW. */
	public static final int SLOW = 1;

	/** The Constant RELOAD. */
	public static final int RELOAD = 2;

	/** The Constant BASIC1. */
	private static final int BASIC1 = 3;

	/** The Constant SLOW1. */
	private static final int SLOW1 = 4;

	/** The Constant RELOAD1. */
	private static final int RELOAD1 = 5;

	/** The Constant BASIC2. */
	private static final int BASIC2 = 6;

	/** The Constant SLOW2. */
	private static final int SLOW2 = 7;

	/** The Constant RELOAD2. */
	private static final int RELOAD2 = 8;

	// setup constants

	/**
	 * The base cost of each tower (towerID gives index for these arraya)
	 */

	private static final int BASE_COST[] = { 80, 100, 120 };
	/**
	 * The cost in coins to upgrade the tower.
	 */
	private static final int[] UPGRADE_COST = { 40, 50, 60 };
	/**
	 * The cost IN BUGBUCKS to maxup the tower
	 */
	private static final int[] MAXUP_COST = { 4, 6, 8 };

	// the package of data used to reconstruct the tower from saved state

	/** The info. */
	private TowerInfo info;

	// derived Variables

	/** The cost. */
	private int cost;// how much tower costs in coins

	// /** The tower paint. */
	// private Paint towerPaint; // color of circle representing tower

	/** The range. */
	private int range;// range of attack

	/** The slow by. */
	private double slowBy;// amount to slow bug by

	/** The damage. */
	private int damage;// amount bug is damaged

	/** The reload time. */
	private long reloadTime;// time between attacks

	/** The last attack time. */
	private long lastAttackTime;// time of last attack

	// for drawing towers
	
	private Paint towerPaint;

	/** The bitmap */
	@SuppressWarnings("unused")
	private Bitmap bmp;
	
	private int width;
	
	private int height;
	
	/** The current bitmap frame - used by the animation. */
	private int currentBitmapFrame;

	/** The Constant BMP_COLUMNS. */
	private static final int[] BMP_COLUMNS = { 5,5,5,5,5,5,5,5,5 };

	/** The Constant TOWER_BITMAP_RESOURCE. */
	public static final int[] TOWER_BITMAP_RESOURCE = { R.drawable.t_gas_yellow,
			R.drawable.t_sticky_yellow, R.drawable.t_zapper_white,
			R.drawable.t_gas_orange, R.drawable.t_sticky_orange,
			R.drawable.t_zapper_blue, R.drawable.t_gas_green,
			R.drawable.t_sticky_green, R.drawable.t_zapper_purple};

	/** The TOWER_BITMAP array. */
	public static Bitmap[] TOWER_BITMAP = null;
	
	public static Point[] TOWER_BITMAP_DIMENSIONS;
	
	

	/**
	 * Initialize tower bitmaps.
	 * 
	 * @param context
	 *            the context
	 */
	public static void initializeTowerBitmaps(Context context) {
		int nTowers = TOWER_BITMAP_RESOURCE.length;
		TOWER_BITMAP = new Bitmap[nTowers];
		TOWER_BITMAP_DIMENSIONS= new Point[nTowers];
		for (int i = 0; i < nTowers; i++) {
			TOWER_BITMAP[i] = BitmapFactory.decodeResource(
					context.getResources(), TOWER_BITMAP_RESOURCE[i]);
			TOWER_BITMAP_DIMENSIONS[i] = new Point(TOWER_BITMAP[i].getWidth()
					/ BMP_COLUMNS[i], TOWER_BITMAP[i].getHeight());
		}
	}

	/**
	 * Instantiates a new tower - this uses the hashcode of a tower to determine
	 * if it is upgraded or not and then assigns values for it based on its type
	 * and upgrade level.
	 * 
	 * @param towerInfo
	 *            the tower info
	 */
	public Tower(TowerInfo towerInfo) {
		this.info = towerInfo;
		//System.out.println("tower hashcode: "+this.hashCode());
		// initialize to default values
		this.setCost(BASE_COST[info.getTowerID()]);
		this.damage = 10;
		this.lastAttackTime = System.currentTimeMillis();
		this.range = 40;
		this.reloadTime = 0;
		this.slowBy = 1;
		this.bmp = TOWER_BITMAP[this.hashCode()];
		this.width = TOWER_BITMAP_DIMENSIONS[hashCode()].getX();
		this.height = TOWER_BITMAP_DIMENSIONS[hashCode()].getY();
		this.currentBitmapFrame = BMPS.getGen().nextInt(BMP_COLUMNS[hashCode()]);
		 this.towerPaint = new Paint();
		 this.towerPaint.setColor(Color.YELLOW);
		 this.towerPaint.setAlpha(60);

		// change values depending on towerID and level (by hashcode)
		setupTower(hashCode());
		info.saveToDatabase();
	}

	private void setupTower(int identifier) {
		switch (identifier) {
		case BASIC1:
			this.towerPaint.setColor(Color.argb(60, 255, 131, 0));
			this.towerPaint.setAlpha(60);
			cost = BASE_COST[info.getTowerID()]
					+ UPGRADE_COST[info.getTowerID()];
			this.damage = 20;
			this.range = 50;
			break;
		case BASIC2:
			this.towerPaint.setColor(Color.argb(70, 0, 200, 0));
			this.towerPaint.setAlpha(60);
			cost = BASE_COST[info.getTowerID()]
					+ UPGRADE_COST[info.getTowerID()];
			this.damage = 30;
			this.range = 60;
			break;
		case SLOW:
			this.towerPaint.setColor(Color.YELLOW);
			this.towerPaint.setAlpha(100);
			cost = BASE_COST[info.getTowerID()];
			this.range = 50;
			this.slowBy = 0.5;
			break;
		case SLOW1:
			this.towerPaint.setColor(Color.argb(60, 255, 131, 0));
			this.towerPaint.setAlpha(60);
			cost = BASE_COST[info.getTowerID()]
					+ UPGRADE_COST[info.getTowerID()];
			this.range = 50;
			this.slowBy = 0.25;
			break;
		case SLOW2:
			this.towerPaint.setColor(Color.argb(70, 0, 200, 0));
			this.towerPaint.setAlpha(60);
			cost = BASE_COST[info.getTowerID()]
					+ UPGRADE_COST[info.getTowerID()];
			this.range = 75;
			this.slowBy = 0.25;
			break;
		case RELOAD:
			this.towerPaint.setColor(Color.WHITE);
			this.towerPaint.setAlpha(60);
			cost = BASE_COST[info.getTowerID()];
			this.range = 50;
			this.damage = 100;
			this.reloadTime = 1500;
			break;
		case RELOAD1:
			this.towerPaint.setColor(Color.BLUE);
			this.towerPaint.setAlpha(60);
			cost = BASE_COST[info.getTowerID()]
					+ UPGRADE_COST[info.getTowerID()];
			this.range = 60;
			this.damage = 160;
			this.reloadTime = 1000;
			break;
		case RELOAD2:
			this.towerPaint.setColor(Color.argb(70, 172, 66, 225));
			this.towerPaint.setAlpha(70);
			cost = BASE_COST[info.getTowerID()]
					+ UPGRADE_COST[info.getTowerID()];
			this.range = 70;
			this.damage = 230;
			this.reloadTime = 650;
			break;
		}
	}

	public boolean canUpgrade() {
		return info.getLevel() < UnlockData.UPGRADED;
	}

	public boolean canMaxUp() {
		return info.getLevel() < UnlockData.MAXUPPED && !canUpgrade();
	}

	public void upgrade() {
		this.info.setLevel(UnlockData.UPGRADED);
		setupTower(hashCode());
	}

	public void maxUp() {
		this.info.setLevel(UnlockData.MAXUPPED);
		setupTower(hashCode());
	}

	public int getUpgradeCost() {
			return UPGRADE_COST[info.getTowerID()];
	}

	public int getMaxUpCost() {
		return MAXUP_COST[info.getTowerID()];
	}

	/**
	 * On draw draws towers and attacks or otherwise effects bugs.
	 * 
	 * @param canvas
	 *            the canvas
	 * @param bugs
	 *            the bugs
	 */
	public void onDraw(Canvas canvas, CopyOnWriteArrayList<Bug> bugs) {
		

		//currentBitmapFrame=currentBitmapFrame++%BMP_COLUMNS[hashCode()];
		//System.out.println("BMP_COLUMNS[hashCode()]: "+BMP_COLUMNS[hashCode()]);
		int x = info.getX() - width / 2;
		int y = info.getY() - height / 2;
		switch (info.getTowerID()) {
		case BASIC:
			y-=4;
			attackBugsInRange(bugs);
			currentBitmapFrame++;
			currentBitmapFrame= currentBitmapFrame%BMP_COLUMNS[hashCode()];
			break;
		case SLOW:
			slowBugsInRange(bugs);
			currentBitmapFrame++;
			currentBitmapFrame= currentBitmapFrame%BMP_COLUMNS[hashCode()];
			break;
		case RELOAD:
			y-=10;//this helps center the intuitive center of this tower to be as expected (the image is bottom-heavy)
			if (System.currentTimeMillis() - lastAttackTime >= reloadTime) {
				currentBitmapFrame= BMP_COLUMNS[hashCode()]-1;
				attackBugsInRange(bugs);
				lastAttackTime = System.currentTimeMillis();
				//System.out.println("if currentBitmapFrame: "+currentBitmapFrame);	
			}else{
				currentBitmapFrame++;
				currentBitmapFrame= currentBitmapFrame%(BMP_COLUMNS[hashCode()]-1);
				//System.out.println("else currentBitmapFrame: "+currentBitmapFrame);
			}
			break;
		default:
			break;
		}

		int srcX = currentBitmapFrame * width;
		int srcY = 0;
		Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
		Rect dst = new Rect(x, y, x + width, y + height);
		canvas.drawCircle(info.getX(), info.getY(), range, towerPaint);
		canvas.drawBitmap(TOWER_BITMAP[hashCode()], src, dst, null);

	}

	/**
	 * Slow bugs in range.
	 * 
	 * @param bugs
	 *            the bugs
	 */
	public void slowBugsInRange(CopyOnWriteArrayList<Bug> bugs) {

		for (Bug bug : bugs) {
			if (bugIsInRange(bug)) {
				if (!bug.isSlowed()) {
					bug.slowSpeed(slowBy);
					bug.setSlowerKey(info.getPrimaryKey());
					System.out.println("slowing");
				}

			} else if (bug.isSlowed()
					&& bug.getSlowerKey() == info.getPrimaryKey()) {
				bug.resumeSpeed();
				System.out.println("resuming speed");
			}
		}
	}

	/**
	 * Attack bugs in range.
	 * 
	 * @param bugs
	 *            the bugs
	 */
	public void attackBugsInRange(CopyOnWriteArrayList<Bug> bugs) {
		for (Bug bug : bugs)
			if (bugIsInRange(bug))
				bug.hit(damage);
	}

	// note that range is a square of 2*range with center x,y
	/**
	 * Bug is in range.
	 * 
	 * @param bug
	 *            the bug
	 * @return true, if successful
	 */
	private boolean bugIsInRange(Bug bug) {
		int x = info.getX();
		int y = info.getY();
		return x - range <= bug.getX() && x + range >= bug.getX()
				&& y - this.range <= bug.getY() && y+range >= bug.getY();
	}

	/**
	 * Gets the TowerInfo.
	 * 
	 * @return the TowerInfo
	 */
	public TowerInfo getInfoCopy() {
		return info.deepCopy();
	}

	public TowerInfo getInfo() {
		return info;
	}

	/**
	 * Gets the cost of the tower.
	 * 
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * Sets the cost.
	 * 
	 * @param cost
	 *            the new cost
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}

	/**
	 * Gets the range.
	 * 
	 * @return the range
	 */
	public int getRange() {
		return range;
	}

	@Override
	public int hashCode() {
		return info.hashCode();
	}

}
