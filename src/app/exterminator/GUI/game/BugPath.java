package app.exterminator.GUI.game;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

/**
 * The Class BugPath represents a series of points that a bug must travel along.
 * 
 * @author Justin Landsgard, modified by carlchapman
 */
@SuppressWarnings("serial")
public class BugPath extends ArrayList<Point> {

	/** The width. */
	private int width;

	/** The color key. */
	private int colorKey;

	/** The index of this path among all the paths. */
	private int index;
	
	/**
	 * Instantiates a new bug path.
	 *
	 * @param index the index
	 */
	public BugPath(int index){
		super();
		this.index=index;
		this.width = 30;
		this.colorKey = Color.GRAY;
	}
	

	/**
	 * Gets the x location.
	 * 
	 * @param index
	 *            the index
	 * @return the x location
	 */
	public int getX(int index) {
		return get(index).getX();
	}

	/**
	 * Gets the y location.
	 * 
	 * @param index
	 *            the index
	 * @return the y location
	 */
	public int getY(int index) {
		return get(index).getY();
	}

	/**
	 * Gets the index of this path among all the paths.
	 * 
	 * @return the index of this path.
	 */
	public int getIndex() {
		return index;
	}
	
	
	/**
	 * Sets the color.
	 *
	 * @param colorKey the new color
	 */
	public void setColor(int colorKey){
		this.colorKey=colorKey;
	}
	
	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(int width){
		this.width=width;
	}

	/**
	 * On draw.
	 * 
	 * @param canvas
	 *            the canvas to draw on
	 */
	public void onDraw(Canvas canvas) {
		if(size()>0){
			Paint paint = new Paint();
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeWidth(width);
			paint.setColor(colorKey);
			Path androidDrawablePath = new Path();
			androidDrawablePath.moveTo(getX(0), getY(0));
			for (int i = 1; i < size(); i++)
				androidDrawablePath.lineTo(getX(i), getY(i));
			canvas.drawPath(androidDrawablePath, paint);
		}
	}
}
