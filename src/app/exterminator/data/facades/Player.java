package app.exterminator.data.facades;

import java.util.ArrayList;
import java.util.Arrays;

import android.database.Cursor;
import app.exterminator.data.structures.BugInfo;
import app.exterminator.data.structures.MapInfo;
import app.exterminator.data.structures.StarData;
import app.exterminator.data.structures.TowerInfo;
import app.exterminator.data.structures.UnlockData;
import app.exterminator.sync.connect.RunnableTask;
import app.exterminator.sync.update.AbstractUpdate;
import app.exterminator.sync.update.BugBuckUpdate;
import app.exterminator.sync.update.LogoutUpdate;
import app.exterminator.sync.update.StarUpdate;
import app.exterminator.sync.update.TowerUpdate;
import app.exterminator.sync.update.UpdateMap;

/**
 * The Class Player is a facade that can be used to put and get data. The data
 * can conceptually be broken into to kinds: player progress and saved data.
 * Player progress concerns things like coins, bugbucks, stars, unlocking of
 * towers, and should be backed up both locally and on the remote server. It can
 * be found as variables here and in the data structures StarData and
 * UnlockData. Saved state pertains to the progress within a level that a player
 * has made. This is only concerns the health, placed towers and bugs, and what
 * the current wave index and time are, and is found only in the MapInfo data
 * structure. It is should be saved locally only when the player presses the
 * 'save' button and should not be kept on the remote server.
 * 
 * Information about the sound settings for this player, if this player object
 * is a guest player or not, their username and authentication token, and a
 * reference to the database adapter are also kept for use within this class and
 * by other classes. The update map keeps track of changes that need to be
 * pushed to the remote server.
 * 
 * This facade and the Database Adapter are available from the application
 * context from any activity or view because the application itself is of type
 * Globals, which provides a way to get and set the player and database adapter
 * for global use.
 * 
 * 
 * @author carlchapman
 */
public class Player {

	//
	/** The DatabaseAdapter to the local database.. */
	private DatabaseAdapter db;

	/** The Update Map. */
	private UpdateMap updateMap;

	/** The number of bug bucks that the player has. */
	private int bugBucks;

	/** The authentication token used to connect to the remote server. */
	private String token;

	/** The player's username. */
	private String username;

	/** A boolean to track if this user is a guest or not. */
	private boolean isGuest;

	/** A boolean to track if this user has set the sound off. */
	private boolean soundOff;

	/** A boolean to track if this user has set the music off. */
	private boolean musicOff;

	/**
	 * Information about each level, including information about what wave the
	 * player is on and where within that wave, as well as the BugInfo and
	 * TowerInfo that tracks the placement of towers and live bugs in a given
	 * level.
	 */
	private MapInfo[] instances;

	/**
	 * Information about the number of stars for each level. Stars are an
	 * earnable currency that can be traded for bugbucks.
	 */
	private StarData[] starData;

	/**
	 * Information about the state of unlockable towers - as users survive
	 * harder levels, more towers are unlocked and should remain unlocked once
	 * unlocked
	 */
	private UnlockData[] unlockData;
	
	public static int INITIAL_BUGBUCKS =3;

	/**
	 * Instantiates a new player. A database adapter is used to pull information
	 * from the local database to set up this player's fields.
	 * 
	 * @param db
	 *            the db
	 */
	public Player(DatabaseAdapter db) {

		starData = new StarData[MapInfo.NUMBER_OF_MAPS];
		unlockData = new UnlockData[UnlockData.NUMBER_OF_TOWERS];
		instances = new MapInfo[MapInfo.NUMBER_OF_MAPS];

		// Default values that may be changed later.
		soundOff = false;
		musicOff = true;

		// Instantiaate the database adapter reference.
		this.db = db;

		// Read user information from the local database.
		Cursor cursor = db.readUserData();

		if (0 == cursor.getCount()) {

			// If there are no rows in the local user table,
			// this means the game is running for the first time
			// and a new guest user will be created.

			System.out.println("First time applicaiton run. Guest user"
					+ " will be created.");

			// Default guest values.
			username = "Guest";
			isGuest = true;
			token = "";
			bugBucks = INITIAL_BUGBUCKS;
			String blankUpdateMapString = "";

			// Create the player in the local database.
			db.createUserData(username, token, bugBucks, isGuest,
					soundOff, musicOff, blankUpdateMapString);

			// Also create other defaults.
			for (int i = 0; i < MapInfo.NUMBER_OF_MAPS; i++) {
				starData[i] = new StarData(i, db);
				db.createStarData(i, 0, 0, 0);
			}
			for (int i = 0; i < UnlockData.NUMBER_OF_TOWERS; i++) {
				unlockData[i] = new UnlockData(i, db);
				db.createTowerData(i, 0);
			}
			for (int i = 0; i < MapInfo.NUMBER_OF_MAPS; i++) {
				instances[i] = new MapInfo(i, db);
				db.createMapInfoData(i, instances[i].getPlayerHealth(),
						instances[i].getCurrentWave(),
						instances[i].getCurrentWaveTime(), false, instances[i].getCoins());
			}
			
			updateMap = new UpdateMap();

		} else {

			// If there are any rows (there should only ever be one),
			// then this isn't the first time the game has been run
			// and a new player will be instantiated with the user
			// data that is in the local database.

			System.out.println("Loading player information from local"
					+ " database.");

			username = cursor.getString(1);
			token = cursor.getString(2);
			//coins = cursor.getInt(3);
			bugBucks = cursor.getInt(3);
			isGuest = cursor.getInt(4) > 0;
			
			//initialize update map from stored string XML representation
			String updateMapString = cursor.getString(7);
			updateMap = new UpdateMap(updateMapString);
			
			System.out.println("Username: " + username + " Token: " + token
					+  " Bugbucks: " + bugBucks
					+ " isGuest: " + isGuest);

			// /////////////////////////////////////////////////////////////////////
			// Load map information from the local database.

			Cursor mapInfoCursor;
			Cursor towerInfoCursor;
			Cursor bugInfoCursor;

			for (int i = 0; i < MapInfo.NUMBER_OF_MAPS; i++) {

				ArrayList<TowerInfo> towerInfo = new ArrayList<TowerInfo>();
				ArrayList<BugInfo> bugInfo = new ArrayList<BugInfo>();

				System.out
						.println("##########################################################");
				System.out.println("levelID: " + i);
				mapInfoCursor = db.readMapInfoData(i);
				System.out.println("mapInfoCursor count: "
						+ mapInfoCursor.getCount());
				towerInfoCursor = db.readTowerInfoData(i);
				System.out.println("towerInfoCursor count: "
						+ towerInfoCursor.getCount());
				bugInfoCursor = db.readBugInfoData(i);
				System.out.println("bugInfoCursor count: "
						+ bugInfoCursor.getCount());
				System.out
						.println("##########################################################");

				if (!(0 == towerInfoCursor.getCount())) {
					for (int j = 0; j < towerInfoCursor.getCount(); j++) {
						towerInfo.add(new TowerInfo(towerInfoCursor.getInt(2),
								towerInfoCursor.getInt(5), towerInfoCursor
										.getInt(3), towerInfoCursor.getInt(4),
								db, towerInfoCursor.getInt(1)));
						towerInfoCursor.moveToNext();
					}
				}

				if (!(0 == bugInfoCursor.getCount())) {
					for (int j = 0; j < bugInfoCursor.getCount(); j++) {
						bugInfo.add(new BugInfo(bugInfoCursor.getInt(2),
								bugInfoCursor.getInt(3), bugInfoCursor
										.getInt(4), bugInfoCursor.getInt(5),
								bugInfoCursor.getDouble(6), bugInfoCursor
										.getDouble(7), bugInfoCursor.getInt(8),
								bugInfoCursor.getInt(9),bugInfoCursor.getInt(10), db, bugInfoCursor
										.getInt(1)));
						bugInfoCursor.moveToNext();
					}
				}

				instances[i] = new MapInfo(mapInfoCursor.getInt(1),
						mapInfoCursor.getInt(3), mapInfoCursor.getInt(2),mapInfoCursor.getInt(6),
						towerInfo, bugInfo, mapInfoCursor.getInt(5) > 0,
						mapInfoCursor.getInt(4), db);

				mapInfoCursor.moveToNext();
			}

			// /////////////////////////////////////////////////////////////////////
			// Load star data from the local.

			Cursor starDataCursor;

			for (int i = 0; i < MapInfo.NUMBER_OF_MAPS; i++) {

				starDataCursor = db.readStarData(i);

				for (int j = 0; j < starDataCursor.getCount(); j++) {
					starData[i] = new StarData(starDataCursor.getInt(1),
							starDataCursor.getInt(2), starDataCursor.getInt(3),
							starDataCursor.getInt(4), db);
					starDataCursor.moveToNext();
				}
			}

			// /////////////////////////////////////////////////////////////////////
			// Load unlockData from the local.

			Cursor towerDataCursor;

			for (int i = 0; i < UnlockData.NUMBER_OF_TOWERS; i++) {

				towerDataCursor = db.readTowerData(i);

				for (int j = 0; j < towerDataCursor.getCount(); j++) {
					unlockData[i] = new UnlockData(towerDataCursor.getInt(1),
							towerDataCursor.getInt(2), db);
				}
			}
		}
	}

	/**
	 * Sets the authentication token.
	 * 
	 * @param newToken
	 *            the new token
	 */
	public void setToken(String newToken) {
		token = newToken;

		// Update token in the local database.
		db.updateUserAuthToken(newToken);
	}
  
	/**
	 * Gets the authentication token.
	 * 
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Gets the database adapter.
	 * 
	 * @return A reference to the database adapter for the local database.
	 */
	public DatabaseAdapter getDatabaseAdapter() {
		return db;
	}

	/**
	 * Gets the username.
	 * 
	 * @return the username
	 */
	public String getUserName() {
		return username;
	}

	/**
	 * Sets the username.
	 * 
	 * @param username
	 *            the new username
	 */
	public void setUsername(String username) {
		this.username = username;

		// Update username in the local database.
		db.updateUserUsername(username);

	}

	/**
	 * Checks if is guest.
	 * 
	 * @return true, if is guest
	 */
	public boolean isGuest() {
		return isGuest;
	}

	/**
	 * Sets the isGuest boolean.
	 * 
	 * @param newIsGuest
	 *            the value of isGuest
	 */
	public void setIsGuest(boolean newIsGuest) {
		this.isGuest = newIsGuest;

		// Update isGuest in the local database.
		db.updateUserIsGuest(newIsGuest);
	}

	/**
	 * Checks if is sound off.
	 * 
	 * @return true, if is sound off
	 */
	public boolean isSoundOff() {
		return soundOff;
	}

	/**
	 * Sets the sound off.
	 * 
	 * @param soundOff
	 *            the new sound off boolean
	 */
	public void setSoundOff(boolean soundOff) {
		this.soundOff = soundOff;

		// Update soundOff in the local database.
		db.updateUserSoundOff(soundOff);
	}

	/**
	 * Checks if is music is set to be off.
	 * 
	 * @return true, if is music is off
	 */
	public boolean isMusicOff() {
		return musicOff;
	}

	/**
	 * Sets the music off boolean.
	 * 
	 * @param musicOff
	 *            the new music off boolean
	 */
	public void setMusicOff(boolean musicOff) {
		this.musicOff = musicOff;

		// Update musicOff in the local database.
		db.updateUserMusicOff(musicOff);
	}

	/**
	 * Gets the MapInfo structure for the given level.
	 * 
	 * @param levelID
	 *            the level id to get MapInfo for
	 * @return the MapInfo for that level
	 */
	public MapInfo getMapInfo(int levelID) {
		return instances[levelID];
	}

	/**
	 * Resets the map in the program and in the database to initial values.
	 * 
	 * @param levelID
	 *            the level id to Reset
	 */
	public void resetLevel(int levelID) {
		instances[levelID].reset();
	}

	/**
	 * Updates a star�s state given the level, a constant �whichStar� which can
	 * be one of: StarInfo.LEFT, StarInfo.CENTER, StarInfo.RIGHT, and the new
	 * state which should be a 0 for no star and a 1 for a star.
	 * 
	 * @param levelID
	 *            the level id of the star to update
	 * @param whichStar
	 *            which star to update - should be one of: StarInfo.LEFT,
	 *            StarInfo.CENTER, StarInfo.RIGHT
	 * @param newState
	 *            the new state - should be 0 for a grey (empty) star or 1 for a
	 *            yellow (full) star
	 */
	public void updateStar(int levelID, int whichStar, int newState) {
		starData[levelID].updateStar(whichStar, newState);
		putUpdate(new StarUpdate(starData[levelID]));
	}

	/**
	 * Gets the integer array of star data for a given level.
	 * 
	 * @param levelID
	 *            the level to get star data for
	 * @return the star data
	 */
	public int[] getStars(int levelID) {
		return starData[levelID].getStars();
	}

	/**
	 * Gets the total number of stars that a player has at this time.
	 * 
	 * @return the total number of stars that this player has
	 */
	public int getNumberOfStars() {
		int toReturn = 0;
		for (int i = 0; i < MapInfo.NUMBER_OF_MAPS; i++) {
			toReturn += starData[i].getStarValue(StarData.LEFT);
			toReturn += starData[i].getStarValue(StarData.CENTER);
			toReturn += starData[i].getStarValue(StarData.RIGHT);
		}
		return toReturn;
	}

	/**
	 * Removes n stars from a player, starting at the highest level.
	 * 
	 * @param starsToRemove
	 *            the number of stars to remove
	 */
	public void removeNStars(int starsToRemove) {
		int i = MapInfo.NUMBER_OF_MAPS - 1;
		int j = 2;
//		System.out.println("going into loop: i, j, stremove: " + i + ", " + j
//				+ ", " + starsToRemove);
		while (0 <= i && 0 < starsToRemove) {
//			System.out.println("in loop: i, j, stremove: " + i + ", " + j
//					+ ", " + starsToRemove);
			while (0 <= j && 0 < starsToRemove) {
				int thisStar = starData[i].getStarValue(j);
//				System.out.println("level: " + i + " has a star at point: " + j
//						+ " with value: " + thisStar);
				if (1 == thisStar) {
					updateStar(i, j, 0);
					starsToRemove--;
					//System.out.println("starsToRemove: " + starsToRemove);
				}
				j--;
			}
			j = 2;
			i--;
		}
	}

	/**
	 * Finds out if the Tower is unlocked.
	 * 
	 * @param towerID
	 *            the tower id to check state of
	 * @return true, if tower is unlocked
	 */
	public boolean towerIsUnlocked(int towerID) {
		return unlockData[towerID].isUnlocked();
	}

	/**
	 * Finds out if this tower's maximum level is unlocked.
	 * 
	 * @param towerID
	 *            the tower id to check
	 * @return true, if this tower has had its maximum level unlocked
	 */
	public boolean towerIsMaxUpped(int towerID) {
		return unlockData[towerID].isMaxUpped();
	}

	/**
	 * Unlocks the tower.
	 * 
	 * @param towerID
	 *            the tower to unlock
	 */
	public void unlockTower(int towerID) {
		unlockData[towerID].unlock();
		putUpdate(new TowerUpdate(unlockData[towerID]));
	}

	/**
	 * Unlocks a tower's highest level.
	 * 
	 * @param towerID
	 *            the tower to maxUp
	 */
	public void maxUpTower(int towerID) {
		unlockData[towerID].maxUp();
		putUpdate(new TowerUpdate(unlockData[towerID]));
	}

	/**
	 * Adds n bug bucks to player.
	 * 
	 * @param n
	 *            the number of bugbucks to add
	 */
	public void addBugBucks(int n) {
		bugBucks += n;

		// Update coins in the local database.
		db.updateUserBugBucks(bugBucks);

		putUpdate(new BugBuckUpdate(bugBucks));
	}

	/**
	 * Subtract n bug bucks.
	 * 
	 * @param n
	 *            the number of bugbucks to subtract
	 */
	public void subtractBugBucks(int n) {
		bugBucks -= n;
		db.updateUserBugBucks(bugBucks);
		putUpdate(new BugBuckUpdate(bugBucks));
	}

	/**
	 * Gets the number of bug bucks.
	 * 
	 * @return the number of bug bucks
	 */
	public int getBugBucks() {
		return bugBucks;
	}

	/**
	 * Sets the bug bucks - should only be used when changing players. Purchases
	 * and bugbucks earned should use addBugbucks and subtractBugbucks.
	 * 
	 * @param bugBucks
	 *            the bug bucks
	 * @return the int
	 */
	public int setBugBucks(int bugBucks) {
		db.updateUserBugBucks(bugBucks);
		this.bugBucks = bugBucks;
		return this.bugBucks;
	}

	/**
	 * Resets all map info - used when a registered player registers as someone
	 * else or starts as a new guest.
	 */
	public void resetAllMapInfo() {

		// Clear map/bug/tower information from the database.
		db.deleteAllMapInfoData();
		db.deleteAllBugInfoData();
		db.deleteAllTowerInfoData();

		// Create the default map information.
		for (int i = 0; i < MapInfo.NUMBER_OF_MAPS; i++) {
			instances[i] = new MapInfo(i, db);
			db.createMapInfoData(i, instances[i].getPlayerHealth(),
					instances[i].getCurrentWave(),
					instances[i].getCurrentWaveTime(), false, instances[i].getCoins());
		}

	};

	/**
	 * Reset player to guest default values.
	 */
	public void resetPlayerToGuest() {
		username = "Guest";
		isGuest = true;
		token = "";
		bugBucks = 3;
		soundOff = false;
		musicOff = true;
	}

	/**
	 * Reset update map by clearing it on a new thread
	 */
	public void resetUpdateMap() {
		new RunnableTask().execute(new ClearMap());
		updateMap = new UpdateMap();
	};

	/**
	 * Put update into map on a new thread
	 * 
	 * @param update
	 *            the update to put
	 */
	private void putUpdate(AbstractUpdate update) {
		new RunnableTask().execute(new PutInMap(update));
	}
	
	public void logoutOldUser(String oldToken){
		putUpdate(new LogoutUpdate(oldToken));
	}

	/**
	 * Perform update on a new thread
	 * 
	 */
	public void performUpdate() {

			new RunnableTask().execute(new UpdateDatabaseAttempt());
	}
	
	public void blankSlate(){
		for( MapInfo m : instances)
			m.reset();
		for(StarData s : starData)
			s.reset();
		for(UnlockData u : unlockData)
			u.reset();
		
		resetUpdateMap();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MockUser [updateMap=" + updateMap + ", starData="
				+ Arrays.toString(starData) + ", unlockData="
				+ Arrays.toString(unlockData) + 
				 ", bugBucks=" + bugBucks + ", token=" + token + "]";
	}

	/**
	 * The Class ClearMap simply clears the given UpdateMap. It calls a
	 * synchronized method to do so, so it will queue up behind other threads
	 * also waiting to access the UpdateMap, if there are any.
	 * 
	 * @author carlchapman
	 * 
	 */
	class ClearMap implements Runnable {

		public void run() {
			updateMap.clear();
			db.updateUserUpdateMap("");
		}
	}

	/**
	 * The Class PutInMap puts the given update into the given updatemap. It
	 * calls a synchronized method to do so, so it will queue up behind other
	 * threads also waiting to access the UpdateMap, if there are any.
	 * 
	 * @author carlchapman
	 */
	class PutInMap implements Runnable {

		/** The update. */
		private AbstractUpdate update;

		/**
		 * Instantiates a new put in map.
		 * 
		 * @param update
		 *            the update
		 * @param updateMap
		 *            the update map
		 */
		public PutInMap(AbstractUpdate update) {
			this.update = update;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			// this calls a synchronized method, making sure it does not
			// interleave data in updateMap
			updateMap.putUpdate(update);
			db.updateUserUpdateMap(updateMap.toXML());
		}
	}

	/**
	 * The Class UpdateDatabaseAttempt attempts to perform an update, using the
	 * given user token to connect to the remote server. It calls a synchronized
	 * method to do so, so it will queue up behind other threads also waiting to
	 * access the UpdateMap, if there are any.
	 * 
	 * @author carlchapman
	 */
	class UpdateDatabaseAttempt implements Runnable {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			// this calls a synchronized method, making sure it does not
			// interleave the data in updateMap
			updateMap.update(token,isGuest);
			db.updateUserUpdateMap(updateMap.toXML());
		}

	}

	public void updateUnlockData(String givenTowerID, String givenTowerState) {
		int towerID = Integer.parseInt(givenTowerID);
		int towerState = Integer.parseInt(givenTowerState);
		unlockData[towerID].updateTower(towerState);
		db.updateTowerData(towerID, towerState);
	}

}
