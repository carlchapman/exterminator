package app.exterminator.data.facades;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Database adapter used to interface with local database (phone storage).
 * 
 * @author colegroff
 */
public class DatabaseAdapter {
	
	/////////////////////////////////////////////////////////////////////////////////////////
	
	private static final String DATABASE_NAME = "data";
	private static final String TAG = "DatabaseAdapter";
	private static final int DATABASE_VERSION = 1;
	
	/////////////////////////////////////////////////////////////////////////////////////////
	
	private DatabaseHelper databaseHelper;
	private SQLiteDatabase database;
	private final Context context;
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// Reusable column names.
	
	private static final String KEY_ID 		 = "_id";
	private static final String KEY_LEVEL_ID = "levelId";
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// userDataTable
	
	private static final String USER_DATA_TABLE 			   = "userDataTable";
	private static final String USER_DATA_TABLE_KEY_USERNAME   = "username";
	private static final String USER_DATA_TABLE_KEY_AUTH_TOKEN = "authToken";
	private static final String USER_DATA_TABLE_KEY_BUG_BUCKS  = "bugBucks";
	private static final String USER_DATA_TABLE_KEY_IS_GUEST   = "isGuest";
	private static final String USER_DATA_TABLE_KEY_SOUND_OFF  = "soundOff";
	private static final String USER_DATA_TABLE_KEY_MUSIC_OFF  = "musicOff";
	private static final String USER_DATA_TABLE_KEY_UPDATE_MAP  = "updateMap";
	
	private static final String CREATE_USER_DATA_TABLE =
			"CREATE TABLE " + USER_DATA_TABLE + " (" +
					KEY_ID 						     + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					USER_DATA_TABLE_KEY_USERNAME     + " TEXT NOT NULL, " +
					USER_DATA_TABLE_KEY_AUTH_TOKEN   + " TEXT NOT NULL, " +
					USER_DATA_TABLE_KEY_BUG_BUCKS    + " INTEGER NOT NULL, " +     
					USER_DATA_TABLE_KEY_IS_GUEST 	 + " BOOLEAN NOT NULL, " +
					USER_DATA_TABLE_KEY_SOUND_OFF    + " BOOLEAN NOT NULL, " +
					USER_DATA_TABLE_KEY_MUSIC_OFF    + " BOOLEAN NOT NULL,"   +
					USER_DATA_TABLE_KEY_UPDATE_MAP    + " TEXT NOT NULL"   +
			");";
	
	public static final String[] USER_DATA_TABLE_COLUMNS = {
		KEY_ID,
		USER_DATA_TABLE_KEY_USERNAME,
		USER_DATA_TABLE_KEY_AUTH_TOKEN,
		USER_DATA_TABLE_KEY_BUG_BUCKS,
		USER_DATA_TABLE_KEY_IS_GUEST,
		USER_DATA_TABLE_KEY_SOUND_OFF,
		USER_DATA_TABLE_KEY_MUSIC_OFF,
		USER_DATA_TABLE_KEY_UPDATE_MAP
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// mapInfoDataTable
	
	private static final String MAP_INFO_DATA_TABLE 				      = "mapInfoDataTable";
	private static final String MAP_INFO_DATA_TABLE_KEY_PLAYER_HEALTH 	  = "playerHealth";
	private static final String MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE      = "currentWave";
	private static final String MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE_TIME = "currentWaveTime";
	private static final String MAP_INFO_DATA_TABLE_KEY_WAS_SAVED		  = "wasSaved";
	private static final String MAP_INFO_DATA_TABLE_KEY_COINS		  = "coins";
	
	private static final String CREATE_MAP_INFO_DATA_TABLE =
			"CREATE TABLE " + MAP_INFO_DATA_TABLE + " ("  +
					KEY_ID								  		  + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					KEY_LEVEL_ID						          + " INTEGER NOT NULL, "  +
					MAP_INFO_DATA_TABLE_KEY_PLAYER_HEALTH 	  	  + " INTEGER NOT NULL, "  +
					MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE 		  + " INTEGER NOT NULL,"   +
					MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE_TIME	  + " INTEGER NOT NULL,"   +
					MAP_INFO_DATA_TABLE_KEY_WAS_SAVED			  + " BOOLEAN NOT NULL, "	   +
					MAP_INFO_DATA_TABLE_KEY_COINS			  + " INTEGER NOT NULL"	   +
			");";
	
	public static final String[] MAP_INFO_DATA_TABLE_COLUMNS = {
		KEY_ID,
		KEY_LEVEL_ID,
		MAP_INFO_DATA_TABLE_KEY_PLAYER_HEALTH,
		MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE,
		MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE_TIME,
		MAP_INFO_DATA_TABLE_KEY_WAS_SAVED,
		MAP_INFO_DATA_TABLE_KEY_COINS
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// starDataTable
	
	private static final String STAR_DATA_TABLE 			 = "starDataTable";
	private static final String STAR_DATA_TABLE_KEY_STAR	 = "star";
	private static final String STAR_DATA_TABLE_KEY_STAR0 	 = "star0";
	private static final String STAR_DATA_TABLE_KEY_STAR1 	 = "star1";
	private static final String STAR_DATA_TABLE_KEY_STAR2 	 = "star2";
	
	private static final String CREATE_STAR_DATA_TABLE =
			"CREATE TABLE " + STAR_DATA_TABLE + " (" +
					KEY_ID					  + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					KEY_LEVEL_ID 			  + " INTEGER NOT NULL, " +
					STAR_DATA_TABLE_KEY_STAR0 + " INTEGER NOT NULL, " +
					STAR_DATA_TABLE_KEY_STAR1 + " INTEGER NOT NULL, " +
					STAR_DATA_TABLE_KEY_STAR2 + " INTEGER NOT NULL"  +
			");";
	
	public static final String[] STAR_DATA_TABLE_COLUMNS = {
		KEY_ID,
		KEY_LEVEL_ID,
		STAR_DATA_TABLE_KEY_STAR0,
		STAR_DATA_TABLE_KEY_STAR1,
		STAR_DATA_TABLE_KEY_STAR2
	};

	/////////////////////////////////////////////////////////////////////////////////////////
	// bugInfoDataTable
	
	private static final String BUG_INFO_DATA_TABLE 						 = "bugInfoDataTable";
	private static final String BUG_INFO_DATA_TABLE_KEY_LEVEL_ID		     = "levelId";
	private static final String BUG_INFO_DATA_TABLE_KEY_BUG_ID 				 = "bugId";
	private static final String BUG_INFO_DATA_TABLE_KEY_X 					 = "x";
	private static final String BUG_INFO_DATA_TABLE_KEY_Y 					 = "y";
	private static final String BUG_INFO_DATA_TABLE_KEY_NODE_INDEX 			 = "nodeIndex";
	private static final String BUG_INFO_DATA_TABLE_KEY_X_SPEED_SCALAR 		 = "xSpeedScalar";
	private static final String BUG_INFO_DATA_TABLE_KEY_Y_SPEED_SCALAR 		 = "ySpeedScalar";
	private static final String BUG_INFO_DATA_TABLE_KEY_HEALTH 				 = "health";
	private static final String BUG_INFO_DATA_TABLE_KEY_CURRENT_BITMAP_FRAME = "currentBitMapFrame";
	private static final String BUG_INFO_DATA_TABLE_KEY_PATH_INDEX = "pathIndex";

	private static final String CREATE_BUG_INFO_DATA_TABLE =
			"CREATE TABLE " + BUG_INFO_DATA_TABLE + " (" +
					KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " INTEGER NOT NULL, " +
					BUG_INFO_DATA_TABLE_KEY_BUG_ID + " INTEGER NOT NULL, " +
					BUG_INFO_DATA_TABLE_KEY_X + " INTEGER NOT NULL, " +
					BUG_INFO_DATA_TABLE_KEY_Y + " INTEGER NOT NULL, " +
					BUG_INFO_DATA_TABLE_KEY_NODE_INDEX + " INTEGER NOT NULL, " +
					BUG_INFO_DATA_TABLE_KEY_X_SPEED_SCALAR + " DOUBLE NOT NULL, " +
					BUG_INFO_DATA_TABLE_KEY_Y_SPEED_SCALAR + " DOUBLE NOT NULL, " +
					BUG_INFO_DATA_TABLE_KEY_HEALTH + " INTEGER NOT NULL, " +
					BUG_INFO_DATA_TABLE_KEY_CURRENT_BITMAP_FRAME + " INTEGER NOT NULL, " +
					BUG_INFO_DATA_TABLE_KEY_PATH_INDEX + " INTEGER NOT NULL" +
			");";
	
	public static final String[] BUT_INFO_DATA_TABLE_COLUMNS = {
		KEY_ID,
		BUG_INFO_DATA_TABLE_KEY_LEVEL_ID,
		BUG_INFO_DATA_TABLE_KEY_BUG_ID,
		BUG_INFO_DATA_TABLE_KEY_X,
		BUG_INFO_DATA_TABLE_KEY_Y,
		BUG_INFO_DATA_TABLE_KEY_NODE_INDEX,
		BUG_INFO_DATA_TABLE_KEY_X_SPEED_SCALAR,
		BUG_INFO_DATA_TABLE_KEY_Y_SPEED_SCALAR,
		BUG_INFO_DATA_TABLE_KEY_HEALTH,
		BUG_INFO_DATA_TABLE_KEY_CURRENT_BITMAP_FRAME,
		BUG_INFO_DATA_TABLE_KEY_PATH_INDEX
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// towerDataTable
	
	private static final String TOWER_DATA_TABLE 				   = "towerDataTable";
	private static final String TOWER_DATA_TABLE_KEY_TOWER_ID 	   = "towerId";
	private static final String TOWER_DATA_TABLE_KEY_TOWER_STATE   = "towerState";
	
	private static final String CREATE_TOWER_DATA_TABLE =
			"CREATE TABLE " + TOWER_DATA_TABLE + " ("  +
					KEY_ID 							   + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					TOWER_DATA_TABLE_KEY_TOWER_ID 	   + " INTEGER NOT NULL, " +
					TOWER_DATA_TABLE_KEY_TOWER_STATE   + " INTEGER NOT NULL" +
			");";
	
	public static final String[] TOWER_DATA_TABLE_COLUMNS = {
		KEY_ID,
		TOWER_DATA_TABLE_KEY_TOWER_ID,
		TOWER_DATA_TABLE_KEY_TOWER_STATE
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// towerInfoDataTable
	
	private static final String TOWER_INFO_DATA_TABLE 				  = "towerInfoDataTable";
	private static final String TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID 	  = "levelId";
	private static final String TOWER_INFO_DATA_TABLE_KEY_TOWER_ID 	  = "towerId";
	private static final String TOWER_INFO_DATA_TABLE_KEY_X 		  = "x";
	private static final String TOWER_INFO_DATA_TABLE_KEY_Y 		  = "y";
	private static final String TOWER_INFO_DATA_TABLE_KEY_TOWER_STATE = "towerStateLevel";
	
	private static final String CREATE_TOWER_INFO_DATA_TABLE =
			"CREATE TABLE " + TOWER_INFO_DATA_TABLE + " (" +
					KEY_ID 								  + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID 	  + " INTEGER NOT NULL, " +
					TOWER_INFO_DATA_TABLE_KEY_TOWER_ID 	  + " INTEGER NOT NULL, " +
					TOWER_INFO_DATA_TABLE_KEY_X 		  + " INTEGER NOT NULL, " +
					TOWER_INFO_DATA_TABLE_KEY_Y 		  + " INTEGER NOT NULL, " +
					TOWER_INFO_DATA_TABLE_KEY_TOWER_STATE + " INTEGER NOT NULL" +
			");";
	
	public static final String[] TOWER_INFO_DATA_TABLE_COLUMNS = {
		KEY_ID,
		TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID,
		TOWER_INFO_DATA_TABLE_KEY_TOWER_ID,
		TOWER_INFO_DATA_TABLE_KEY_X,
		TOWER_INFO_DATA_TABLE_KEY_Y,
		TOWER_INFO_DATA_TABLE_KEY_TOWER_STATE
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Constructor - takes the context to allow the database to be
     * opened/created.
     * 
     * @param givenContext the Context within which to work
     */
    public DatabaseAdapter(Context givenContext) {    	
        context = givenContext;        
    }
    
    /**
     * Open the database.  If it cannot be opened, try to create a new
     * instance of the database.  If it cannot be created, throw an exception to
     * signal the failure.
     * 
     * @return this (self reference, allowing this to be chained in an
     *		   initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public DatabaseAdapter open() throws SQLException {    	
    	databaseHelper = new DatabaseHelper(context);
    	database = databaseHelper.getWritableDatabase();
    	return this;    	
    }
    
    /**
     * Close any open database object.
     */
    public void close() {
    	databaseHelper.close();
    }

	/////////////////////////////////////////////////////////////////////////////////////////    
    // userDataTable CRUD
    
    /**
     * Create a row in the local database for user information.  If the row is successfully
     * created return the primary key for the row, otherwise return -1 to indicate failure.
     * 
     * @param givenUsername given username
     * @param givenAuthToken given authentication token
     * @param givenCoins given number of coins
     * @param givenIsGuest given isguest boolean
     * @param givenSoundOff given soundoff boolean
     * @param givenMusicOff given musicoff boolean
     * @return primary key or -1 if failed
     */
    public long createUserData(String givenUsername,
    		String givenAuthToken, int givenBugBucks, boolean givenIsGuest,
    		boolean givenSoundOff, boolean givenMusicOff, String givenUpdateMap) {
    	ContentValues initialValues = new ContentValues();
    	initialValues.put(USER_DATA_TABLE_KEY_USERNAME, givenUsername);
    	initialValues.put(USER_DATA_TABLE_KEY_AUTH_TOKEN, givenAuthToken);
    	initialValues.put(USER_DATA_TABLE_KEY_BUG_BUCKS, givenBugBucks);
    	initialValues.put(USER_DATA_TABLE_KEY_IS_GUEST, givenIsGuest);
    	initialValues.put(USER_DATA_TABLE_KEY_SOUND_OFF, givenSoundOff);
    	initialValues.put(USER_DATA_TABLE_KEY_MUSIC_OFF, givenMusicOff);
    	initialValues.put(USER_DATA_TABLE_KEY_UPDATE_MAP, givenUpdateMap);
    	return database.insert(USER_DATA_TABLE, null, initialValues);
    }
    
    /**
     * Return a Cursor over the user's row in the database
     * 
     * @return Cursor positioned to the user's row in the database
     * @throws SQLException if row could not be found/retrieved
     */
    public Cursor readUserData() throws SQLException {
    	Cursor cursor = database.query(true,
    					USER_DATA_TABLE,
					    USER_DATA_TABLE_COLUMNS,
					    null, null, null, null, null, null);
    	if(null != cursor) {
    		cursor.moveToFirst();
    	}
    	return cursor;
    }
    
    /**
     * Update the user's username in the database
     * 
     * @param givenUsername username to update to
     * @return true if username was successfully updated, false otherwise
     */
    public boolean updateUserUsername(String givenUsername) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(USER_DATA_TABLE_KEY_USERNAME, givenUsername);
    	return database.update(USER_DATA_TABLE, givenArguments, null, null) > 0;
    }
    
    /**
     * Update the user's isGuest status
     * 
     * @param givenIsGuest isGuest status to update to
     * @return true if isGuest was successfully updated, false otherwise
     */
    public boolean updateUserIsGuest(boolean givenIsGuest) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(USER_DATA_TABLE_KEY_IS_GUEST, givenIsGuest);
    	return database.update(USER_DATA_TABLE, givenArguments, null, null) > 0;
    }
    
    /**
     * Update the user's authentication token
     * 
     * @param givenAuthToken authentication token to update to
     * @return true if the authenticatoin token was successfully updated,
     * 				false otherwise
     */
    public boolean updateUserAuthToken(String givenAuthToken) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(USER_DATA_TABLE_KEY_AUTH_TOKEN, givenAuthToken);
    	return database.update(USER_DATA_TABLE, givenArguments, null, null) > 0;
    }
    
    /**
     * Update the user's number of bugbucks
     * 
     * @param givenBugBucks number of bugbucks to update to
     * @return true if the number of bugbucks was successfully updated,
     * 				false otherwise
     */
    public boolean updateUserBugBucks(int givenBugBucks) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(USER_DATA_TABLE_KEY_BUG_BUCKS, givenBugBucks);
    	return database.update(USER_DATA_TABLE, givenArguments, null, null) > 0;
    }
    
    /**
     * Update the user's soundOff variable
     * 
     * @param givenSoundOff boolean to update soundOff to
     * @return true if the soundOff variable was successfully updated,
     * 				false otherwise
     */
    public boolean updateUserSoundOff(boolean givenSoundOff) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(USER_DATA_TABLE_KEY_SOUND_OFF, givenSoundOff);
    	return database.update(USER_DATA_TABLE, givenArguments, null, null) > 0;
    }
    
    /**
     * Update the user's musicOff variable
     * 
     * @param givenMusicOff boolean to update musicOff to
     * @return true if the musicOff variable was successfully updated,
     * 				false otherwise
     */
    public boolean updateUserMusicOff(boolean givenMusicOff) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(USER_DATA_TABLE_KEY_MUSIC_OFF, givenMusicOff);
    	return database.update(USER_DATA_TABLE, givenArguments, null, null) > 0;
    }
    
    public boolean updateUserUpdateMap(String givenUpdateMap) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(USER_DATA_TABLE_KEY_UPDATE_MAP, givenUpdateMap);
    	return database.update(USER_DATA_TABLE, givenArguments, null, null) > 0;
    }
    
    /**
     * Delete the user's information from the database
     * 
     * @return true if deleted, false otherwise
     */
    public boolean deleteUserData() {
    	return database.delete(USER_DATA_TABLE, null, null) > 0;
    }     
    
	/////////////////////////////////////////////////////////////////////////////////////////    
    // mapInfoDataTable CRUD
    
    /**
     * Create a row in the database for map information.
     * 
     * @param givenLevelId the level id of the map
     * @param givenPlayerHealth the player's health
     * @param givenCurrentWave the current wave
     * @param givenCurrentWaveTime the current wave time
     * @param givenWasSaved if the level has been saved or not
     * @return the primary key or -1 if failed
     */
    public long createMapInfoData(int givenLevelId, int givenPlayerHealth, int givenCurrentWave,
    		int givenCurrentWaveTime, boolean givenWasSaved, int givenCoins) {    	 
    	ContentValues initialValues = new ContentValues();
    	initialValues.put(KEY_LEVEL_ID, givenLevelId);
    	initialValues.put(MAP_INFO_DATA_TABLE_KEY_PLAYER_HEALTH, givenPlayerHealth);
    	initialValues.put(MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE, givenCurrentWave);
    	initialValues.put(MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE_TIME, givenCurrentWaveTime);
    	initialValues.put(MAP_INFO_DATA_TABLE_KEY_WAS_SAVED, givenWasSaved);
    	initialValues.put(MAP_INFO_DATA_TABLE_KEY_COINS, givenCoins);
    	return database.insert(MAP_INFO_DATA_TABLE, null, initialValues);
    }

    /**
     * Return a Cursor over the map information with the given level id
     * 
     * @param givenLevelId the level id for the map information to get
     * @return Cursor over the map information with the given level id
     * @throws SQLException if map information could not be found/retrieved
     */
    public Cursor readMapInfoData(int givenLevelId) throws SQLException {
    	Cursor cursor = database.query(true,
    					MAP_INFO_DATA_TABLE,
    					MAP_INFO_DATA_TABLE_COLUMNS,
					    KEY_LEVEL_ID + " = " + givenLevelId,
					    null, null, null, null, null);
    	if(null != cursor) {
    		cursor.moveToFirst();
    	}
    	return cursor;
    }
    
    /**
     * Update map information for the level with the given level id
     * 
     * @param givenLevelId the level id
     * @param givenPlayerHealth the player's health
     * @param givenCurrentWave current wave
     * @param givenCurrentWaveTime current wave time
     * @param givenWasSaved if the level has been saved or not
     * @return true if the map information was successfully updated, false otherwise
     */
    public boolean updateMapInfoData(int givenLevelId, int givenPlayerHealth, int givenCurrentWave,
    		int givenCurrentWaveTime, boolean givenWasSaved, int givenCoins) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_PLAYER_HEALTH, givenPlayerHealth);
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE, givenCurrentWave);
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE_TIME, givenCurrentWaveTime);
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_WAS_SAVED, givenWasSaved);
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_COINS, givenCoins);
    	return database.update(MAP_INFO_DATA_TABLE, givenArguments, KEY_LEVEL_ID + " = " + givenLevelId, null) > 0;
    }
    
    public boolean updateMapInfoCoins(int givenLevelId, int givenCoins) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_COINS, givenCoins);
    	return database.update(MAP_INFO_DATA_TABLE, givenArguments, KEY_LEVEL_ID + " = " + givenLevelId, null) > 0;
    }
    
    /**
     * Update the level's wasSaved variable in the database for the level
     * with the given level id
     * 
     * @param givenLevelId id of level to update
     * @param givenWasSaved if the level has been saved or not
     * @return true if the wasSaved variable was successfully updated, false otherwise
     */
    public boolean updateWasSaved(int givenLevelId, boolean givenWasSaved) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_WAS_SAVED, givenWasSaved);
    	return database.update(MAP_INFO_DATA_TABLE, givenArguments, KEY_LEVEL_ID + " = " + givenLevelId, null) > 0;
    }
    
    /**
     * Update the current wave time variable in the database for the level
     * with the given level id
     * 
     * @param givenLevelId id of level to update
     * @param givenCurrentWaveTime the current wave time
     * @return true if the current wave time was successfully updated, false otherwise
     */
    public boolean updateCurrentWaveTime(int givenLevelId, int givenCurrentWaveTime) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE_TIME, givenCurrentWaveTime);
    	return database.update(MAP_INFO_DATA_TABLE, givenArguments, KEY_LEVEL_ID + " = " + givenLevelId, null) > 0;
    }
    
    /**
     * Update the player's health variable in the database for the level
     * with the given level id
     * 
     * @param givenLevelId id of the level to update
     * @param givenPlayerHealth the player's health
     * @return true if the player's health was successfully updated, false otherwise
     */
    public boolean updatePlayerHealth(int givenLevelId, int givenPlayerHealth) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_PLAYER_HEALTH, givenPlayerHealth);
    	return database.update(MAP_INFO_DATA_TABLE, givenArguments, KEY_LEVEL_ID + " = " + givenLevelId, null) > 0;
    }
    
    /**
     * Update the current wave variable in the database for the level with the
     * given id
     * 
     * @param givenLevelId id of the level to update
     * @param givenCurrentWave the current wave
     * @return true if the current wave was successfully updated, false otherwise
     */
    public boolean updateCurrentWave(int givenLevelId, int givenCurrentWave) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(MAP_INFO_DATA_TABLE_KEY_CURRENT_WAVE, givenCurrentWave);
    	return database.update(MAP_INFO_DATA_TABLE, givenArguments, KEY_LEVEL_ID + " = " + givenLevelId, null) > 0;
    }    
    
    /**
     * Delete map information in the database for the level with the given level id
     * 
     * @param givenLevelId id of level to delete
     * @return true if deleted, false otherwise
     */
    public boolean deleteMapInfoData(int givenLevelId) {
    	return database.delete(MAP_INFO_DATA_TABLE, KEY_LEVEL_ID + " = " + givenLevelId, null) > 0;
    }
    
    /**
     * Delete all map information in the database
     * 
     * @return true if all information was deleted, false otherwise
     */
    public boolean deleteAllMapInfoData() {
    	return database.delete(MAP_INFO_DATA_TABLE, null, null) > 0;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////
    // starDataTable CRUD
    
    /**
     * Create a star data row in the database
     * 
     * @param givenLevelId
     * @param givenStar1
     * @param givenStar2
     * @param givenStar3
     * @return
     */
    public long createStarData(int givenLevelId, int givenStar1, int givenStar2, int givenStar3) {
    	ContentValues initialValues = new ContentValues();
    	initialValues.put(KEY_LEVEL_ID, givenLevelId);
    	initialValues.put(STAR_DATA_TABLE_KEY_STAR0, givenStar1);
    	initialValues.put(STAR_DATA_TABLE_KEY_STAR1, givenStar2);
    	initialValues.put(STAR_DATA_TABLE_KEY_STAR2, givenStar3);
    	return database.insert(STAR_DATA_TABLE, null, initialValues);
    }
    
    /**
     * Return a Cursor positioned at the star data that matches the given level id
     * 
     * @param givenLevelId the given level id
     * @return Cursor positioned at the matching star data, if found
     * @throws SQLException if the star data could not be found/retrieved
     */
    public Cursor readStarData(int givenLevelId) throws SQLException {
    	Cursor cursor = database.query(true,
					    STAR_DATA_TABLE,
					    STAR_DATA_TABLE_COLUMNS,
					    KEY_LEVEL_ID + " = " + givenLevelId,
					    null, null, null, null, null);
    	if(null != cursor) {
    		cursor.moveToFirst();
    	}
    	return cursor;
    }
    
    /**
     * Update star data for the level at the given level id
     * 
     * @param givenLevelId the given level id
     * @param givenStar1 star1
     * @param givenStar2 star2
     * @param givenStar3 star3
     * @return true if the star data was successfully updated, false otherwise
     */
    public boolean updateStarData(int givenLevelId, int givenWhichStar, int givenNewState) {
    	ContentValues givenArguments = new ContentValues();
    	String whichStar = STAR_DATA_TABLE_KEY_STAR + givenWhichStar;
    	givenArguments.put(whichStar, givenNewState);
    	return database.update(STAR_DATA_TABLE, givenArguments, KEY_LEVEL_ID + " = " + givenLevelId, null) > 0;
    }
    
    /**
     * Delete star data for the level at the given level id
     * 
     * @param givenLevelId the given level id
     * @return true if the star data was successfully deleted, false otherwise
     */
    public boolean deleteStarData(int givenLevelId) {
    	return database.delete(STAR_DATA_TABLE, KEY_LEVEL_ID + " = " + givenLevelId, null) > 0;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////
    // towerDataTable CRUD
    
    /**
     * Create a tower data row in the database
     * 
     * @param givenTowerId id for which type of tower
     * @param givenUpgradeLevel the tower's upgrade level
     * @return the primary key for the newly created row, or -1 if failed to create
     */
    public long createTowerData(int givenTowerId, int givenTowerState) {
    	ContentValues initialValues = new ContentValues();
    	initialValues.put(TOWER_DATA_TABLE_KEY_TOWER_ID, givenTowerId);
    	initialValues.put(TOWER_DATA_TABLE_KEY_TOWER_STATE, givenTowerState);
    	return database.insert(TOWER_DATA_TABLE, null, initialValues);
    }
    
    /**
     * Return a Cursor positioned at the row with the given tower id
     * 
     * @param givenTowerId the given tower id
     * @return a Cursor positioned at the row with the given tower id, if found
     * @throws SQLException if the tower data could not be found/retrieved
     */
    public Cursor readTowerData(int givenTowerId) throws SQLException {
    	Cursor cursor = database.query(true,
    					TOWER_DATA_TABLE,
    					TOWER_DATA_TABLE_COLUMNS,
    					TOWER_DATA_TABLE_KEY_TOWER_ID + " = " + givenTowerId,
					    null, null, null, null, null);
    	if(null != cursor) {
    		cursor.moveToFirst();
    	}
    	return cursor;
    }

    /**
     * Update the tower data for the tower with the given tower id
     * 
     * @param givenTowerId given tower id
     * @param givenUpgradeLevel upgrade level of tower
     * @return true if the tower data was successfully updated, false otherwise
     */
    public boolean updateTowerData(int givenTowerId, int givenTowerState) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(TOWER_DATA_TABLE_KEY_TOWER_STATE, givenTowerState);
    	return database.update(TOWER_DATA_TABLE, givenArguments, TOWER_DATA_TABLE_KEY_TOWER_ID + " = " + givenTowerId, null) > 0;
    }
    
    /**
     * Delete the tower data for the tower with the given tower id
     * 
     * @param givenTowerId the given tower id
     * @return true if the tower data was successfully deleted, false otherwise
     */
    public boolean deleteTowerData(int givenTowerId) {
    	return database.delete(TOWER_DATA_TABLE, TOWER_DATA_TABLE_KEY_TOWER_ID + " = " + givenTowerId, null) > 0;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////
    // towerInfoDataTable CRUD
    
    /**
     * Create tower information
     * 
     * @param givenLevelId given level id
     * @param givenTowerId given tower id
     * @param givenX x location
     * @param givenY y location
     * @param givenTowerState given tower state
     * @return the primary key for the tower information, or -1 if failed
     */
    public long createTowerInfoData(int givenLevelId, int givenTowerId, int givenX,
    		int givenY, int givenTowerState) {
    	ContentValues initialValues = new ContentValues();
    	initialValues.put(TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID, givenLevelId);
    	initialValues.put(TOWER_INFO_DATA_TABLE_KEY_TOWER_ID, givenTowerId);
    	initialValues.put(TOWER_INFO_DATA_TABLE_KEY_X, givenX);
    	initialValues.put(TOWER_INFO_DATA_TABLE_KEY_Y, givenY);
    	initialValues.put(TOWER_INFO_DATA_TABLE_KEY_TOWER_STATE, givenTowerState);
    	return database.insert(TOWER_INFO_DATA_TABLE, null, initialValues);
    }
    
    /**
     * Return a Cursor positioned at the row for the tower information with the given
     * level id, tower id, and primary key
     * 
     * @param givenLevelId the given level id
     * @param givenTowerId the given tower id
     * @param givenPrimaryKey the given primary key
     * @return a Cursor position at the tower information, if found
     * @throws SQLException if the tower information could not be found/retrieved
     */
    public Cursor readTowerInfoData(int givenLevelId, int givenTowerId, int givenPrimaryKey) throws SQLException {    	
    	Cursor cursor = database.query(true,
    					TOWER_INFO_DATA_TABLE,
    					TOWER_INFO_DATA_TABLE_COLUMNS,
    					TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId + " AND " +
    					TOWER_INFO_DATA_TABLE_KEY_TOWER_ID + " = " + givenTowerId + " AND " +
    					KEY_ID + " = " + givenPrimaryKey,
					    null, null, null, null, null);
    	if(null != cursor) {
    		cursor.moveToFirst();
    	}
    	return cursor;
    }
    
    /**
     * Return a Cursor positioned at the row for the tower information with the
     * given level id
     * 
     * @param givenLevelId given level id
     * @return a Cursor positioned at the row for the tower information, if found
     * @throws SQLException id the tower information could not be found/retrieved
     */
    public Cursor readTowerInfoData(int givenLevelId) throws SQLException {    	
    	Cursor cursor = database.query(true,
    					TOWER_INFO_DATA_TABLE,
    					TOWER_INFO_DATA_TABLE_COLUMNS,
    					TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId,
					    null, null, null, null, null);
    	if(null != cursor) {
    		cursor.moveToFirst();
    	}
    	return cursor;
    }

    /**
     * Update tower information for the tower with the given level id, tower id,
     * and primary key
     * 
     * @param givenLevelId given level id
     * @param givenTowerId given tower id
     * @param givenX x position
     * @param givenY y position
     * @param givenTowerState given tower state
     * @param givenPrimaryKey primary key
     * @return true if update was successful, false otherwise
     */
    public boolean updateTowerInfoData(int givenLevelId, int givenTowerId, int givenX,
    		int givenY, int givenTowerState, int givenPrimaryKey) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(TOWER_INFO_DATA_TABLE_KEY_X, givenX);
    	givenArguments.put(TOWER_INFO_DATA_TABLE_KEY_Y, givenY);
    	givenArguments.put(TOWER_INFO_DATA_TABLE_KEY_TOWER_STATE, givenTowerState);
    	return database.update(TOWER_INFO_DATA_TABLE, givenArguments,
    			TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + TOWER_INFO_DATA_TABLE_KEY_TOWER_ID + " = "
    			+ givenTowerId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Update tower state for the tower with the given level id, tower id, and
     * primary key
     * 
     * @param givenLevelId given level id
     * @param givenTowerId given tower id
     * @param givenPrimaryKey given primary key
     * @param givenTowerState tower state
     * @return true if update successful, false otherwise
     */
    public boolean updateTowerLevel(int givenLevelId, int givenTowerId,
    		int givenPrimaryKey, int givenTowerState) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(TOWER_INFO_DATA_TABLE_KEY_TOWER_STATE, givenTowerState);
    	return database.update(TOWER_INFO_DATA_TABLE, givenArguments,
    			TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + TOWER_INFO_DATA_TABLE_KEY_TOWER_ID + " = "
    			+ givenTowerId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Delete tower information for the tower with the given level id, tower id, 
     * and primary key
     * 
     * @param givenLevelId given level id
     * @param givenTowerId given tower id
     * @param givenPrimaryKey given primary key
     * @return true if deletion successful, false otherwise
     */
    public boolean deleteTowerInfoData(int givenLevelId, int givenTowerId, int givenPrimaryKey) {
    	return database.delete(TOWER_INFO_DATA_TABLE, TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID
    			+ " = " + givenLevelId + " AND " + TOWER_INFO_DATA_TABLE_KEY_TOWER_ID + " = "
    			+ givenTowerId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Delete tower information for the towers with the given level id
     * 
     * @param givenLevelId given level it
     * @return true if deletion successful, false otherwise
     */
    public boolean deleteTowerInfoData(int givenLevelId) {
    	return database.delete(TOWER_INFO_DATA_TABLE, TOWER_INFO_DATA_TABLE_KEY_LEVEL_ID
    			+ " = " + givenLevelId, null) > 0;
    }
    
    /**
     * Delete all tower information
     * 
     * @return true if deletion successful, false otherwise
     */
    public boolean deleteAllTowerInfoData() {
    	return database.delete(TOWER_INFO_DATA_TABLE, null, null) > 0;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////
    // bugInfoDataTable CRUD

    /**
     * Create bug information for a bug with the given level id and bug id
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenX x location
     * @param givenY y location
     * @param givenNodeIndex given node index
     * @param givenXSpeedScalar x speed scalar
     * @param givenYSpeedScalar y speed scalar
     * @param givenHealth given health
     * @param givenCurrentBitmapFrame current bitmap frame
     * @return the primary key of the newly created bug information, or -1 if failed
     */
    public long createBugInfoData(int givenLevelId, int givenBugId, int givenX,
    		int givenY, int givenNodeIndex, double givenXSpeedScalar, double givenYSpeedScalar,
    		int givenHealth, int givenCurrentBitmapFrame, int givenPathIndex) {
    	ContentValues initialValues = new ContentValues();
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_LEVEL_ID, givenLevelId);
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_BUG_ID, givenBugId);
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_X, givenX);
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_Y, givenY);
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_NODE_INDEX, givenNodeIndex);
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_X_SPEED_SCALAR, givenXSpeedScalar);
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_Y_SPEED_SCALAR, givenYSpeedScalar);
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_HEALTH, givenHealth);
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_CURRENT_BITMAP_FRAME, givenCurrentBitmapFrame);
    	initialValues.put(BUG_INFO_DATA_TABLE_KEY_PATH_INDEX, givenPathIndex);
    	return database.insert(BUG_INFO_DATA_TABLE, null, initialValues);
    }
    
    /**
     * Return a Cursor positioned at the bug information for the bug with given
     * level id, given bug id, and given primary key
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenPrimaryKey given primary key
     * @return a Cursor positioned at the bug information, if found
     * @throws SQLException if bug information could not be found/retrieved
     */
    public Cursor readBugInfoData(int givenLevelId, int givenBugId, int givenPrimaryKey) throws SQLException {    	
    	Cursor cursor = database.query(true,
    					BUG_INFO_DATA_TABLE,
    					BUT_INFO_DATA_TABLE_COLUMNS,
    					BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId + " AND " +
    					BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = " + givenBugId + " AND " +
    					KEY_ID + " = " + givenPrimaryKey,
					    null, null, null, null, null);
    	if(null != cursor) {
    		cursor.moveToFirst();
    	}
    	return cursor;
    }
    
    /**
     * Return a Cursor positioned at the bug information for the bugs at the
     * given level id
     * 
     * @param givenLevelId given level id
     * @return a Cursor positioned at the bug information, if found
     * @throws SQLException if bug information could not be found/retrieved
     */
    public Cursor readBugInfoData(int givenLevelId) throws SQLException {    	
    	Cursor cursor = database.query(true,
    					BUG_INFO_DATA_TABLE,
    					BUT_INFO_DATA_TABLE_COLUMNS,
    					BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId,
					    null, null, null, null, null);
    	if(null != cursor) {
    		cursor.moveToFirst();
    		System.out.println("Cursor not null");
    	} else {
    		System.out.println("Cursor null");
    	}
    	
    	return cursor;
    }

    /**
     * Update bug information for the bug with the given level id, given bug id,
     * and given primary key
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenX x location
     * @param givenY y location
     * @param givenNodeIndex given node index
     * @param givenXSpeedScalar x speed scalar
     * @param givenYSpeedScalar y speed scalar
     * @param givenHealth given health
     * @param givenCurrentBitmapFrame given current bitmap frame
     * @param givenPrimaryKey given primary key
     * @return true if update was successful, false otherwise
     */
    public boolean updateBugInfoData(int givenLevelId, int givenBugId, int givenX,
    		int givenY, int givenNodeIndex, double givenXSpeedScalar,
    		double givenYSpeedScalar, int givenHealth, int givenCurrentBitmapFrame,
    		int givenPrimaryKey, int givenPathIndex) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_X, givenX);
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_Y, givenY);
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_NODE_INDEX, givenNodeIndex);
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_X_SPEED_SCALAR, givenXSpeedScalar);
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_Y_SPEED_SCALAR, givenYSpeedScalar);
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_HEALTH, givenHealth);
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_PATH_INDEX, givenPathIndex);
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_CURRENT_BITMAP_FRAME, givenCurrentBitmapFrame);
    	
    	return database.update(BUG_INFO_DATA_TABLE, givenArguments,
    			BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = "
    			+ givenBugId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }

    /**
     * Update x position for the bug with the given level id, bug id, and primary key.
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenPrimaryKey given primary key
     * @param givenX x location
     * @return true if update successful, false otherwise
     */
    public boolean updateBugInfoX(int givenLevelId, int givenBugId,
    		int givenPrimaryKey, int givenX) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_X, givenX);
    	return database.update(BUG_INFO_DATA_TABLE, givenArguments,
    			BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = "
    			+ givenBugId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Update x location for the bug with the given level id, bug id, 
     * and primary key.
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenPrimaryKey given primary key
     * @param givenY y location
     * @return true if update successful, false otherwise
     */
    public boolean updateBugInfoY(int givenLevelId, int givenBugId,
    		int givenPrimaryKey, int givenY) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_Y, givenY);
    	return database.update(BUG_INFO_DATA_TABLE, givenArguments,
    			BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = "
    			+ givenBugId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Update bug health for the bug with the given level id, bug id, 
     * and primary key
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenPrimaryKey given primary key
     * @param givenHealth given health
     * @return true if update successful, false otherwise
     */
    public boolean updateBugHealth(int givenLevelId, int givenBugId,
    		int givenPrimaryKey, int givenHealth) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_HEALTH, givenHealth);
    	return database.update(BUG_INFO_DATA_TABLE, givenArguments,
    			BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = "
    			+ givenBugId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Update current bitmap frame for the bug with the given level id, bug id,
     * and primary key
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenPrimaryKey given primary key
     * @param givenCurrentBitmapFrame current bitmap frame
     * @return true if update successful, false otherwise
     */
    public boolean updateCurrentBitmapFrame(int givenLevelId, int givenBugId,
    		int givenPrimaryKey, int givenCurrentBitmapFrame) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_CURRENT_BITMAP_FRAME, givenCurrentBitmapFrame);
    	return database.update(BUG_INFO_DATA_TABLE, givenArguments,
    			BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = "
    			+ givenBugId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Update the current node index for the bug with the given level id, bug id,
     * and primary key
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenPrimaryKey given primary key
     * @param givenCurrentNodeIndex current node index
     * @return true if the update was successful, false otherwise
     */
    public boolean updateCurrentNodeIndex(int givenLevelId, int givenBugId,
    		int givenPrimaryKey, int givenCurrentNodeIndex) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_NODE_INDEX, givenCurrentNodeIndex);
    	return database.update(BUG_INFO_DATA_TABLE, givenArguments,
    			BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = "
    			+ givenBugId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Update the x speed scalar for the bug with the given bug id, level id,
     * and primary key
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenPrimaryKey given primary key
     * @param givenXSpeedScalar x speed scalar
     * @return true if the update was successful, false otherwise
     */
    public boolean updateXSpeedScalar(int givenLevelId, int givenBugId,
    		int givenPrimaryKey, double givenXSpeedScalar) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_X_SPEED_SCALAR, givenXSpeedScalar);
    	return database.update(BUG_INFO_DATA_TABLE, givenArguments,
    			BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = "
    			+ givenBugId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Update the y speed scalar for the bug with the given bug id, level id,
     * and primary key
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenPrimaryKey given primary key
     * @param givenYSpeedScalar y speed scalar
     * @return true if the update was successful, false otherwise
     */
    public boolean updateYSpeedScalar(int givenLevelId, int givenBugId,
    		int givenPrimaryKey, double givenYSpeedScalar) {
    	ContentValues givenArguments = new ContentValues();
    	givenArguments.put(BUG_INFO_DATA_TABLE_KEY_Y_SPEED_SCALAR, givenYSpeedScalar);
    	return database.update(BUG_INFO_DATA_TABLE, givenArguments,
    			BUG_INFO_DATA_TABLE_KEY_LEVEL_ID + " = " + givenLevelId
    			+ " AND " + BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = "
    			+ givenBugId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Delete bug information for the bug with the given level id, bug id, and
     * primary key
     * 
     * @param givenLevelId given level id
     * @param givenBugId given bug id
     * @param givenPrimaryKey given primary key
     * @return true if deletion was successful, false otherwise
     */
    public boolean deleteBugInfoData(int givenLevelId, int givenBugId, int givenPrimaryKey) {
    	return database.delete(BUG_INFO_DATA_TABLE, BUG_INFO_DATA_TABLE_KEY_LEVEL_ID
    			+ " = " + givenLevelId + " AND " + BUG_INFO_DATA_TABLE_KEY_BUG_ID + " = "
    			+ givenBugId + " AND " + KEY_ID + " = " + givenPrimaryKey, null) > 0;
    }
    
    /**
     * Delete bug information for the bugs at the given level id
     * 
     * @param givenLevelId given level id
     * @return true if deletion successful, false otherwise
     */
    public boolean deleteBugInfoData(int givenLevelId) {
    	return database.delete(BUG_INFO_DATA_TABLE, BUG_INFO_DATA_TABLE_KEY_LEVEL_ID
    			+ " = " + givenLevelId, null) > 0;
    }
    
    /**
     * Delete all bug information
     * 
     * @return true if deletion successful, false otherwise
     */
    public boolean deleteAllBugInfoData() {
    	return database.delete(BUG_INFO_DATA_TABLE, null, null) > 0;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    // DatabaseHelper
    
	/**
	 * A helper class to manage database creation and version management.
	 * 
	 * @author colegroff
	 */
    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
        	
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            
        }

        @Override
        public void onCreate(SQLiteDatabase givenDatabase) {

        	givenDatabase.execSQL(CREATE_MAP_INFO_DATA_TABLE); // Create .
        	givenDatabase.execSQL(CREATE_STAR_DATA_TABLE);	// Create .
        	givenDatabase.execSQL(CREATE_USER_DATA_TABLE);	// Create .
        	givenDatabase.execSQL(CREATE_TOWER_DATA_TABLE); // Create .
        	givenDatabase.execSQL(CREATE_TOWER_INFO_DATA_TABLE); // Create .
        	givenDatabase.execSQL(CREATE_BUG_INFO_DATA_TABLE);
            
        }

		@Override
		public void onUpgrade(SQLiteDatabase givenDatabase, int oldVersion, int newVersion) {

			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
			// givenDatabase.execSQL("DROP TABLE IF EXISTS notes"); DROPS/ALTERS
            // onCreate(givenDatabase);
            
		}

    }
    
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
	
}

