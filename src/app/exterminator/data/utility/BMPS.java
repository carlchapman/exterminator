package app.exterminator.data.utility;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import app.exterminator.R;
import app.exterminator.GUI.game.Bug;
import app.exterminator.data.structures.MapInfo;

/**
 * The Class BMPS is a place to hold most of the bitmaps needed by various
 * objects when they are drawn. The idea is to statically initialize them once
 * so that they are never created again, thus preventing any memory leaks.
 * 
 * @author carlchapman
 */
public class BMPS {

	// for screens:

	/** The screen backgrounds. */
	private static Bitmap[] screenBackgrounds;

	/** The Constant SCREEN_RESOURCES. */
	private static final int[] SCREEN_RESOURCES = {
			R.drawable.s_market_screen_portrait,
			R.drawable.s_market_screen_landscape };

	/** The Constant STAR_VIEW_PORTRAIT. */
	public static final int STAR_VIEW_PORTRAIT = 0;

	/** The Constant STAR_VIEW_LANDSCAPE. */
	public static final int STAR_VIEW_LANDSCAPE = 1;

	// for icons:

	/** The icons. */
	private static Bitmap[] icons;

	/** The Constant numberOfIcons. */
	private static final int numberOfIcons = 13;

	/** The Constant BUGBUCK. */
	public static final int BUGBUCK = 0;

	/** The Constant COIN. */
	public static final int COIN = 1;

	/** The Constant HEART. */
	public static final int HEART = 2;

	/** The Constant STAR. */
	public static final int STAR = 3;

	/** The Constant GREYSTAR. */
	public static final int GREYSTAR = 4;

	/** The Constant MARKET. */
	public static final int MARKET = 5;

	/** The Constant SLOW. */
	public static final int SLOW = 6;

	/** The Constant MEDIUM. */
	public static final int MEDIUM = 7;

	/** The Constant FAST. */
	public static final int FAST = 8;

	/** The Constant PAUSE. */
	public static final int PAUSE = 9;

	/** The Constant PLAY. */
	public static final int PLAY = 10;
	
	/** The Constant UP_GREY. */
	public static final int UP_GREY = 11;
	
	/** The Constant UPUP_GREY. */
	public static final int UPUP_GREY = 12;

	/** The Constant ICON_RESOURCES. */
	private static final int[] ICON_RESOURCES = { R.drawable.i_bugbuck,
			R.drawable.i_coin, R.drawable.i_heart, R.drawable.i_yellowstar,
			R.drawable.i_greystar, R.drawable.i_market, R.drawable.i_slow,
			R.drawable.i_medium, R.drawable.i_fast, R.drawable.i_pause,
			R.drawable.i_play,R.drawable.i_up_grey,R.drawable.i_upup_grey };

	// for maps

	/** The maps. */
	private static Bitmap[] maps;

	/** The Constant MAP_RESOURCES. */
	public static final int[] MAP_RESOURCES = { R.drawable.m_green_grass,
			R.drawable.m_brown_dirt, R.drawable.m_blue_fabric,
			R.drawable.m_orange_honeycomb, R.drawable.m_pink_silk,
			R.drawable.m_purple_plastic, R.drawable.m_red_brick,
			R.drawable.m_white_paint, R.drawable.m_yellow_asphault,
			R.drawable.m_teal_metal, R.drawable.m_grey_stone,
			R.drawable.m_gold_woodchips, R.drawable.m_slate_tile,
			R.drawable.m_tan_sand };

	/**
	 * The next map index - note that the maps are the largest bitmaps and they
	 * take much longer to initialize, so they are initialized one at a time to
	 * help in showing a ProgressBar that reflects the time left to finish
	 * loading. This variable is used to keep track of which map to initialize
	 * next during that process.
	 */
	private static int nextMapIndex = 0;
	
	// for random number getting
	/** The gen. */
	private static Random gen;

	/**
	 * Initializes the icons.
	 * 
	 * @param context
	 *            the context
	 */
	public static void initializeIcons(Context context) {
		icons = new Bitmap[numberOfIcons];
		for (int i = 0; i < numberOfIcons; i++) {
			icons[i] = BitmapFactory.decodeResource(context.getResources(),
					ICON_RESOURCES[i]);
		}

	}

	/**
	 * Initializes screen backgrounds and the random number Generator.
	 * 
	 * @param context
	 *            the context
	 */
	public static void initializeScreenBackgrounds(Context context) {
		gen = new Random(System.currentTimeMillis());
		screenBackgrounds = new Bitmap[SCREEN_RESOURCES.length];
		for (int i = 0; i < SCREEN_RESOURCES.length; i++) {
			screenBackgrounds[i] = BitmapFactory.decodeResource(
					context.getResources(), SCREEN_RESOURCES[i]);
		}
	}

	/**
	 * Gets the next map index.
	 * 
	 * @return the next map index
	 */
	public static int getNextMapIndex() {
		return nextMapIndex;
	}

	/**
	 * This method initializes all of the maps of the game during loading, one
	 * at a time so that the progress bar can reflect its progress. It is done
	 * this way because maps are by far the slowest thing to load.
	 * 
	 * Note that it is possible for an Android device to be interrupted during
	 * loading. If that happens, the nextMapIndex should be set to the first
	 * null map image, allowing the loader to skip over maps that have already
	 * been initialized
	 * 
	 * @param context
	 *            the context
	 * @return the nextMapIndex after it has been incremented - this informs a
	 *         loop that breaks when all maps have been initialized
	 */
	public static int initializeNextMap(Context context) {
		if (nextMapIndex == 0)
			maps = new Bitmap[MapInfo.NUMBER_OF_MAPS];

		if (nextMapIndex < MAP_RESOURCES.length) {
			maps[nextMapIndex] = BitmapFactory.decodeResource(
					context.getResources(), MAP_RESOURCES[nextMapIndex]);
			System.out.println("nmi: "+nextMapIndex);
			maps[nextMapIndex] = Bitmap.createScaledBitmap(maps[nextMapIndex],
					850, 400, false);
		}
		return ++nextMapIndex;
	}

	/**
	 * Gets the desired background image.
	 * 
	 * @param identifier
	 *            the identifier
	 * @return the background
	 */
	public static Bitmap getBackground(int identifier) {
		return screenBackgrounds[identifier];
	}

	/**
	 * Gets the map.
	 * 
	 * @param levelID
	 *            the level id
	 * @return the map
	 */
	public static Bitmap getMap(int levelID) {
		return maps[levelID];
	}
	
	/**
	 * Gets a random number generator.
	 * 
	 * @return the Random generator
	 */
	public static Random getGen() {
		return gen;
	}

	/**
	 * Gets the icon.
	 * 
	 * @param identifier
	 *            the identifier
	 * @return the icon
	 */
	public static Bitmap getIcon(int identifier) {
		return icons[identifier];
	}

	/**
	 * Returns true if all bitmaps are not null.
	 * 
	 * @return true if all bitmaps are not null
	 */
	public static boolean areSetup() {
		boolean mapsAreSetup = setupMapIndex();
		if (null == screenBackgrounds || null == icons)
			return false;
		for (int i = 0; i < ICON_RESOURCES.length; i++)
			if (null == getIcon(i))
				return false;
		for (int i = 0; i < SCREEN_RESOURCES.length; i++)
			if (null == getBackground(i))
				return false;
		return mapsAreSetup && Bug.areSetup();
	}

	/**
	 * Sets up map index to be the first null map in the array of map Bitmaps.
	 * 
	 * @return true, if all of the maps have been initialized
	 */
	private static boolean setupMapIndex() {
		if (null == maps) {
			nextMapIndex = 0;
			return false;
		} else {
			for (int i = 0; i < BMPS.MAP_RESOURCES.length; i++) {
				if (null == BMPS.getMap(i)) {
					nextMapIndex = i;
					return false;
				}
			}
		}
		return true;
	}

}
