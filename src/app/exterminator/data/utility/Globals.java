package app.exterminator.data.utility;

import android.app.Application;
import app.exterminator.data.facades.DatabaseAdapter;
import app.exterminator.data.facades.Player;

/**
 * The Class Globals extends Application, which makes it available from the
 * getApplicationContext() method within any Activity. This allows the storage
 * and retrieval of global variables.
 * 
 * @author carlchapman
 */
public class Globals extends Application {

	/** The player. */
	private Player player;

	/** The db. */
	private DatabaseAdapter db;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Application#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();
	}

	/**
	 * Sets the player.
	 * 
	 * @param newPlayer
	 *            the new player
	 */
	public void setPlayer(Player newPlayer) {
		this.player = newPlayer;
	}

	/**
	 * Gets the player.
	 * 
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Gets the database adapter.
	 * 
	 * @return the database adapter
	 */
	public DatabaseAdapter getDatabaseAdapter() {
		return db;
	}

	/**
	 * Sets the database adapter.
	 * 
	 * @param db
	 *            the new database adapter
	 */
	public void setDatabaseAdapter(DatabaseAdapter db) {
		this.db = db;
	}

}
