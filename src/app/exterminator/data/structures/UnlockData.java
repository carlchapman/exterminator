package app.exterminator.data.structures;

import app.exterminator.data.facades.DatabaseAdapter;
import app.exterminator.sync.update.ToXML;

/**
 * The Class UnlockData tracks what towers have been unlocked and to what
 * extent. The default state is 0, but can go up to 1 or 2.
 * 
 * @author carlchapman modified by Cole Groff
 * 
 */
public class UnlockData implements ToXML {

	/** The Constant NUMBER_OF_TOWERS. */
	public static final int NUMBER_OF_TOWERS = 3;
	
	public static final int DEFAULT = 0;

	public static final int UPGRADED = 1;

	public static final int MAXUPPED = 2;

	/** The tower id. */
	private int towerID;

	/** The tower state. */
	private int towerState;

	// DatabaseAdapter to local database.
	/** The db. */
	private DatabaseAdapter db;

	/**
	 * Instantiates a new unlock data.
	 * 
	 * @param towerID
	 *            the tower id
	 * @param db
	 *            the db
	 */
	public UnlockData(int towerID, DatabaseAdapter db) {
		this.db = db;
		this.towerID = towerID;
		towerState = 0;
	}

	/**
	 * Instantiates a new unlock data.
	 * 
	 * @param towerID
	 *            the tower id
	 * @param towerState
	 *            the tower state
	 */
	public UnlockData(int towerID, int towerState) {
		this.towerID = towerID;
		this.towerState = towerState;
	}

	/**
	 * Instantiates a new unlock data.
	 * 
	 * @param towerID
	 *            the tower id
	 * @param towerState
	 *            the tower state
	 * @param db
	 *            the db
	 */
	public UnlockData(int towerID, int towerState, DatabaseAdapter db) {
		this.db = db;
		this.towerID = towerID;
		this.towerState = towerState;
		// db.createTowerData(towerID, towerState);
	}

	/**
	 * Gets the tower id.
	 * 
	 * @return the tower id
	 */
	public int getTowerID() {
		return towerID;
	}

	/**
	 * Checks if is unlocked.
	 * 
	 * @return true, if is unlocked
	 */
	public boolean isUnlocked() {
		return towerState > 0;
	}

	/**
	 * Unlock.
	 */
	public void unlock() {
		if (towerState != 2) {
			towerState = 1;
			db.updateTowerData(towerID, towerState);
		}
	}

	/**
	 * Checks if is max upped.
	 * 
	 * @return true, if is max upped
	 */
	public boolean isMaxUpped() {
		return towerState == 2;
	}

	/**
	 * Max up.
	 */
	public void maxUp() {
		towerState = 2;
		db.updateTowerData(towerID, towerState);
	}

	public void updateTower(int towerState) {
		this.towerState = towerState;
	}

	public void reset() {
		towerState = 0;
		db.updateTowerData(towerID, 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 3;
		result = prime * result + towerID;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnlockData other = (UnlockData) obj;
		if (towerID != other.towerID)
			return false;
		if (towerState != other.towerState)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.ToXML#toXML()
	 */
	@Override
	public String toXML() {
		return "<towerID>" + towerID + "</towerID><towerState>" + towerState
				+ "</towerState>";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TowerData [towerID=" + towerID + ", towerState=" + towerState
				+ "]";
	}

}
