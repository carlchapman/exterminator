package app.exterminator.data.structures;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import app.exterminator.GUI.game.Bug;
import app.exterminator.GUI.game.Tower;
import app.exterminator.data.facades.DatabaseAdapter;

/**
 * The Class MapInfo holds information needed to recreate the saved state of a
 * level.
 * 
 * 
 * @author carlchapman modified by Cole Groff
 */
public class MapInfo {

	// DatabaseAdapter to local database.
	/** The db. */
	private DatabaseAdapter db;
 
	/** The Constant NUMBER_OF_MAPS. */
	public static final int NUMBER_OF_MAPS = 14;

	/** The Constant START_HEALTH. */
	public static final int[] START_HEALTH = { 40, 40, 50, 100, 100, 130,
			100, 100, 100, 120, 100, 200, 222, 100 };

	/** The Constant START_COINS. */
	public static final int[] START_COINS = { 120, 400, 250, 320, 300, 160,
			300, 300, 300, 450, 300, 300, 300, 760 };

	/** The level id. */
	private int levelID;

	/** The player health. */
	private int playerHealth;

	/** The current wave. */
	private int currentWave;

	/** The number of coins that the player has in this map. */
	private int coins;

	/** The current wave time. */
	private int currentWaveTime;

	/** The tower info. */
	private ArrayList<TowerInfo> towerInfo;

	/** The bug info. */
	private ArrayList<BugInfo> bugInfo;

	/** The was saved. */
	private boolean wasSaved;

	/**
	 * Instantiates a new map info.
	 * 
	 * @param levelID
	 *            the level id
	 * @param db
	 *            the db
	 */
	public MapInfo(int levelID, DatabaseAdapter db) {
		this(levelID, 0, START_HEALTH[levelID], START_COINS[levelID],
				new ArrayList<TowerInfo>(), new ArrayList<BugInfo>(), false, 0,
				db);
	}

	/**
	 * Instantiates a new map info.
	 * 
	 * @param levelID
	 *            the level id
	 * @param currentWave
	 *            the current wave
	 * @param playerHealth
	 *            the player health
	 * @param startCoins
	 *            the starting number of coins
	 * @param towerInfo
	 *            the tower info
	 * @param bugInfo
	 *            the bug info
	 * @param wasSaved
	 *            the was saved
	 * @param currentWaveTime
	 *            the current wave time
	 * @param db
	 *            the db
	 */
	public MapInfo(int levelID, int currentWave, int playerHealth,
			int startCoins, ArrayList<TowerInfo> towerInfo,
			ArrayList<BugInfo> bugInfo, boolean wasSaved, int currentWaveTime,
			DatabaseAdapter db) {
		this.levelID = levelID;
		this.playerHealth = playerHealth;
		this.coins = startCoins;
		this.currentWave = currentWave;
		this.towerInfo = towerInfo;
		this.bugInfo = bugInfo;
		this.wasSaved = wasSaved;
		this.currentWaveTime = currentWaveTime;
		this.db = db;
	}

	/**
	 * Gets the level id.
	 * 
	 * @return the level id
	 */
	public int getLevelID() {
		return levelID;
	}

	/**
	 * Was saved.
	 * 
	 * @return true, if this was saved
	 */
	public boolean wasSaved() {
		return wasSaved;
	}

	/**
	 * Gets the player health.
	 * 
	 * @return the player health
	 */
	public int getPlayerHealth() {
		return playerHealth;
	}

	/**
	 * Adds n coins to the player in this map.
	 * 
	 * @param n
	 *            the number of coins to add
	 */
	public void addCoins(int n) {
		coins += n;
		db.updateMapInfoCoins(levelID, coins);
	}

	/**
	 * Subtract n coins from the player in this map.
	 * 
	 * @param n
	 *            the number of coins to subtract
	 */
	public void subtractCoins(int n) {
		//System.out.println("subtracting "+ n + " coins");
		coins -= n;
		db.updateMapInfoCoins(levelID, coins);
	}
	
	/**
	 * Gets the number of coins.
	 * 
	 * @return the number of coins
	 */
	public int getCoins() {
		return coins;
	}
	/**
	 * Sets the number of coins - should only be used at startup.
	 * 
	 * @param coins
	 *            the new value for coins
	 */
	public void setCoins(int coins) {
		this.coins = coins;
	}

	/**
	 * Checks if is dead.
	 * 
	 * @return true, if the player is dead in this level
	 */
	public boolean isDead() {
		return playerHealth < 1;
	}

	/**
	 * Reduce player health.
	 * 
	 * @param damage
	 *            the damage
	 */
	public void reducePlayerHealth(int damage) {
		this.playerHealth -= damage;
		db.updatePlayerHealth(levelID, playerHealth);
	}

	/**
	 * Gets the current wave.
	 * 
	 * @return the current wave
	 */
	public int getCurrentWave() {
		return currentWave;
	}

	/**
	 * Increment current wave.
	 */
	public void incrementCurrentWave() {
		currentWave++;
		db.updateCurrentWave(levelID, currentWave);
	}

	/**
	 * Gets the tower info.
	 * 
	 * @return the tower info
	 */
	public ArrayList<TowerInfo> getTowerInfo() {
		return towerInfo;
	}

	/**
	 * Gets the bug info.
	 * 
	 * @return the bug info
	 */
	public ArrayList<BugInfo> getBugInfo() {
		return bugInfo;
	}

	/**
	 * Resets the values to their default values. This is a very critical
	 * method, because if the currentWave is not reset to zero, an
	 * IndexOutOfBounds Exception can easily occur when this level is reloaded
	 * after being completed once.
	 */
	public void reset() {
		this.playerHealth = START_HEALTH[levelID];
		this.coins = START_COINS[levelID];
		this.currentWave = 0;
		this.towerInfo.clear();
		db.deleteTowerInfoData(levelID);
		this.bugInfo.clear();
		db.deleteBugInfoData(levelID);
		this.wasSaved = false;
		this.currentWaveTime = 0;
		db.updateMapInfoData(levelID, playerHealth, currentWave, 0, wasSaved, coins);
	}

	/**
	 * Saves the current state of all bugs and towers on the map, the current
	 * wave time and then writes all of this to the local database.
	 * 
	 * @param bugs
	 *            the bugs to remember
	 * @param towers
	 *            the towers to remember
	 * @param currentWaveTime
	 *            the current wave time
	 */
	public void save(CopyOnWriteArrayList<Bug> bugs,
			CopyOnWriteArrayList<Tower> towers, int currentWaveTime) {

		db.deleteBugInfoData(levelID);
		db.deleteTowerInfoData(levelID);

		ArrayList<TowerInfo> newTowerInfo = new ArrayList<TowerInfo>();
		ArrayList<BugInfo> newBugInfo = new ArrayList<BugInfo>();
		for (Bug bug : bugs)
			newBugInfo.add(bug.getInfoCopy());
		for (Tower tower : towers)
			newTowerInfo.add(tower.getInfoCopy());

		this.towerInfo = newTowerInfo;
		this.bugInfo = newBugInfo;
		this.wasSaved = true;
		this.currentWaveTime = currentWaveTime;

		// System.out.println("////////////  save() ////////////////");
		// System.out.println("levelID: " + levelID);
		// System.out.println("playerHealth: " + playerHealth);
		// System.out.println("currentWaveTime: " + currentWaveTime);
		// System.out.println("currentWave: " + currentWave);
		// System.out.println("wasSaved: " + wasSaved);

		//TODO - put coins in here
		db.updateMapInfoData(levelID, playerHealth, currentWave,
				currentWaveTime, wasSaved, coins);

	}

	/**
	 * Gets the current wave time.
	 * 
	 * @return the current wave time
	 */
	public int getCurrentWaveTime() {
		return currentWaveTime;
	}

}
