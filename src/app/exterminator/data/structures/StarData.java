package app.exterminator.data.structures;

import java.util.Arrays;

import app.exterminator.data.facades.DatabaseAdapter;
import app.exterminator.sync.update.ToXML;

/**
 * The Class StarData holds data about the state of stars earned for each level.
 * 
 * @author carlchapman modified by Cole Groff
 * 
 */
public class StarData implements ToXML {

	// DatabaseAdapter to local database.
	/** The db. */
	private DatabaseAdapter db;

	/** The level id. */
	private int levelID;

	/** The array of star data, where 0 is no star, 1 is a star. */
	private int star[];

	// for use by the UpdateStar method, the whichStar field
	/** The Constant LEFT. */
	public static final int LEFT = 0;

	/** The Constant CENTER. */
	public static final int CENTER = 1;

	/** The Constant RIGHT. */
	public static final int RIGHT = 2;

	/**
	 * Instantiates a new star data with all stars initialized to zero.
	 * 
	 * @param levelID
	 *            the level id
	 * @param db
	 *            the db
	 */
	public StarData(int levelID, DatabaseAdapter db) {
		this.levelID = levelID;
		star = new int[3];
		for (int i = 0; i < 3; i++)
			star[i] = 0;
		this.db = db;
	}

	/**
	 * Instantiates a new star data from the given values without using the db
	 * adapter - for use by UpdateMap when quickly creating temporary updates
	 * from a server response to compare with it's own contents to verify that
	 * all Updates have reached the server.
	 * 
	 * @param levelID
	 *            the level id
	 * @param s1
	 *            the s1
	 * @param s2
	 *            the s2
	 * @param s3
	 *            the s3
	 */
	public StarData(int levelID, int s1, int s2, int s3) {
		this.levelID = levelID;
		star = new int[3];
		star[LEFT] = s1;
		star[CENTER] = s2;
		star[RIGHT] = s3;
		// does not use db adapter - only for testing equality in UpdateMap
	}

	/**
	 * Instantiates a new star data from the given values.
	 * 
	 * @param levelID
	 *            the level id
	 * @param s1
	 *            the s1
	 * @param s2
	 *            the s2
	 * @param s3
	 *            the s3
	 * @param db
	 *            the db
	 */
	public StarData(int levelID, int s1, int s2, int s3, DatabaseAdapter db) {
		this.levelID = levelID;
		this.db = db;
		star = new int[3];
		star[LEFT] = s1;
		star[CENTER] = s2;
		star[RIGHT] = s3;
	}

	/**
	 * Gets the level id.
	 * 
	 * @return the level id
	 */
	public int getLevelID() {
		return levelID;
	}

	/**
	 * Gets the star value.
	 * 
	 * @param whichStar
	 *            the star to get the value of - should be one of StarData.LEFT,
	 *            StarData.CENTER, StarData.RIGHT
	 * @return the star value
	 */
	public int getStarValue(int whichStar) {
		return star[whichStar];
	}

	/**
	 * Update star.
	 * 
	 * @param whichStar
	 *            the star to set the state of - should be one of StarData.LEFT,
	 *            StarData.CENTER, StarData.RIGHT
	 * @param newState
	 *            the new state of the star
	 */
	public void updateStar(int whichStar, int newState) {
		// System.out.println("updating star number "+whichStar+" to state: "+newState);
		// if (db == null)
		// System.out.println("db is null");
		// else
		// System.out.println("db is ok");
		star[whichStar] = newState;
		db.updateStarData(levelID, whichStar, newState);
	}

	/**
	 * Gets the array of integers representing the star states.
	 * 
	 * @return the array of integers representing the star states.
	 */
	public int[] getStars() {
		return star;
	}
	
	public void reset(){
		star = new int[3];
		for (int i = 0; i < 3; i++){
			star[i] = 0;
			db.updateStarData(levelID,i,0);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + levelID;
		result = prime * result + Arrays.hashCode(star);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StarData other = (StarData) obj;
		if (levelID != other.levelID)
			return false;
		if (!Arrays.equals(star, other.star))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.exterminator.sync.update.ToXML#toXML()
	 */
	public String toXML() {
		return "<levelID>" + levelID + "</levelID><S1>" + star[LEFT]
				+ "</S1><S2>" + star[CENTER] + "</S2><S3>" + star[RIGHT]
				+ "</S3>";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LevelData [levelID=" + levelID + ", star="
				+ Arrays.toString(star) + "]";
	}

}
