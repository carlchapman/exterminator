
package app.exterminator.data.structures;

import app.exterminator.data.facades.DatabaseAdapter;

/**
 * The Class BugInfo contains information about the current state of a bug. The
 * idea is to be able to use this information to recreate the bug in action on a
 * map, doing what it was doing when this information was gathered.
 * 
 * 
 * @author carlchapman, modified by Cole Groff
 */
public class BugInfo {

	/** The Constant EPSILON. */
	private static final double EPSILON = 0.001;

	/** The Constant LEFT. */
	public static final int LEFT = 0;

	/** The Constant RIGHT. */
	public static final int RIGHT = 1;

	/** The Constant UP. */
	public static final int UP = 2;

	/** The Constant DOWN. */
	public static final int DOWN = 3;

	/** The DatabaseAdapter to local database. */
	private DatabaseAdapter db;

	/** The primary key of this bug in the local database. */
	private int primaryKey;

	/** The level id where the bug is. */
	private int levelID;

	/** The bug id, used to recreate this bug's bug-specific constants. */
	private int bugID;

	/** The x location. */
	private int x;

	/** The y location. */
	private int y;

	/** The index of the bugpath node that this bug is going towards. */
	private int nodeIndex;

	/** The x speed scalar - controls direction and relative speed. */
	private double xSpeedScalar;

	/** The y speed scalar - controls direction and relative speed. */
	private double ySpeedScalar;

	/** The health of the bug. */
	private int health;

	/** The current bitmap frame - used by the animation. */
	private int currentBitmapFrame;
	
	/** the index of a map's paths to use to get this bug's path. */
	private int pathIndex;

	/**
	 * Instantiates a new bug info.
	 *
	 * @param bugID the bug id
	 * @param x the x location
	 * @param y the y location
	 * @param nodeIndex the node index
	 * @param xSpeedScalar the x speed scalar
	 * @param ySpeedScalar the y speed scalar
	 * @param health the health
	 * @param currentBitmapFrame the current bitmap frame
	 * @param pathIndex the path index
	 * @param db the db
	 * @param levelID the level id
	 */
	public BugInfo(int bugID, int x, int y, int nodeIndex, double xSpeedScalar,
			double ySpeedScalar, int health, int currentBitmapFrame, int pathIndex,
			DatabaseAdapter db, int levelID) {
		this.bugID = bugID;
		this.levelID = levelID;
		this.x = x;
		this.y = y;
		this.nodeIndex = nodeIndex;
		this.xSpeedScalar = xSpeedScalar;
		this.ySpeedScalar = ySpeedScalar;
		this.health = health;
		this.currentBitmapFrame = currentBitmapFrame;
		this.pathIndex = pathIndex;
		this.db = db;
	}

	/**
	 * Gets the bug id.
	 * 
	 * @return the bug id
	 */
	public int getBugID() {
		return bugID;
	}

	/**
	 * Gets the x location.
	 * 
	 * @return the x location
	 */
	public int getX() {
		return x;
	}

	/**
	 * Gets the y location.
	 * 
	 * @return the y location
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the x location.
	 * 
	 * @param x
	 *            the new x location
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Sets the y location.
	 * 
	 * @param y
	 *            the new y location
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Decrement health.
	 * 
	 * @param damage
	 *            the amount of damage to cause the bug
	 */
	public void decrementHealth(int damage) {
		this.health -= damage;
	}
	
	/**
	 * Increment health.
	 *
	 * @param amount the amount
	 */
	public void incrementHealth(int amount){
		this.health+=amount;
	}

	/**
	 * Increment current bitmap frame.
	 * 
	 * @param bmp_cols
	 *            the number of columns for this sprite bitmap
	 */
	public void incrementCurrentBitmapFrame(int bmp_cols) {
		this.currentBitmapFrame = ++currentBitmapFrame % bmp_cols;
	}
	
	/**
	 * Gets the path index.
	 *
	 * @return the path index
	 */
	public int getPathIndex(){
		return pathIndex;
	}

	/**
	 * Gets the node index.
	 * 
	 * @return the node index
	 */
	public int getNodeIndex() {
		return nodeIndex;
	}

	/**
	 * Sets the node index.
	 * 
	 * @param newNodeIndex
	 *            the new node index
	 */
	public void setNodeIndex(int newNodeIndex) {
		this.nodeIndex = newNodeIndex;
	}

	/**
	 * Checks if this bug is slowed.
	 * 
	 * @return true, if this bug is slowed
	 */
	public boolean isSlowed() {
		return Math.abs(xSpeedScalar) + EPSILON < 1
				&& Math.abs(ySpeedScalar) + EPSILON < 1;
	}

	/**
	 * Gets the x speed scalar.
	 * 
	 * @return the x speed scalar
	 */
	public double getxSpeedScalar() {
		return xSpeedScalar;
	}

	/**
	 * Sets the x speed scalar.
	 * 
	 * @param xSpeedScalar
	 *            the new x speed scalar
	 */
	public void setxSpeedScalar(double xSpeedScalar) {
		this.xSpeedScalar = xSpeedScalar;
	}

	/**
	 * Gets the y speed scalar.
	 * 
	 * @return the y speed scalar
	 */
	public double getySpeedScalar() {
		return ySpeedScalar;
	}

	/**
	 * Sets the y speed scalar.
	 * 
	 * @param ySpeedScalar
	 *            the new y speed scalar
	 */
	public void setySpeedScalar(double ySpeedScalar) {
		this.ySpeedScalar = ySpeedScalar;
	}

	/**
	 * Slow speed.
	 * 
	 * @param slowBy
	 *            the slow by amount
	 */
	public void slowSpeed(double slowBy) {
		xSpeedScalar *= slowBy;
		ySpeedScalar *= slowBy;
	}

	/**
	 * Resume speed.
	 */
	public void resumeSpeed() {

		// if going left
		if (xSpeedScalar < 0) {
			xSpeedScalar = -1.0;
			ySpeedScalar = 0.0;
			// if going right
		} else if (xSpeedScalar > 0) {
			xSpeedScalar = 1.0;
			ySpeedScalar = 0.0;
			// if going down
		} else if (ySpeedScalar > 0) {
			xSpeedScalar = 0.0;
			ySpeedScalar = 1.0;
			// if going up
		} else if (ySpeedScalar < 0) {
			xSpeedScalar = 0.0;
			ySpeedScalar = -1.0;
		} else
			System.err.println("bug seems to be going in no direction");
	}

	/**
	 * Go.
	 * 
	 * @param direction
	 *            the direction to go in - should be one of BugInfo.LEFT,
	 *            BugInfo.RIGHT, BugInfo.UP, BugInfo.DOWN
	 */
	public void go(int direction) {
		// if xscalar is zero or close enough, then the current 'scalar' is the
		// y scalar, else it is the x scalar - scalar will always be positive
		double scalar = Math.abs(xSpeedScalar) < EPSILON ? Math
				.abs(ySpeedScalar) : Math.abs(xSpeedScalar);

//		System.out.println("changing direction with scalar: " + scalar
//				+ " and direction: " + direction(direction));

		switch (direction) {
		case LEFT:
			xSpeedScalar = -scalar;
			ySpeedScalar = 0.0;
			break;
		case RIGHT:
			xSpeedScalar = scalar;
			ySpeedScalar = 0.0;
			break;
		case UP:
			xSpeedScalar = 0.0;
			ySpeedScalar = -scalar;
			break;
		case DOWN:
			xSpeedScalar = 0.0;
			ySpeedScalar = scalar;
			break;
		}

	}

//	/**
//	 * Direction - used in debugging to convert from a direction constant to a
//	 * human-readable string.
//	 * 
//	 * @param direction
//	 *            the direction
//	 * @return the string indicating the direction
//	 */
//	private String direction(int direction) {
//		switch (direction) {
//		case LEFT:
//			return "left";
//		case RIGHT:
//			return "right";
//		case UP:
//			return "up";
//		case DOWN:
//			return "down";
//		default:
//			return "unknown int: " + direction;
//		}
//	}

	/**
	 * Gets the health.
	 * 
	 * @return the health
	 */
	public int getHealth() {
		return health;
	}

	/**
	 * Gets the current frame.
	 * 
	 * @return the current bitmap frame
	 */
	public int getCurrentFrame() {
		return currentBitmapFrame;
	}

	/**
	 * Deep copy.
	 * 
	 * @return a new instance of this bugInfo with the same values
	 */
	public BugInfo deepCopy() {
		saveToDatabase();
		return new BugInfo(bugID, x, y, nodeIndex, xSpeedScalar, ySpeedScalar,
				health, currentBitmapFrame, pathIndex, db, levelID);
	}

	/**
	 * Save to database.
	 */
	public void saveToDatabase() {
		// Create bug info tuple in the local database.    
		primaryKey = (int) db.createBugInfoData(levelID, bugID, x, y,
				nodeIndex, xSpeedScalar, ySpeedScalar, health,
				currentBitmapFrame, pathIndex);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bugID;
		result = prime * result + currentBitmapFrame;
		result = prime * result + ((db == null) ? 0 : db.hashCode());
		result = prime * result + health;
		result = prime * result + levelID;
		result = prime * result + nodeIndex;
		result = prime * result + pathIndex;
		result = prime * result + primaryKey;
		result = prime * result + x;
		long temp;
		temp = Double.doubleToLongBits(xSpeedScalar);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + y;
		temp = Double.doubleToLongBits(ySpeedScalar);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BugInfo other = (BugInfo) obj;
		if (bugID != other.bugID)
			return false;
		if (currentBitmapFrame != other.currentBitmapFrame)
			return false;
		if (db == null) {
			if (other.db != null)
				return false;
		} else if (!db.equals(other.db))
			return false;
		if (health != other.health)
			return false;
		if (levelID != other.levelID)
			return false;
		if (nodeIndex != other.nodeIndex)
			return false;
		if (pathIndex != other.pathIndex)
			return false;
		if (primaryKey != other.primaryKey)
			return false;
		if (x != other.x)
			return false;
		if (Double.doubleToLongBits(xSpeedScalar) != Double
				.doubleToLongBits(other.xSpeedScalar))
			return false;
		if (y != other.y)
			return false;
		if (Double.doubleToLongBits(ySpeedScalar) != Double
				.doubleToLongBits(other.ySpeedScalar))
			return false;
		return true;
	}
}
