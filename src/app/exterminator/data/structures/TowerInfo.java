package app.exterminator.data.structures;

import app.exterminator.data.facades.DatabaseAdapter;

/**
 * The Class TowerInfo holds enough information about placed towers to be able
 * to recreate them after a player has saved a level and then restarts it.
 * 
 * 
 * @author carlchapman modified by Cole Groff
 */
public class TowerInfo {

	// DatabaseAdapter to local database.
	/** The db. */
	private DatabaseAdapter db;

	/** The primary key. */
	int primaryKey;

	/** The level id. */
	int levelID;

	/**
	 * The tower id refers to a value in the Tower class used in the constructor
	 * to initialize relevant fields.
	 */
	int towerID;

	/** The x location. */
	int x;

	/** The y location. */
	int y;

	/**
	 * The level of this tower indicates if it has been upgraded from the
	 * default 0 to a 1 or 2.
	 */
	int level;

	/**
	 * Instantiates a new tower info.
	 * 
	 * @param towerID
	 *            the tower id
	 * @param level
	 *            the level
	 * @param x
	 *            the x location
	 * @param y
	 *            the y location
	 * @param db
	 *            the db
	 * @param levelID
	 *            the level id
	 */
	public TowerInfo(int towerID, int level, int x, int y, DatabaseAdapter db,
			int levelID) {
		this.towerID = towerID;
		this.level = level;
		this.x = x;
		this.y = y;
		this.db = db;
		this.levelID = levelID;
	}

	/**
	 * Gets the tower id.
	 * 
	 * @return the tower id
	 */
	public int getTowerID() {
		return towerID;
	}
	
	/**
	 * Gets the primary key.
	 * 
	 * @return the primary key
	 */
	public int getPrimaryKey() {
		return primaryKey;
	}
	

	/**
	 * Gets the x location.
	 * 
	 * @return the x location
	 */
	public int getX() {
		return x;
	}

	/**
	 * Gets the y location.
	 * 
	 * @return the y location
	 */
	public int getY() {
		return y;
	}

	/**
	 * Gets the level of this tower.
	 * 
	 * @return the level of this tower
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Sets the level of this tower.
	 * 
	 * @param level
	 *            the new level of this tower
	 */
	public void setLevel(int level) {
		this.level = level;
		db.updateTowerInfoData(levelID, towerID, x, y, this.level, primaryKey);
	}

	/**
	 * Deep copy.
	 * 
	 * @return the tower info copy
	 */
	public TowerInfo deepCopy() {
		saveToDatabase();
		return new TowerInfo(towerID, level, x, y, db, levelID);
	}

	
	/**
	 * Saves this tower to the database and sets the primary key to the key
	 * created by the database.
	 */
	public void saveToDatabase() {
		primaryKey = (int) db
				.createTowerInfoData(levelID, towerID, x, y, level);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 3;
		int result = towerID;
		result = result + prime * level;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TowerInfo other = (TowerInfo) obj;
		if (level != other.level)
			return false;
		if (towerID != other.towerID)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
}
