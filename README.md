# Exterminator #

This project was developed using the eclipse Android sdk, which includes a simulator.  This simulator or a similar setup is required to run the application.  Also, the server referenced in the code (for registration, purchasing bugbucks, etc.) is no longer active, so that code would need to be re-created.

This repository is meant to be more of an archive than an actively developed product, though there has been some talk of an iOS port.  However, the graphics and level designs are amateurish (we did our best but are not trained graphic designers), so we would probably need some input from a skilled party to move forward.

